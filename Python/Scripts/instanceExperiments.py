import pandas as pd
import numpy as np

from Models.Trip import Trip
from Util.Plots import CustomPlots


def compareApproximatedParetoFronts(p_destinations=4, p_components=6, p_fetcherSeed=0, p_daysStay=1, p_ordered=False, p_notebook=False):

    v_instanceExact = Trip.getSavedTrip(p_destinations=p_destinations,
                                          p_components=p_components,
                                          p_fetcherSeed=p_fetcherSeed,
                                          p_daysStay=p_daysStay,
                                          p_approximated=False,
                                          p_ordered=p_ordered
                                          )

    v_instanceApproximated = Trip.getSavedTrip(p_destinations=p_destinations,
                                          p_components=p_components,
                                          p_fetcherSeed=p_fetcherSeed,
                                          p_daysStay=p_daysStay,
                                          p_approximated=True,
                                          p_ordered=p_ordered
                                          )

    v_jsonDataExact = v_instanceExact.jsonData.copy()
    v_jsonDataExact.pop('getAllSolutionsTime')
    v_jsonDataExact.pop('totalSolutions')
    v_jsonDataExact.pop('paretoSolutionsDf')
    if('approximated' in v_jsonDataExact.keys()):
        v_jsonDataExact.pop('approximated')
    v_jsonDataExact.pop('nadirPointExact')

    v_jsonDataApprox = v_instanceApproximated.jsonData.copy()
    if('getAllSolutionsTime' in v_jsonDataApprox.keys()):
        v_jsonDataApprox.pop('getAllSolutionsTime')
    v_jsonDataApprox.pop('totalSolutions')
    v_jsonDataApprox.pop('paretoSolutionsDf')
    v_jsonDataApprox.pop('approximated')
    if ('nadirPointExact' in v_jsonDataApprox.keys()):
        v_jsonDataApprox.pop('nadirPointExact')

    assert (str(v_jsonDataExact) == str(v_jsonDataApprox))

    v_relativeHv = v_instanceApproximated.paretoHv / v_instanceExact.paretoHv
    print('Approximated relative HV = ' + str(v_relativeHv))

    v_plotter = CustomPlots(p_notebook=p_notebook)
    v_plotter.plotSolutions(v_instanceApproximated.paretoDf, p_allSolutionsDf=None, p_nadirPoint=v_instanceExact.nadirHeuristic, p_paretoRealDf=v_instanceExact.paretoDf)

    return v_relativeHv


def compareSearchSpaces(p_notebook=False, p_destinationsList = (2,3,4)):

    v_groups = {'Base Instance': {'x': [], 'y': []},
               'Stay Length = 0': {'x': [], 'y': []},
               'Ordered': {'x': [], 'y': []},
               'Average Components = 3': {'x': [], 'y': []},
               }
    for i_destinations in p_destinationsList:

        i_instanceBase = Trip.getSavedTrip(p_destinations=i_destinations,
                                          p_components=6,
                                          p_fetcherSeed=0,
                                          p_daysStay=1,
                                          p_approximated=False,
                                          p_ordered=False
                                          )
        i_instanceDaysStay0 = Trip.getSavedTrip(p_destinations=i_destinations,
                                          p_components=6,
                                          p_fetcherSeed=0,
                                          p_daysStay=0,
                                          p_approximated=False,
                                          p_ordered=False
                                          )
        i_instanceOrdered = Trip.getSavedTrip(p_destinations=i_destinations,
                                          p_components=6,
                                          p_fetcherSeed=0,
                                          p_daysStay=1,
                                          p_approximated=False,
                                          p_ordered=True
                                          )
        i_instanceLessComponents = Trip.getSavedTrip(p_destinations=i_destinations,
                                          p_components=3,
                                          p_fetcherSeed=0,
                                          p_daysStay=1,
                                          p_approximated=False,
                                          p_ordered=False
                                          )

        v_groups['Base Instance']['x'].append(i_destinations)
        v_groups['Base Instance']['y'].append(i_instanceBase.totalSolutions)

        v_groups['Stay Length = 0']['x'].append(i_destinations)
        v_groups['Stay Length = 0']['y'].append(i_instanceDaysStay0.totalSolutions)

        v_groups['Ordered']['x'].append(i_destinations)
        v_groups['Ordered']['y'].append(i_instanceOrdered.totalSolutions)

        v_groups['Average Components = 3']['x'].append(i_destinations)
        v_groups['Average Components = 3']['y'].append(i_instanceLessComponents.totalSolutions)

    v_plotter = CustomPlots(p_notebook=p_notebook)
    v_plotter.plotBarChartGroups(p_groupsDict=v_groups, p_xLabel='Number of Destinations', p_yLabel = 'Search Space Size')