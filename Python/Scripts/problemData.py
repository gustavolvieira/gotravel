import pandas as pd
import json
from collections import namedtuple
import os

from Models.Trip import Trip
from Models.TripSolution import TripSolution
from Models.TripOptimizers.ACO.OptimizerACO import OptimizerACO
from Models.TripOptimizers.LocalSearch.LocalSearch1OptFirst import LocalSearch1OptFirst
from Models.Util import getNadirHeuristic
from Models.TripOptimizers.LocalSearchUtil import searchLocal
from Models.DataFetcher.RandomFetcher import RandomFetcher
from Models.SolutionComponent import SolutionComponent
from Util.Timer import Timer
# from Util.Dispy import DispyAux


def generateProblemInstance(p_numberDestinations = 2, p_tripOrdered = False, p_tripDateStartDays=0, p_daysStayMaxDiff=1, p_fetcherSeed= 0, p_avgAccommodations=3, p_avgTransports=3, p_parallel = True, p_approximated = False):
    import json
    from Models.Trip import Trip
    from Models.DataFetcher.RandomFetcher import RandomFetcher
    from Models.TripSolution import TripSolution
    from Models.SolutionComponent import SolutionComponent
    from Util.Timer import Timer

    v_fetcher = RandomFetcher(p_seed=p_fetcherSeed, p_numberAccommodations=p_avgAccommodations, p_numberTransports=p_avgTransports)
    v_trip = Trip.generateTrip(p_numberDestinations=p_numberDestinations, p_ordered=p_tripOrdered,  p_daysStayMaxDiff=p_daysStayMaxDiff)
    v_objectives = ['cost', 'time']

    v_accommodationOptions, v_transportOptions = v_fetcher.fetchComponents(v_trip)
    v_componentsDf, v_normalizatonReference = SolutionComponent.getComponentsDf(v_accommodationOptions, v_transportOptions, v_objectives, p_normalized=False)
    v_componentsDf['time'] = v_componentsDf['time'] / 8600

    v_nadirPointHeuristic = getNadirHeuristic(v_componentsDf, v_objectives)

    v_data = {}
    v_data['trip'] = {'numberDestinations': p_numberDestinations, 'daysStayMaxDiff': p_daysStayMaxDiff, 'ordered': p_tripOrdered, 'dateStartDays': p_tripDateStartDays}
    v_data['fetcher'] = {'type': 'randomFetcher', 'seed': p_fetcherSeed, 'averageAccommodations': p_avgAccommodations, 'averageTransports': p_avgTransports}
    v_data['objectives'] = v_objectives
    v_data['normalizatonReference'] = v_normalizatonReference
    v_data['nadirPointHeuristic'] = v_nadirPointHeuristic
    v_data['approximated'] = p_approximated

    v_fileName = 'trip-d' + str(p_numberDestinations) + '-ds' + str(p_daysStayMaxDiff) + '-or' + str(p_tripOrdered)[0] + '-s0' + '-a' + str(p_avgAccommodations) + '-t' + str(p_avgTransports) +  '-sf' + str(p_fetcherSeed) + '.json'
    if(p_approximated):
        v_fileName = v_fileName[:-5] + '-approx.json'

    with Timer(p_verbose=True, p_contextName='generateSolutions_' + v_fileName) as v_timer:
        if(p_approximated):
            v_return = getApproximatedParetoFront(p_trip=v_trip, p_componentsDf=v_componentsDf, p_objectives=v_objectives)
        else:
            if(p_parallel):
                v_return = TripSolution.getAllSolutionsParallel(v_trip, v_componentsDf)
            else:
                v_return = TripSolution.getAllSolutions(v_trip, v_componentsDf)

        v_timeElapsed = v_timer.getTimeElapsedString()


        v_data['totalSolutions'] = v_return.numberSolutions

        # if(v_fileName not in os.listdir('Data/ProblemInstances')):
        #     v_paretoSolutions = v_return.solutionsList
        #     v_paretoDf = TripSolution.builtSolutionsDf(v_paretoSolutions, v_componentsDf)
        #     v_data['getAllSolutionsTime'] = v_timeElapsed
        # else:
        #     v_problemInstance = loadProblemInstance(v_fileName)
        #     v_data['getAllSolutionsTime'] = v_timeElapsed

        v_paretoSolutions = v_return.solutionsList
        v_paretoSolutions, _ = TripSolution.updateParetoSet([], v_paretoSolutions)
        v_paretoDf = TripSolution.builtSolutionsDf(v_paretoSolutions, v_componentsDf)
        v_data['getAllSolutionsTime'] = v_timeElapsed
        v_data['paretoSolutionsDf'] = v_paretoDf.to_json()

    # with open("Data/ProblemInstances/" + v_fileName, "w") as v_fileWrite:
    #     json.dump(v_data, v_fileWrite)

    from Util.Plots import CustomPlots
    v_allSolutions = TripSolution.builtSolutionsDf(v_return.solutionsList, v_componentsDf)
    v_plotter = CustomPlots(p_notebook=False)
    v_plotter.plotSolutions(v_paretoDf, p_allSolutionsDf=v_allSolutions, p_nadirPoint=v_nadirPointHeuristic, p_paretoRealDf=None)

    return


def loadProblemInstance(p_fileName):

    InstanceReturn = namedtuple('InstanceReturn', ['trip', 'componentsDf', 'objectives', 'nadirHeuristic', 'paretoDf', 'paretoHv', 'totalSolutions', 'jsonData', 'fetcher', 'fileName', 'approximated', 'normalizationReference'])

    with open("Data/ProblemInstances/"+p_fileName, "r") as v_fileRead:
        v_data = json.load(v_fileRead)

    if(v_data['fetcher']['type'] == 'randomFetcher'):
        v_fetcher = RandomFetcher(p_seed=v_data['fetcher']['seed'],
                                  p_numberAccommodations=v_data['fetcher']['averageAccommodations'],
                                  p_numberTransports=v_data['fetcher']['averageTransports'],
                                  p_priceMinTransport=v_data['fetcher']['priceMinTransport'],
                                  p_priceMaxTransport=v_data['fetcher']['priceMaxTransport'],
                                  p_priceMinAccommodationDay=v_data['fetcher']['priceMinAccommodationDay'],
                                  p_priceMaxAccommodationDay=v_data['fetcher']['priceMaxAccommodationDay'],
                                  )
    else:
        raise Exception('Not Implemented')
    v_trip = Trip.generateTrip(p_numberDestinations=v_data['trip']['numberDestinations'],
                                p_daysStayMaxDiff=v_data['trip']['daysStayMaxDiff'],
                                p_ordered=v_data['trip']['ordered'],
                                p_dateStartDaysVariation=v_data['trip']['dateStartDays']
                                )

    v_objectives = v_data['objectives']
    v_nadirPointHeuristic = v_data['nadirPointHeuristic']
    v_totalSolutions = v_data['totalSolutions']
    v_paretoDf = pd.read_json(v_data['paretoSolutionsDf'])
    v_paretoHv = TripSolution.calcHypervolume(p_solutionsDf=v_paretoDf, p_nadirPoint=v_nadirPointHeuristic)
    if('approximated' in v_data.keys()):
        v_approximated = v_data['approximated']
    else:
        v_approximated = False

    v_accommodationOptions, v_transportOptions = v_fetcher.fetchComponents(v_trip)
    v_componentsDf, v_normalizatonReference = SolutionComponent.getComponentsDf(v_accommodationOptions, v_transportOptions, v_objectives, p_normalized=True)
    for i_obj in v_objectives:
        assert(v_normalizatonReference[i_obj] == v_data['normalizatonReference'][i_obj])

    return InstanceReturn(trip=v_trip,
                          componentsDf=v_componentsDf,
                          objectives=v_objectives,
                          nadirHeuristic= v_nadirPointHeuristic,
                          paretoDf=v_paretoDf,
                          totalSolutions=v_totalSolutions,
                          jsonData=v_data,
                          fetcher=v_fetcher,
                          fileName=p_fileName,
                          paretoHv = v_paretoHv,
                          approximated = v_approximated,
                          normalizationReference=v_normalizatonReference
                          )


def getApproximatedParetoFront(p_trip: Trip, p_componentsDf: pd.DataFrame, p_objectives):

    Return = namedtuple('return_getAllSolutions', ['solutionsList', 'numberSolutions'])

    # v_dispyAux = DispyAux()
    # v_dispyAux.initDispy(dispy_getOptimizationResults)

    import dispy, dispy.httpd
    import os

    # dispy initialization
    v_localIp = '192.168.0.28'
    v_nodesIps = ['192.168.0.28']
    ROOT_DIR = os.getcwd().split('Python')[0] + 'Python'
    v_cluster = dispy.JobCluster(dispy_getOptimizationResults, nodes=v_nodesIps, ip_addr=v_localIp, secret='351786', depends=[], dest_path=ROOT_DIR)
    v_httpServer = dispy.httpd.DispyHTTPServer(v_cluster, host=v_localIp)
    v_jobs = []

    v_jobs = []
    i_jobId = 0

    v_paretoSolutions = []
    v_iterationsWithoutImprovement = 0
    v_random=True
    for j in range(14):
        for i in range(150):
            # i_job = v_dispyAux.cluster.submit(p_trip, p_componentsDf, p_objectives)
            i_job = v_cluster.submit(p_trip, p_componentsDf, p_objectives, j)
            i_job.id = i_jobId
            i_jobId += 1
            v_jobs.append(i_job)

        # run jobs

        for i_job in v_jobs:
            print('Waiting for job ' + str(i_job.id) + ' of ' + str(len(v_jobs)))
            i_solutionList = i_job()
            # i_solutionListLocal = [searchLocal(i_solution) for i_solution in i_solutionList]
            v_paretoSolutions, v_newSolutions = TripSolution.updateParetoSet(v_paretoSolutions, i_solutionList)
            if(v_newSolutions == 0):
                v_iterationsWithoutImprovement += 1
            else:
                v_iterationsWithoutImprovement = 0

    print('iterationsWithoutImprovement: ' + str(v_iterationsWithoutImprovement))

    v_paretoSolutionsDf = TripSolution.builtSolutionsDf(v_paretoSolutions, p_componentsDf)

    # dispy closure
    v_cluster.print_status()
    v_cluster.close()
    v_httpServer.shutdown(wait=False)

    # v_dispyAux.closeDispy()

    return Return(solutionsList=v_paretoSolutions, numberSolutions=None)


def dispy_getOptimizationResults(p_trip, p_componentsDf, p_objectives, p_loopCount=0):
    from Models.TripOptimizers.ACO.OptimizerACO import OptimizerACO
    from Models.TripOptimizers.ACO.OptimizerMOEAD_ACO import OptimizerMOEAD_ACO
    from Models.TripOptimizers.LocalSearch.LocalSearch1OptFirst import LocalSearch1OptFirst

    v_localSearchModuleSteps = LocalSearch1OptFirst()
    if(p_loopCount < 4):
        v_optimizer = OptimizerACO(p_objectives=p_objectives, p_maxStagnantGenerations=1000, p_localSearchModuleSteps=v_localSearchModuleSteps, p_localSearchModuleFinal=None, p_distributed=False, p_improvedGetComponents=True, p_verbose=False)
        v_optimizer.setPresetParams('random-pure')
    elif(p_loopCount < 8):
        v_optimizer = OptimizerMOEAD_ACO(p_objectives=p_objectives, p_maxStagnantGenerations=1000, p_localSearchModuleSteps=v_localSearchModuleSteps, p_localSearchModuleFinal=None, p_distributed=False, p_improvedGetComponents=True, p_verbose=False, p_numberGroups=6, p_neighbourhoodSize=4, p_limitPheromones=True, p_minPheromoneLimitPercentage=1 / 250, p_currentSolutionInfluence=0.1, p_chooseBestComponentProb=0.9)
    else:
        v_optimizer = OptimizerMOEAD_ACO(p_objectives=p_objectives, p_maxStagnantGenerations=1000, p_localSearchModuleSteps=None, p_localSearchModuleFinal=v_localSearchModuleSteps, p_distributed=False, p_improvedGetComponents=True, p_verbose=False, p_numberGroups=6, p_neighbourhoodSize=4, p_limitPheromones=True, p_minPheromoneLimitPercentage=1 / 250, p_currentSolutionInfluence=0.1, p_chooseBestComponentProb=0.9)

    v_optimizationResults = v_optimizer.optimize(p_trip=p_trip, p_maxTimeSeconds=300, p_componentsDf=p_componentsDf, p_nadirPoint=None)

    return v_optimizationResults.solutionsList


def mergeParetoFronts():

    v_fileName = 'trip-d7-ds1-orF-s0-a6-t6-sf0-approx.json'
    v_instance1 = loadProblemInstance('trip-d7-ds1-orF-s0-a6-t6-sf0-approx.json')
    v_instance2 = loadProblemInstance('trip-d7-ds1-orF-s0-a6-t6-sf0-approx-2.json')

    assert (v_instance1.nadirHeuristic == v_instance2.nadirHeuristic)

    v_pareto1 = v_instance1.paretoDf
    v_pareto2 = v_instance2.paretoDf

    v_paretoDf, v_addedSolutions  =TripSolution.updateParetoDf(v_pareto1, v_pareto2)

    # from Util.Plots import CustomPlots
    # v_plotter = CustomPlots(p_notebook=False)
    # v_plotter.plotSolutions(v_pareto1, p_allSolutionsDf=None, p_nadirPoint=v_instance1.nadirHeuristic, p_paretoRealDf=v_paretoDf)

    with open('Data/ProblemInstances/' + v_fileName, 'r+') as v_file:
        v_instanceJson = json.load(v_file)
        v_instanceJson['paretoSolutionsDf'] = v_paretoDf.to_json()
    with open("Data/ProblemInstances/" + v_fileName, "w") as v_fileWrite:
        json.dump(v_instanceJson, v_fileWrite)

