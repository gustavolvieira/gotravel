import pandas as pd
import numpy as np
import json
from collections import namedtuple
import os
from pprint import pprint

from Models.Trip import Trip
from Models.TripSolution import TripSolution
from Models.TripOptimizers.ACO.OptimizerACO import OptimizerACO
from Models.TripOptimizers.LocalSearch.LocalSearch1OptFirst import LocalSearch1OptFirst
from Util.Timer import Timer
from Util.Plots import CustomPlots


def runOptimizationExperiment(p_numberRuns: int = 8, p_maxOptimizationSeconds = 20, p_maxStagnantGenerations=1000, p_distributed=False, p_improvedGetComponents = True, p_verbose=False, p_problemInstance = None, p_optimizer = None):

    # define problem
    if(p_problemInstance is None):
        p_problemInstance = Trip.getSavedTrip(p_destinations=2,
                                              p_components=6,
                                              p_fetcherSeed=0,
                                              p_daysStay=1,
                                              p_approximated=False,
                                              p_ordered=False
                                              )

    # define optimizer
    if(p_optimizer is None):
        v_localSearchModuleSteps = LocalSearch1OptFirst()
        v_localSearchModuleFinal = None
        p_optimizer = OptimizerACO(p_objectives=p_problemInstance.objectives, p_maxStagnantGenerations=1000, p_localSearchModuleSteps=v_localSearchModuleSteps, p_localSearchModuleFinal=v_localSearchModuleFinal, p_distributed=p_distributed, p_improvedGetComponents=p_improvedGetComponents, p_verbose=p_verbose)
        # v_optimizer = OptimizerMOEAD_ACO(p_objectives=v_problemInstance.objectives, p_maxStagnantGenerations=1000, p_localSearch=p_localSearch, p_distributed=p_distributed, p_improvedGetComponents=p_improvedGetComponents, p_verbose=p_verbose, p_numberGroups=4, p_neighbourhoodSize=8, p_limitPheromones=True, p_minPheromoneLimitPercentage=1/250, p_currentSolutionInfluence = 0.1, p_chooseBestComponentProb = 0.9)
        # v_optimizer = OptimizerACO(p_objectives=v_problemInstance.objectives, p_maxStagnantGenerations=1000, p_localSearch=p_localSearch, p_distributed=p_distributed, p_improvedGetComponents=p_improvedGetComponents, p_verbose=p_verbose, p_chooseBestComponentProb = 0.9)

    v_data = {}

    # trip info
    v_data['trip'] = {}
    v_data['trip']['numberDestinations'] = p_problemInstance.trip.destinationNumber
    v_data['trip']['daysStayMaxDiff'] = p_problemInstance.jsonData['trip']['daysStayMaxDiff']
    v_data['trip']['ordered'] = p_problemInstance.jsonData['trip']['ordered']
    v_data['trip']['dateStartVariation'] = p_problemInstance.jsonData['trip']['dateStartDays']
    v_data['trip']['maxTransportWaitHours'] = p_problemInstance.jsonData['trip']['maxTransportWaitingHours']

    # fetcher info
    v_data['fetcher'] = p_problemInstance.fetcher.getInfoDict()

    # optimizer info
    v_data['optimizer'] = p_optimizer.getInfoDict()

    # other info
    v_data['extraInfo'] = {}
    if('approximated' in p_problemInstance.jsonData.keys()):
        v_data['extraInfo']['paretoApproximated'] = p_problemInstance.jsonData['approximated']
    else:
        v_data['extraInfo']['paretoApproximated'] = False
    v_data['extraInfo']['instanceFile'] = p_problemInstance.fileName
    v_data['extraInfo']['maxOptimizationSeconds'] = p_maxOptimizationSeconds
    v_data['extraInfo']['maxStagnantGenerations'] = p_maxStagnantGenerations

    # get file
    v_experimentIdString = __getExperimentIdString(v_data)
    v_filePath = __getExperimentFilePath(p_idString=v_experimentIdString)
    v_existingResults = __getExperimentsResults(v_filePath)

    # run experiments
    v_data['results'] = v_existingResults
    with Timer(p_verbose=True, p_contextName='Optimization Experiments (' + str(p_numberRuns) + ' runs)' ) as v_timer:
        for i in range(p_numberRuns):
            i_optimizationResults = p_optimizer.optimize(p_trip=p_problemInstance.trip, p_maxTimeSeconds=p_maxOptimizationSeconds, p_componentsDf=p_problemInstance.componentsDf, p_nadirPoint=p_problemInstance.nadirHeuristic)

            i_solutionsDf = TripSolution.builtSolutionsDf(i_optimizationResults.solutionsList, i_optimizationResults.componentsDf)

            i_runData = {}
            i_runData['solutions'] = i_solutionsDf.to_json()
            i_runData['hypervolume'] = i_optimizationResults.hypervolumeSeries.to_json()
            i_runData['optimizationTime'] = i_optimizationResults.timeElapsed
            i_runData['datetime'] = v_timer.getDatetimeString()
            i_runData['log'] = i_optimizationResults.log
            i_runData['iterations'] = i_optimizationResults.iterations
            i_runData['stopCondition'] = i_optimizationResults.stopCondition
            i_runData['generationsWithoutImprovement'] = i_optimizationResults.generationsWithoutImprovement
            v_data['results'].append(i_runData)
            print(v_timer.getDatetimeString() + ' - Experiment ' + str(i+1) + ' finished. \nFile: ' + v_filePath)

    with open(v_filePath, "w") as v_fileWrite:
        json.dump(v_data, v_fileWrite)

    # update index
    with open('Data/Experiments/Optimization/pathIndex.json', "r+") as v_file:
        v_pathIndex = json.load(v_file)
        v_pathIndex[v_experimentIdString] = v_filePath
    with open('Data/Experiments/Optimization/pathIndex.json', "w") as v_file:
        json.dump(v_pathIndex, v_file)


def loadExperimentData(p_fileName: str):
    with open('Data/Experiments/Optimization/' + p_fileName, "r") as v_fileRead:
        v_data = json.load(v_fileRead)

    for i in range(len(v_data['results'])):
        v_data['results'][i]['solutions'] = pd.read_json(v_data['results'][i]['solutions'])
        v_data['results'][i]['hypervolume'] = pd.read_json(v_data['results'][i]['hypervolume'], typ='series', convert_axes=False)
        v_data['results'][i]['hypervolume'].index = v_data['results'][i]['hypervolume'].index.astype(float)
        v_data['results'][i]['hypervolume'].sort_index(inplace=True)
        v_data['results'][i]['hypervolume'].name = p_fileName
    return v_data


def getExperimentsDf(p_optimizerFilters: dict = None):
    """

    :param p_optimizerFilters: dictionary where the key is the name of the experiment optimizer field to be filtered and the value is a list of accepted values
    :return:
    """

    v_dfData = []
    v_dfIndex = []
    v_fileList = os.listdir('Data/Experiments/Optimization/')
    v_fileList.remove('pathIndex.json')
    for i_experimentFile in v_fileList:
        i_experimentData = loadExperimentData(i_experimentFile)

        if(p_optimizerFilters is not None):
            v_removeRow = False
            for i_filterKey, i_filterValues in p_optimizerFilters.items():
                if(i_experimentData['optimizer'][i_filterKey] not in i_filterValues):
                    v_removeRow = True
                    break
            if(v_removeRow):
                continue

        i_instanceData = __loadProblemDataFromExperiment(i_experimentData)

        i_averageHvDf = __getAverageHv(p_experimentData=i_experimentData, p_instanceData=i_instanceData)
        i_hvAt60 = i_averageHvDf.loc[:60, 'average'].iloc[-1]
        i_hvAt60Std = i_averageHvDf.loc[:60, 'deviation'].iloc[-1]
        i_hvArea = np.trapz(i_averageHvDf.loc[:60, 'average'])

        i_row = {**i_experimentData['optimizer'], **i_experimentData['fetcher'], **i_experimentData['trip'], **i_experimentData['extraInfo']}
        i_row['HV at 60'] = i_hvAt60
        i_row['HV at 60 Std'] = i_hvAt60Std
        i_row['HV area'] = i_hvArea

        v_iterationSeries = np.array([i_run['iterations'] for i_run in i_experimentData['results']])
        i_row['IterationsAvg'] = v_iterationSeries.mean()
        i_row['IterationsStd'] = v_iterationSeries.std()
        i_row['NumberExperiments'] = len(i_experimentData['results'])

        i_row['GenStagnantAvg'] = np.array([i_run['generationsWithoutImprovement'] for i_run in i_experimentData['results']]).mean()

        i_startupTimeAvg, i_startupTimeStd = __getStartupTime(p_experimentData=i_experimentData)
        i_row['StartupTimeAvg'] = i_startupTimeAvg
        i_row['StartupTimeStd'] = i_startupTimeStd

        v_dfData.append(i_row)
        v_dfIndex.append(int(i_experimentFile.split('_')[1].split('.')[0]))

    v_df = pd.DataFrame(v_dfData, index=v_dfIndex)
    v_df.sort_index()
    return v_df


def saveExperimentsCsv():

    v_dfData = []
    v_fileList = os.listdir('Data/Experiments/Optimization/')
    v_fileList.remove('pathIndex.json')
    for i_experimentFile in v_fileList:
        i_experimentData = loadExperimentData(i_experimentFile)
        i_instanceData = __loadProblemDataFromExperiment(i_experimentData)

        i_experimentNumber = int(i_experimentFile.split('_')[1].split('.')[0])
        for i_run in i_experimentData['results']:
            v_hvSeries = i_run['hypervolume'] / i_instanceData.paretoHv
            i_hvAt60 = v_hvSeries.loc[:60].iloc[-1]
            i_hvArea = np.trapz(v_hvSeries.loc[:60])

            i_row = {**i_experimentData['optimizer'], **i_experimentData['fetcher'], **i_experimentData['trip'], **i_experimentData['extraInfo']}
            i_row['HV at 60']   = i_hvAt60
            i_row['HV area'] = i_hvArea
            i_row['Iterations'] = i_run['iterations']

            i_row['StartupTime'] = v_hvSeries[v_hvSeries > 0].index[0]

            i_row['iterationsWithoutImprovement'] = i_run['generationsWithoutImprovement']
            i_row['experiment'] = i_experimentNumber


            v_dfData.append(i_row)

    v_df = pd.DataFrame(v_dfData)
    v_df.to_csv('Data/Experiments/OptimizationExperiments.csv', index=False)


def viewExperiment(p_experimentsFileName, p_notebook=False, p_showLegend=True, p_showTitle=True, p_nadir = False, p_normalizedResults = True):

    v_experimentData = loadExperimentData(p_experimentsFileName)
    v_instanceData = __loadProblemDataFromExperiment(v_experimentData)

    v_hvSeriesList = [i_run['hypervolume']/ v_instanceData.paretoHv for i_run in v_experimentData['results']]
    v_solutionsDf = __getBestSolutionsDf(v_experimentData)

    if p_nadir:
        v_nadir = v_instanceData.nadirHeuristic
    else:
        v_nadir = None

    __printCompleteExperimentInfo(v_experimentData, v_instanceData)

    v_plotter = CustomPlots(p_notebook=p_notebook)

    v_plotter.plotHypervolumeAverage(p_optimizerSeriesList=[v_hvSeriesList], p_showLegend=p_showLegend, p_showTitle=p_showTitle)
    v_plotter.plotHypervolumeSeries(p_seriesList=v_hvSeriesList)

    if not p_normalizedResults:
        v_solutionsDf['cost'] = v_solutionsDf['cost']*v_instanceData.normalizationReference['cost']
        v_solutionsDf['time'] = 0.5 * (v_solutionsDf['time']*v_instanceData.normalizationReference['time'])/3600
        v_instanceData.paretoDf['cost'] = v_instanceData.paretoDf['cost']*v_instanceData.normalizationReference['cost']
        v_instanceData.paretoDf['time'] = 0.5* (v_instanceData.paretoDf['time']*v_instanceData.normalizationReference['time'])/3600
        if p_nadir:
            v_nadir['cost'] = v_nadir['cost']*v_instanceData.normalizationReference['cost']
            v_nadir['time'] = (v_nadir['time']*v_instanceData.normalizationReference['time'])/3600


    v_plotter.plotSolutions(v_solutionsDf, p_allSolutionsDf=None, p_nadirPoint=v_nadir, p_paretoRealDf=v_instanceData.paretoDf)



def compareExperiments(p_experimentsFileList, p_notebook=False, p_names = None):

    v_firstExpFile = p_experimentsFileList[0]
    v_firstExpData = loadExperimentData(v_firstExpFile)
    v_instanceData = __loadProblemDataFromExperiment(v_firstExpData)
    v_instanceIdString = __getInstanceIdString(p_instanceData=v_instanceData)
    __printInstanceInfo(v_firstExpData, v_instanceData)

    v_hvSeriesList = []
    for i in range(len(p_experimentsFileList)):
        i_experiment = p_experimentsFileList[i]
        i_experimentData = loadExperimentData(i_experiment)
        i_instanceData = __loadProblemDataFromExperiment(i_experimentData)

        assert (v_instanceIdString == __getInstanceIdString(p_instanceData=i_instanceData))

        i_hvSeries = [i_run['hypervolume'] / v_instanceData.paretoHv for i_run in i_experimentData['results']]
        if(p_names is not None):
            i_hvSeries[0].name = p_names[i]

        v_hvSeriesList.append(i_hvSeries)

    v_plotter = CustomPlots(p_notebook=p_notebook)
    v_plotter.plotHypervolumeAverage(p_optimizerSeriesList=v_hvSeriesList)


def __printCompleteExperimentInfo(p_experimentData, p_instanceData):
    __printInstanceInfo(p_experimentData, p_instanceData)
    __printOptimizerInfo(p_experimentData)
    __printAnalysisInfo(p_experimentData)


def __printInstanceInfo(p_experimentData, p_instanceData):
    print('\nTRIP INFO:')
    pprint(p_experimentData['trip'], depth=99)

    print('\nFETCHER INFO:')
    pprint(p_experimentData['fetcher'], depth=99)

    print('\nEXTRA INFO:')
    pprint(p_experimentData['extraInfo'], depth=99)

    v_instanceInfo = {}
    v_instanceInfo['totalComponents'] = len(p_instanceData.componentsDf)
    v_instanceInfo['nadirPoint'] = len(p_instanceData.nadirHeuristic)
    pprint(v_instanceInfo, depth=99)


def __printOptimizerInfo(p_experimentData):
    print('\nOPTIMIZER INFO:')
    pprint(p_experimentData['optimizer'], depth=99)


def __printAnalysisInfo(p_experimentData):
    print('\nANALYSIS INFO:')

    v_analysisData = {}
    v_analysisData['TotalRuns'] = len(p_experimentData['results'])
    v_iterationSeries = np.array([i_run['iterations'] for i_run in p_experimentData['results']])
    v_analysisData['IterationsAvg'] = v_iterationSeries.mean()
    v_analysisData['IterationsStd'] = v_iterationSeries.std()
    pprint(v_analysisData, depth=99)


def __getExperimentIdString(v_data):
    v_string = str(v_data['trip']) + str(v_data['fetcher']) + str(v_data['optimizer']) + str(v_data['extraInfo'])
    return v_string


def __getInstanceIdString(p_instanceData):
    v_jsonData = p_instanceData.jsonData.copy()
    v_jsonData.pop('paretoSolutionsDf')
    v_string = str(v_jsonData)
    return v_string


def __getExperimentFilePath(p_idString):

    with open('Data/Experiments/Optimization/pathIndex.json', "r") as v_file:
        v_pathIndex = json.load(v_file)

    if(p_idString in v_pathIndex.keys()):
        v_filePath = v_pathIndex[p_idString]
    else:
        v_experimentsList = list(v_pathIndex.values())
        v_experimentsNumbers = [int(s.split('.json')[0].split('_')[1]) for s in v_experimentsList]
        v_nextNumber = max(v_experimentsNumbers) + 1
        v_fileName = 'OptimizationExperiment_' + str(v_nextNumber) + '.json'
        v_filePath = 'Data/Experiments/Optimization/' + v_fileName
        assert(v_filePath not in v_experimentsList)

    return v_filePath


def __getExperimentsResults(p_filePath):
    if(os.path.isfile(p_filePath)):
        with open(p_filePath, "r") as v_fileRead:
            v_data = json.load(v_fileRead)
        return v_data['results']
    else:
        return []


def __loadProblemDataFromExperiment(p_experimentData):

    assert (p_experimentData['fetcher']['numberAccommodations'] == p_experimentData['fetcher']['numberTransports'])

    v_problemData = Trip.getSavedTrip(p_destinations=p_experimentData['trip']['numberDestinations'],
                                      p_components=p_experimentData['fetcher']['numberAccommodations'],
                                      p_fetcherSeed=p_experimentData['fetcher']['seed'],
                                      p_daysStay=p_experimentData['trip']['daysStayMaxDiff'],
                                      p_approximated=p_experimentData['extraInfo']['paretoApproximated'],
                                      p_ordered=p_experimentData['trip']['ordered']
                                      )
    return v_problemData


def __getAverageHv(p_experimentData, p_instanceData):

    v_hvSeriesList = [i_run['hypervolume'] / p_instanceData.paretoHv for i_run in p_experimentData['results']]
    v_timeIndex = []
    [v_timeIndex.extend(i_series.index) for i_series in v_hvSeriesList]
    v_timeIndex.sort()

    v_df = []
    for i_time in v_timeIndex:
        i_valuesArray = np.array([i_series.loc[:i_time].iloc[-1] for i_series in v_hvSeriesList])
        i_row = {}
        i_row['average'] = i_valuesArray.mean()
        i_row['deviation'] = i_valuesArray.std()
        v_df.append(i_row)

    v_df = pd.DataFrame(v_df, index=v_timeIndex)
    return v_df


def __getStartupTime(p_experimentData):

    v_startupTimes = []
    for i_run in p_experimentData['results']:
        i_series = i_run['hypervolume']
        v_startupTimes.append(i_series[i_series > 0].index[0])

    v_startupTimes = np.array(v_startupTimes)
    v_mean = v_startupTimes.mean()
    v_std = v_startupTimes.std()

    return v_mean, v_std


def __getBestSolutionsDf(p_experimentData):

    v_bestHv = 0
    v_bestSolutionsDf = None

    for i_run in p_experimentData['results']:
        i_hv = i_run['hypervolume'].iloc[-1]
        if(i_hv > v_bestHv):
            v_bestHv = i_hv
            v_bestSolutionsDf = i_run['solutions']

    return v_bestSolutionsDf


def fixMaxStagnantGenerationMissingInfo():
    v_fileList = os.listdir('Data/Experiments/Optimization/')
    v_fileList.remove('pathIndex.json')

    with open('Data/Experiments/Optimization/pathIndex.json', "r") as v_file:
        v_pathIndex = json.load(v_file)

    for i_experimentFile in v_fileList:
        with open('Data/Experiments/Optimization/' + i_experimentFile, "r") as v_fileRead:
            i_experimentData = json.load(v_fileRead)

        if('maxStagnantGenerations' not in list(i_experimentData['extraInfo'].keys())):

            for i_key, i_value in v_pathIndex.items():
                if(i_experimentFile in i_value):
                    v_key = i_key

            v_pathIndex.pop(v_key)
            v_newKey = v_key[:-1] + ", 'maxStagnantGenerations': 1000}"

            i_experimentData['extraInfo']['maxStagnantGenerations'] = 1000


            v_pathIndex[v_newKey] = 'Data/Experiments/Optimization/' + i_experimentFile

            # if(i_experimentIdString in list(v_pathIndex.keys())):
            #     print('duplicate experiments: ' + i_experimentFile + 'and ' + v_pathIndex[i_experimentIdString])

        with open('Data/Experiments/Optimization/' + i_experimentFile, "w") as v_fileWrite:
            json.dump(i_experimentData, v_fileWrite)


    with open('Data/Experiments/Optimization/pathIndex.json', "w") as v_file:
        json.dump(v_pathIndex, v_file)



def deleteExperiments():
    v_fileList = os.listdir('Data/Experiments/Optimization/')
    v_fileList.remove('pathIndex.json')

    with open('Data/Experiments/Optimization/pathIndex.json', "r") as v_file:
        v_pathIndex = json.load(v_file)

    for i_experimentFile in v_fileList:
        with open('Data/Experiments/Optimization/' + i_experimentFile, "r") as v_fileRead:
            i_experimentData = json.load(v_fileRead)


        if (i_experimentData['trip']['numberDestinations'] in [6, 7]):

            os.remove('Data/Experiments/Optimization/' + i_experimentFile)
            print('File removed: ' + i_experimentFile)

            for i_key, i_value in v_pathIndex.items():
                if(i_experimentFile in i_value):
                    v_key = i_key

            v_pathIndex.pop(v_key)


    with open('Data/Experiments/Optimization/pathIndex.json', "w") as v_file:
        json.dump(v_pathIndex, v_file)


def fixExperiments():
    v_fileList = os.listdir('Data/Experiments/Optimization/')
    v_fileList.remove('pathIndex.json')

    for i_experimentFile in v_fileList:
        with open('Data/Experiments/Optimization/' + i_experimentFile, "r") as v_fileRead:
            i_experimentData = json.load(v_fileRead)


        if (i_experimentData['trip']['numberDestinations'] in [5]):

            i_resultsData = []
            for i_run in i_experimentData['results']:
                if(i_run['optimizationTime'] < 85):
                    i_resultsData.append(i_run)

            i_experimentData['results'] = i_resultsData

            with open('Data/Experiments/Optimization/' + i_experimentFile, "w") as v_file:
                json.dump(i_experimentData, v_file)

            print('File updated: ' + i_experimentFile)
