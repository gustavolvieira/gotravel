
if __name__ == '__main__':

    # from tests import testGetParetoIndexes
    # testGetParetoIndexes()

    # from tests import testRandomFetcher
    # testRandomFetcher()

    # from tests import testOptimization
    # testOptimization()

    # from tests import testGetAllSolutions
    # testGetAllSolutions()

    from Scripts.problemData import generateProblemInstance
    # generateProblemInstance(p_numberDestinations = 6, p_daysStayMaxDiff=1, p_fetcherSeed= 0, p_avgAccommodations=6, p_avgTransports=6, p_parallel=True, p_approximated=True, p_tripOrdered=False)
    generateProblemInstance(p_numberDestinations = 2, p_daysStayMaxDiff=1, p_fetcherSeed= 0, p_avgAccommodations=6, p_avgTransports=6, p_parallel=False, p_approximated=False, p_tripOrdered=False)
    # generateProblemInstance(p_numberDestinations = 4, p_daysStayMaxDiff=1, p_fetcherSeed= 0, p_avgAccommodations=6, p_avgTransports=6, p_parallel=True, p_approximated=False, p_tripOrdered=True)
    # generateProblemInstance(p_numberDestinations = 5, p_daysStayMaxDiff=1, p_fetcherSeed= 0, p_avgAccommodations=6, p_avgTransports=6, p_parallel=True, p_approximated=False, p_tripOrdered=True)
    # generateProblemInstance(p_numberDestinations = 6, p_daysStayMaxDiff=1, p_fetcherSeed= 0, p_avgAccommodations=6, p_avgTransports=6, p_parallel=True, p_approximated=False, p_tripOrdered=True)
    # generateProblemInstance(p_numberDestinations = 7, p_daysStayMaxDiff=1, p_fetcherSeed= 0, p_avgAccommodations=6, p_avgTransports=6, p_parallel=True, p_approximated=False, p_tripOrdered=True)
    # generateProblemInstance(p_numberDestinations = 8, p_daysStayMaxDiff=1, p_fetcherSeed= 0, p_avgAccommodations=6, p_avgTransports=6, p_parallel=True, p_approximated=False, p_tripOrdered=True)

    # from Models.Util import loadProblemInstance
    # loadProblemInstance(p_fileName='trip-d2-ds1-orF-s0-a6-t6-sf1.json')

    # from Scripts.optimizationExperiments import runOptimizationExperiment
    # runOptimizationExperiment()

    # from Scripts.optimizationExperiments import viewExperiment
    # viewExperiment('OptimizationExperiment_21.json')

    # from Scripts.Experiments import loadExperimentData
    # loadExperimentData('2018-06-26_19-02-59_optimization_trip-d2-s0-a6-t6.json')

    # from tests import testPlotHypervolumeAverage
    # testPlotHypervolumeAverage()

    # from tests import testLocalSearch
    # testLocalSearch()

    # from tests import testBottleneck
    # testBottleneck()

    # from tests import viewSavedTrip
    # viewSavedTrip()

    # from tests import runOptimizationExperimentsVarying
    # runOptimizationExperimentsVarying()

    # from Scripts.optimizationExperiments import getExperimentsList
    # getExperimentsList()

    # from Scripts.optimizationExperiments import compareExperiments
    # compareExperiments(['OptimizationExperiment_32.json', 'OptimizationExperiment_19.json'])

    # from Scripts.instanceExperiments import compareApproximatedParetoFronts
    # compareApproximatedParetoFronts()

    # from Scripts.instanceExperiments import compareSearchSpaces
    # compareSearchSpaces()

    # from Scripts.optimizationExperiments import deleteExperiments
    # deleteExperiments()

    # from Scripts.optimizationExperiments import fixExperiments
    # fixExperiments()

    # from Scripts.optimizationExperiments import saveExperimentsCsv
    # saveExperimentsCsv()

    # from Scripts.problemData import mergeParetoFronts
    # mergeParetoFronts()