import numpy as np
import pandas as pd


def getObjectives():
    return ('time', 'cost')


# calc pareto reference: https://stackoverflow.com/questions/32791911/fast-calculation-of-pareto-front-in-python
# original solution included as pareto dominated solutions with equal costs in one objective. Fixed it.
def getParetoIndexes(p_costsArray):
    """
    :param p_costsArray: An (n_points, n_costs) array
    :return: A (n_points, ) boolean array, indicating whether each point is Pareto efficient
    """
    # todo: tests to check turning point for efficiency
    if(p_costsArray.shape[1] <= 10):
        v_isPareto = np.ones(p_costsArray.shape[0], dtype=bool)
        for i_index, i_cost in enumerate(p_costsArray):
            if v_isPareto[i_index]:
                v_isPareto[v_isPareto] = np.any(p_costsArray[v_isPareto] < i_cost, axis=1)
                v_isPareto[i_index] = True
        return v_isPareto
    else:
        v_isPareto = np.ones(p_costsArray.shape[0], dtype=bool)
        for i_index, i_cost in enumerate(p_costsArray):
            v_isPareto[i_index] = np.all(np.any(p_costsArray >= i_cost, axis=1))
            # todo: fix dominated solutions issue
        return


def getNadirHeuristic(p_componentsDf: pd.DataFrame, p_objectives: list):

    v_uniqueDestinationsIds = set(p_componentsDf['destinationInitialId'].values)
    v_nadirValues = {}
    for i_obj in p_objectives:
        v_nadirValues[i_obj] = 0

    for i_destinationId in v_uniqueDestinationsIds:

        i_filteredDf = p_componentsDf[p_componentsDf['destinationInitialId'] == i_destinationId]
        i_accommodationDf = i_filteredDf[i_filteredDf['type'] == 'accommodation']
        i_transportDf = i_filteredDf[i_filteredDf['type'] == 'transport']

        for i_obj in p_objectives:

            if(len(i_accommodationDf) > 0):
                v_nadirValues[i_obj] += i_accommodationDf[i_obj].values.max()
            if(len(i_transportDf) > 0):
                v_nadirValues[i_obj] += i_transportDf[i_obj].values.max()

    return v_nadirValues


