from Models.SolutionComponent import SolutionComponent

class TransportOption(SolutionComponent):

    def __init__(self, p_branchesList: list, p_price, **kwargs):

        self.branchesList = p_branchesList
        self._price = p_price

        super().__init__(**kwargs)



    def _getDestinationFinal(self):
        return self.branchesList[-1].destinationArrival


    def _getDestinationInitial(self):
        return self.branchesList[0].destinationDeparture


    def _getCostValue(self):
        return self._price


    def _getTimeValue(self):
        return (self._getDateEnd() - self._getDateStart()).total_seconds()


    def _getDateStart(self):
        return self.branchesList[0].timeDeparture


    def _getDateEnd(self):
        return self.branchesList[-1].timeArrival
