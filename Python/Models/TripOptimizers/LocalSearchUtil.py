import pandas as pd
import numpy as np
from random import shuffle

from Models.TripSolution import TripSolution


def searchLocal(p_solution: TripSolution):

    v_componentsDf = p_solution.componentsDf
    v_newSolution= p_solution.copyCustom()

    v_change = True
    while(v_change is True):
        v_change = False
        v_componentsReplacementIds = v_newSolution.componentsIdsList.copy()
        shuffle(v_componentsReplacementIds)

        for i_replacementId in v_componentsReplacementIds:

            i_alternativeFeasibleComponentsIds = TripSolution.getFeasibleComponentsAlternativesIds(v_newSolution, i_replacementId)

            for i_obj in v_newSolution.objectives:
                i_alternativeFeasibleComponentsIds = [i_componentId for i_componentId in i_alternativeFeasibleComponentsIds if v_componentsDf.at[i_componentId, i_obj] <= v_componentsDf.at[i_replacementId, i_obj]]

            if(len(i_alternativeFeasibleComponentsIds) > 0):
                v_change = True
                shuffle(i_alternativeFeasibleComponentsIds)
                i_chosenComponent = i_alternativeFeasibleComponentsIds[0]
                v_newSolution.replaceComponent(p_oldId=i_replacementId, p_newId=i_chosenComponent)


    return v_newSolution