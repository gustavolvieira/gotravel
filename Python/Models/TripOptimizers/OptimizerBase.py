from abc import ABC, abstractmethod
from collections import namedtuple

from Models.Trip import Trip

class TripOptimizerBase(ABC):

    return_optimize = namedtuple('return_optimize', ['solutionsList', 'hypervolumeSeries', 'timeElapsed', 'componentsDf', 'nadirPoint', 'iterations', 'stopCondition', 'generationsWithoutImprovement', 'log'])

    def __init__(self, p_objectives : list, p_paretoSize = 50, p_name: str = None):
        self.objectives = p_objectives
        self.paretoSize = p_paretoSize
        if(p_name is not None):
            self.name = p_name
        else:
            self.name = self.__class__.__name__


    @abstractmethod
    def optimize(self, p_trip: Trip, p_maxTime, p_dataFetcher):
        raise Exception('Method not implemented')


    @abstractmethod
    def getInfoDict(self):
        raise Exception('Method not implemented')