import numpy as np
import pandas as pd

from Models.TripSolution import TripSolution
from Models.Trip import Trip
from Models.TripOptimizers.LocalSearch.LocalSearchBase import LocalSearchBase

class Ant():

    def __init__(self, p_pheromoneDf: pd.DataFrame, p_heuristicDf: pd.DataFrame, p_pheromoneModifierDict: dict,  p_heuristicModifierDict: dict, p_trip: Trip, p_componentsDf: pd.DataFrame, p_objectives: dict, p_localSearchModule: LocalSearchBase, p_chooseBestComponentProb:float, p_currentSolutionInfluence:float, p_id : int = 0):

        assert (p_heuristicDf.values.min() >= 0)
        assert (0 not in p_heuristicDf.values)
        assert (max(p_heuristicModifierDict.values()) <= 99)
        assert (max(p_pheromoneModifierDict.values()) <= 99)

        assert ((p_chooseBestComponentProb <= 1) and (p_chooseBestComponentProb >= 0))

        self.pheromoneDf = p_pheromoneDf
        self.heuristicDf = 1/(p_heuristicDf/p_heuristicDf.sum(axis=0))
        self.heuristicDf = self.heuristicDf/self.heuristicDf.max(axis=0) # only for readability
        # self.heuristicDf = abs(p_heuristicDf-1)
        self.trip = p_trip
        self.componentsDf = p_componentsDf
        self.objectives = p_objectives
        self.localSearchModule = p_localSearchModule
        self.chooseBestComponentProb = p_chooseBestComponentProb
        self.currentSolutionInfluence = p_currentSolutionInfluence
        self.id = p_id

        self.referenceSolution = None
        self.lastSolution = None


        self.pheromoneModifier = []
        for i_key in self.pheromoneDf.columns:
            self.pheromoneModifier.append(p_pheromoneModifierDict[i_key])

        self.heuristicModifier = []
        for i_key in self.heuristicDf.columns:
            self.heuristicModifier.append(p_heuristicModifierDict[i_key])

        # todo: evaluate cost of multiplying all vs only when needed
        # for i_objKey in p_heuristicModifierDict.keys():
        #     self.heuristicDf.loc[:, i_objKey] = abs(self.heuristicDf.loc[:, i_objKey] - 1) ** p_heuristicModifierDict[i_objKey]


    def builtSolution(self, p_feasibleComponentsDict: dict = None):

        self.lastSolution = TripSolution(p_trip=self.trip, p_componentsDf=self.componentsDf, p_objectives=self.objectives)

        while(self.lastSolution.isComplete() is False):
            v_feasibleStepsIds = self.lastSolution.getFeasibleComponentsIds(p_feasibleComponentsDict)

            if(len(v_feasibleStepsIds) < 1):
                self.lastSolution.unfeasible = True
                break

            self.lastSolution.addComponent(self.__chooseNextStep(v_feasibleStepsIds))

        if((self.localSearchModule is not None) and (not self.lastSolution.unfeasible)):
            self.lastSolution = self.localSearchModule.search(self.lastSolution, p_feasibleComponentsDict=p_feasibleComponentsDict)

        return self.lastSolution


    def __chooseNextStep(self, p_feasibleStepsIds):

        if (len(p_feasibleStepsIds) == 1):
            return p_feasibleStepsIds[0]

        v_pheromonesDf = self.pheromoneDf.loc[p_feasibleStepsIds]
        if((self.currentSolutionInfluence > 0) and (self.referenceSolution is not None)):
            v_componentsInReferenceIds = [i_id for i_id in self.referenceSolution.componentsIdsList if i_id in v_pheromonesDf.index]
            v_additionalValue = self.currentSolutionInfluence * np.ones([len(v_componentsInReferenceIds), len(v_pheromonesDf.columns)])
            v_pheromonesDf.loc[v_componentsInReferenceIds] += v_additionalValue

        v_pheromonesDf = v_pheromonesDf ** self.pheromoneModifier
        v_heuristicDf = self.heuristicDf.loc[p_feasibleStepsIds] ** self.heuristicModifier

        v_probValues =  v_pheromonesDf.product(axis=1) * v_heuristicDf.product(axis=1)
        v_probValues = v_probValues/v_probValues.sum()

        if(np.random.random() < self.chooseBestComponentProb):
            v_chosenStepId = v_pheromonesDf.index.values[v_probValues.values.argmax()]     # returns first value. Problem?
        else:
            v_chosenStepId = np.random.choice(v_pheromonesDf.index.values, p=v_probValues)

        return v_chosenStepId