import pandas as pd
import numpy as np

from Models.TripOptimizers.OptimizerBase import TripOptimizerBase, Trip
from Models.TripOptimizers.ACO.Ant import Ant
from Models.TripSolution import TripSolution
from Models.Util import getNadirHeuristic
from Models.TripOptimizers.LocalSearch.LocalSearchBase import LocalSearchBase

from Util.Timer import Timer


class OptimizerACO(TripOptimizerBase):

    def __init__(self, p_numberAnts:int = 30,
                 p_evaporationRate = 0.05, p_depositRate = 0.1, p_pheromoneDefault = 1, # pheromone related parameters
                 p_minimumParetoImprovement = 1, p_maxStagnantGenerations:int =100, # stop-condition parameters
                 p_pheromoneModifierDict: dict = None, p_heuristicModifierDict: dict = None,
                 p_chooseBestComponentProb = 0, p_currentSolutionInfluence = 0, # solution construction
                 p_localSearchModuleSteps: LocalSearchBase = None, p_localSearchModuleFinal: LocalSearchBase = None,
                 p_distributed: bool = False, p_improvedGetComponents: bool = True,
                 p_limitPheromones: bool = False, p_minPheromoneLimitPercentage = None, # pheromone limits
                 p_verbose: bool = False,
                 **kwargs
                 ):
        super().__init__(**kwargs)

        self.evaporationRate = p_evaporationRate
        self.depositRate = p_depositRate
        self.pheromoneDefault = p_pheromoneDefault
        self.numberAnts = p_numberAnts
        self.minimumParetoImprovement = p_minimumParetoImprovement
        self.maxStagnantGenerations = p_maxStagnantGenerations
        self.distributed = p_distributed
        self.verbose = p_verbose
        self.improvedGetComponents = p_improvedGetComponents
        self.chooseBestComponentProb = p_chooseBestComponentProb
        self.currentSolutionInfluence = p_currentSolutionInfluence

        self.localSearchModuleSteps = p_localSearchModuleSteps
        self.localSearchModuleFinal = p_localSearchModuleFinal

        self.limitPheromones = p_limitPheromones
        self.minPheromoneLimitPercentage = p_minPheromoneLimitPercentage

        if(p_pheromoneModifierDict is None):
            p_pheromoneModifierDict = {}
            for i_objective in self.objectives:
                p_pheromoneModifierDict[i_objective] = 1
        self.pheromoneModifierDict = p_pheromoneModifierDict

        if(p_heuristicModifierDict is None):
            p_heuristicModifierDict = {}
            for i_objective in self.objectives:
                p_heuristicModifierDict[i_objective] = 2
        self.heuristicModifierDict = p_heuristicModifierDict
        # Heuristic value is always between 0 and 1
        # - modifier = 0 => heuristic = 1, no effect
        # - modifier = 1 => heuristic effect linear
        # - modifier = 2 => heuristic effect quadratic, and so on


    def getInfoDict(self):

        v_dict = {}
        v_dict['Name']=  self.name
        v_dict['NumberAnts']= self.numberAnts
        v_dict['EvaporationRate']= self.evaporationRate
        v_dict['DepositRate']= self.depositRate
        v_dict['PheromoneDefault']= self.pheromoneDefault
        v_dict['MinimumParetoImprovement']=  self.minimumParetoImprovement
        v_dict['MaxStagnantGenerations']=  self.maxStagnantGenerations
        v_dict['PheromoneModifier']= self.pheromoneModifierDict
        v_dict['HeuristicModifier']= self.heuristicModifierDict

        v_dict['ChooseBestComponentProb']= self.chooseBestComponentProb
        v_dict['CurrentSolutionInfluence']= self.currentSolutionInfluence

        v_dict['Distributed']= self.distributed
        v_dict['ImprovedGetComponents']= self.improvedGetComponents

        v_dict['LimitPheromones']= self.distributed
        v_dict['MinPheromoneLimitPercentage']= self.minPheromoneLimitPercentage

        if(self.localSearchModuleSteps is not None):
            v_dict['LocalSearchSteps']= self.localSearchModuleSteps.getInfoDict()
        else:
            v_dict['LocalSearchSteps'] = self.localSearchModuleSteps

        if(self.localSearchModuleFinal is not None):
            v_dict['LocalSearchFinal']= self.localSearchModuleFinal.getInfoDict()
        else:
            v_dict['LocalSearchFinal'] = self.localSearchModuleFinal

        return v_dict


    def setPresetParams(self, p_type='default'):
        if(p_type == 'deterministic-greedy'):
            for i_objective in self.objectives:
                self.pheromoneModifierDict[i_objective] = 0
                self.heuristicModifierDict[i_objective] = 99
        elif(p_type == 'random-greedy'):
            for i_objective in self.objectives:
                self.pheromoneModifierDict[i_objective] = 0
                self.heuristicModifierDict[i_objective] = 1
        elif(p_type == 'random-pure'):
            for i_objective in self.objectives:
                self.pheromoneModifierDict[i_objective] = 0
                self.heuristicModifierDict[i_objective] = 0
        elif(p_type == 'default'):
            for i_objective in self.objectives:
                self.pheromoneModifierDict[i_objective] = 1
                self.heuristicModifierDict[i_objective] = 2
        else:
            raise Exception('Invalid optimizer preset')


    def optimize(self, p_trip: Trip, p_maxTimeSeconds, p_componentsDf: pd.DataFrame, p_nadirPoint:dict = None):

        if (self.distributed):
            from Util.Dispy import DispyAux

            self.dispyAux = DispyAux()
            self.dispyAux.initDispy(getAntSolution_dispy)

        with Timer() as v_timer:

            v_pheromoneDf = self._getPheromoneDf(p_componentsDf=p_componentsDf)

            v_heuristicDf = p_componentsDf.loc[:, self.objectives]
            v_heuristicDf.loc[v_heuristicDf.filter(like='a', axis=0).index, 'time'] = 1
            v_heuristicDf.sort_index()

            if(p_nadirPoint is None):
                p_nadirPoint = getNadirHeuristic(p_componentsDf, self.objectives)

            self._getAnts(p_trip= p_trip, p_componentsDf = p_componentsDf, p_heuristicDf = v_heuristicDf, p_pheromoneDf= v_pheromoneDf)

            if(self.improvedGetComponents):
                v_feasibleComponentsDict = {}
            else:
                v_feasibleComponentsDict = None

            self.optimizationSolutions = []
            v_generationsWithoutImprovement = 0
            v_hypervolumeDict = {}
            v_hypervolumeDict[0] = 0

            v_iterations = 0
            while(
                    (v_timer.getSecondsElapsed() < p_maxTimeSeconds) and
                    v_generationsWithoutImprovement <= self.maxStagnantGenerations
            ):
                v_iterations += 1
                if (self.distributed):
                    v_newSolutions, v_feasibleComponentsDict = self._getSolutionsDistributed(p_dispyCluster=self.dispyAux.cluster, p_feasibleComponentsDict=v_feasibleComponentsDict)
                else:
                    v_newSolutions = [i_ant.builtSolution(v_feasibleComponentsDict) for i_ant in self.ants]
                v_newSolutions = [i_solution for i_solution in v_newSolutions if i_solution.unfeasible is not True]

                self.optimizationSolutions, v_addedSolutions = TripSolution.updateParetoSet(self.optimizationSolutions, v_newSolutions)
                if(len(v_addedSolutions) == 0):
                    v_generationsWithoutImprovement+=1
                else:
                    v_generationsWithoutImprovement=0

                self._updateReferenceSolutions()
                self._updatePheromones(p_pheromoneDf=v_pheromoneDf, p_newSolutions=v_newSolutions, p_addedSolutions=v_addedSolutions)

                v_hypervolumeDict[v_timer.getSecondsElapsed()] = TripSolution.calcHypervolume(p_solutionsList=self.optimizationSolutions, p_nadirPoint=p_nadirPoint)

            if(self.localSearchModuleFinal is not None):
                v_newSolutions = [self.localSearchModuleFinal.search(i_solution) for i_solution in self.optimizationSolutions]
                self.optimizationSolutions, v_addedSolutions = TripSolution.updateParetoSet(self.optimizationSolutions, v_newSolutions)
                if (len(v_addedSolutions) == 0):
                    v_generationsWithoutImprovement += 1
                else:
                    v_generationsWithoutImprovement = 0
                v_hypervolumeDict[v_timer.getSecondsElapsed()] = TripSolution.calcHypervolume(p_solutionsList=self.optimizationSolutions, p_nadirPoint=p_nadirPoint)

            if(v_generationsWithoutImprovement > self.maxStagnantGenerations):
                v_stopCondition =  'generationsWithoutImprovement'
            else:
                v_stopCondition =  'maxTime'

            if (self.distributed):
                self.dispyAux.closeDispy()

            v_hypervolumeSeries = pd.Series(v_hypervolumeDict, name=self.name)
            v_timeElapsed = v_timer.getSecondsElapsed()

            return TripOptimizerBase.return_optimize(solutionsList= self.optimizationSolutions,
                                                hypervolumeSeries=v_hypervolumeSeries,
                                                timeElapsed=v_timeElapsed,
                                                componentsDf=p_componentsDf,
                                                nadirPoint=p_nadirPoint,
                                                iterations=v_iterations,
                                                stopCondition=v_stopCondition,
                                                generationsWithoutImprovement=v_generationsWithoutImprovement,
                                                log=[]
                                                )


    def _getPheromoneDf(self, p_componentsDf: pd.DataFrame):
        v_pheromoneDf = pd.DataFrame(data=np.ones([len(p_componentsDf), len(self.objectives)], dtype=np.float64),
                                     index=p_componentsDf.index,
                                     columns=self.objectives
                                     )
        v_pheromoneDf.sort_index()

        return v_pheromoneDf


    def _getAnts(self, p_trip: Trip, p_componentsDf: pd.DataFrame, p_heuristicDf: pd.DataFrame, p_pheromoneDf: pd.DataFrame, **kwargs):
        v_ants = [Ant(p_pheromoneDf=p_pheromoneDf,
                      p_heuristicDf=p_heuristicDf,
                      p_pheromoneModifierDict=self.pheromoneModifierDict,
                      p_heuristicModifierDict=self.heuristicModifierDict,
                      p_trip=p_trip,
                      p_componentsDf=p_componentsDf,
                      p_objectives=self.objectives,
                      p_localSearchModule=self.localSearchModuleSteps,
                      p_chooseBestComponentProb = self.chooseBestComponentProb,
                      p_currentSolutionInfluence = self.currentSolutionInfluence,
                      p_id=i
                      )
                  for i in range(self.numberAnts)
                  ]

        self.ants = v_ants


    def _updateReferenceSolutions(self, **kwargs):
        pass


    def _updatePheromones(self, p_pheromoneDf, p_newSolutions, p_addedSolutions):

        p_pheromoneDf *= (1-self.evaporationRate)
        for i_solution in p_newSolutions:
            i_solutionValues = i_solution.objectiveValues
            for i_obj in self.objectives:
                p_pheromoneDf.loc[i_solution.componentsIdsList, i_obj] += self.depositRate * 1/i_solutionValues[i_obj]


    def _getSolutionsDistributed(self, p_dispyCluster, p_feasibleComponentsDict: dict):

        v_jobs = []
        i_jobId = 0
        v_antLists = [list(i) for i in np.array_split(np.array(self.ants), 11)]
        for i_antList in v_antLists:
            i_job = p_dispyCluster.submit(i_antList, p_feasibleComponentsDict)
            i_job.id = i_jobId
            i_jobId += 1
            v_jobs.append(i_job)

        # run jobs
        v_solutions = []
        v_feasibleComponentsDictList = []
        v_updatedAnts = []
        for i_job in v_jobs:
            if (self.verbose):
                print('Waiting for job ' + str(i_job.id) + ' of ' + str(len(v_jobs)))
            # i_solution = i_job()
            i_solutionList, i_feasibleComponentsDict, i_antList = i_job()
            # v_solutions.append(i_solution)
            v_feasibleComponentsDictList.append(i_feasibleComponentsDict)
            v_solutions.extend(i_solutionList)
            v_updatedAnts.extend(i_antList)

        if(p_feasibleComponentsDict is None):
            v_feasibleComponentsDictUpdated = None
        else:
            v_feasibleComponentsDictUpdated = {}
            for i_dict in v_feasibleComponentsDictList:
                v_feasibleComponentsDictUpdated = {**v_feasibleComponentsDictUpdated, **i_dict}

        v_updatedAnts.sort(key= lambda i_ant: i_ant.id)
        self.ants = v_updatedAnts
        return v_solutions, v_feasibleComponentsDictUpdated


def getAntSolution_dispy(p_antList, p_feasibleComponentsDict):
    v_solutions = []
    if(p_feasibleComponentsDict is None):
        v_feasibleComponentsDict = None
    else:
        v_feasibleComponentsDict = p_feasibleComponentsDict.copy()

    for i_ant in p_antList:
        v_solutions.append(i_ant.builtSolution(v_feasibleComponentsDict))

    return v_solutions, v_feasibleComponentsDict, p_antList