import pandas as pd
import numpy as np
from math import floor, sqrt

from Models.Trip import Trip
from Models.TripOptimizers.ACO.Ant import Ant
from Models.TripOptimizers.ACO.OptimizerACO import OptimizerACO


class OptimizerMOEAD_ACO(OptimizerACO):

    def __init__(self, p_numberGroups: int, p_neighbourhoodSize: int = 6, p_pheromoneModifierDict = None, p_heuristicModifierDict = None, **kwargs):

        super().__init__(**kwargs)

        # assert (len(p_pheromoneModifierDict.keys()) == 1)
        # assert (len(p_heuristicModifierDict.keys()) == 1)

        # todo: generalize later
        assert (len(self.objectives) == 2)
        self.numberGroups = p_numberGroups
        self.neighbourhoodSize = p_neighbourhoodSize
        self.minObjValue = 99999

        if(p_pheromoneModifierDict is None):
            p_pheromoneModifierDict = {0: 1}
        else:
            p_pheromoneModifierDict = {0: list(p_pheromoneModifierDict.values())[0]}
        self.pheromoneModifierDict = p_pheromoneModifierDict

        if(p_heuristicModifierDict is None):
            p_heuristicModifierDict = {0: 2}
        else:
            p_heuristicModifierDict = {0: list(p_heuristicModifierDict.values())[0]}
        self.heuristicModifierDict = p_heuristicModifierDict


        # Heuristic value is always between 0 and 1
        # - modifier = 0 => heuristic = 1, no effect
        # - modifier = 1 => heuristic effect linear
        # - modifier = 2 => heuristic effect quadratic, and so on


    def getInfoDict(self):

        v_dict = super().getInfoDict()

        v_dict['NumberGroups'] = self.numberGroups
        v_dict['NeighbourhoodSize'] = self.neighbourhoodSize
        v_dict['MinPheromoneLimitPercentage'] = '1/(sqrt(len(p_componentsDf)))'
        v_dict['CurrentSolutionInfluence'] = '0.05 * v_maxPheromones'

        return v_dict


    def optimize(self, p_trip: Trip, p_maxTimeSeconds, p_componentsDf: pd.DataFrame, p_nadirPoint: dict = None):

        self.minPheromoneLimitPercentage = 1/(sqrt(len(p_componentsDf)))

        return super().optimize(p_trip, p_maxTimeSeconds, p_componentsDf, p_nadirPoint)

    def _getPheromoneDf(self, p_componentsDf: pd.DataFrame):
        return None


    def _getAnts(self, p_trip: Trip, p_componentsDf: pd.DataFrame, p_heuristicDf: pd.DataFrame, p_pheromoneDf: pd.DataFrame, **kwargs):

        v_weightsAux = np.array(range(self.numberAnts))/(self.numberAnts-1)

        v_ants = []
        v_groups = [{'ants': [],
                     'pheromoneDf': pd.DataFrame(data=np.ones([len(p_componentsDf)],dtype=np.float64), index=p_componentsDf.index)
                     }
                    for i in range(self.numberGroups)
                    ]

        i_group = 0
        for i in range(self.numberAnts):
            i_weights = [v_weightsAux[i], v_weightsAux[len(v_weightsAux)-1-i]]
            assert (np.array(i_weights).sum() == 1)

            i_heuristicDf = pd.DataFrame((p_heuristicDf*i_weights).sum(axis=1))

            i_ant = Ant(p_pheromoneDf=v_groups[i_group]['pheromoneDf'],
                        p_heuristicDf=i_heuristicDf,
                        p_pheromoneModifierDict=self.pheromoneModifierDict,
                        p_heuristicModifierDict=self.heuristicModifierDict,
                        p_trip=p_trip,
                        p_componentsDf=p_componentsDf,
                        p_objectives=self.objectives,
                        p_localSearchModule=self.localSearchModuleSteps,
                        p_chooseBestComponentProb=self.chooseBestComponentProb,
                        p_currentSolutionInfluence=self.currentSolutionInfluence,
                        p_id=i
                        )
            i_ant.weights = i_weights
            i_ant.group = i_group

            v_groups[i_group]['ants'].append(i_ant)
            v_ants.append(i_ant)

            i_group = (i_group+1) % self.numberGroups


        self.groups = v_groups
        self.ants = v_ants


    def _updatePheromones(self, p_pheromoneDf, p_newSolutions, p_addedSolutions):

        v_maxPheromones = (len(self.optimizationSolutions) + 1)/(self.evaporationRate*self.minObjValue)
        v_updatedCurrentSolutionInfluence = 0.05 * v_maxPheromones

        v_addedSolutionsIds = [i_solution.id for i_solution in p_addedSolutions]
        for i_group in self.groups:
            i_pheromoneDf = i_group['pheromoneDf']
            i_pheromoneDf *= (1-self.evaporationRate)

        for i_ant in self.ants:
            i_ant.currentSolutionInfluence = v_updatedCurrentSolutionInfluence
            i_solution = i_ant.lastSolution
            if(i_solution.id not in v_addedSolutionsIds):
                continue
            v_addedSolutionsIds.remove(i_solution.id) # avoids repetition if different ants generated the same solution
            i_pheromoneDf = i_ant.pheromoneDf

            i_solutionWeightedValues = (np.array(list(i_solution.objectiveValues.values())) * i_ant.weights).sum()
            if(i_solutionWeightedValues < self.minObjValue):
                self.minObjValue = i_solutionWeightedValues

            i_pheromoneDf.loc[i_solution.componentsIdsList] += self.depositRate * 1/i_solutionWeightedValues

        self.__limitPheromones()


    def __limitPheromones(self):
        v_maxPheromones = (len(self.optimizationSolutions) + 1)/(self.evaporationRate*self.minObjValue)
        v_minPheromones = self.minPheromoneLimitPercentage * v_maxPheromones

        for i_group in self.groups:
            i_pheromoneDf = i_group['pheromoneDf']
            i_pheromoneDf[i_pheromoneDf > v_maxPheromones] = v_maxPheromones
            i_pheromoneDf[i_pheromoneDf < v_minPheromones] = v_minPheromones


    def _updateReferenceSolutions(self, **kwargs):

        v_usedSolutionsIds = []
        for i in range(self.numberAnts):
            i_mainAnt = self.ants[i]
            i_neighbourhood = [i_ant for i_ant in self.__getAntNeighbourhood(i_index=i)]
            i_filteredNeighbourhood = [i_ant for i_ant in i_neighbourhood if i_ant.lastSolution.id not in v_usedSolutionsIds]

            if(i_filteredNeighbourhood):
                i_weightedSolutionsValues = np.array([(np.array(list(i_ant.lastSolution.objectiveValues.values())) * i_ant.weights).sum() for i_ant in i_filteredNeighbourhood])
                i_selectedSolution = i_filteredNeighbourhood[i_weightedSolutionsValues.argmin()].lastSolution
                i_mainAnt.referenceSolution = i_selectedSolution
                v_usedSolutionsIds.append(i_selectedSolution.id)



    def __getAntNeighbourhood(self, i_index):
        v_lowerLimit = max(0, i_index-floor(self.neighbourhoodSize/2))
        v_upperLimit = min(self.neighbourhoodSize-1, 1+i_index+floor(self.neighbourhoodSize/2))
        v_neighbourhood = self.ants[v_lowerLimit:v_upperLimit][:self.neighbourhoodSize]

        return v_neighbourhood

