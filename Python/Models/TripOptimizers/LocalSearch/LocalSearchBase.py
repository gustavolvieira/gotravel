from abc import ABC, abstractmethod
from math import inf

from Models.TripSolution import TripSolution

class LocalSearchBase(ABC):

    def __init__(self, p_details: str = None, p_maxSteps = inf, p_skipSearchFrequency = inf):
        self.name = self.__class__.__name__
        self.details = p_details
        self.maxSteps = p_maxSteps

        assert (p_skipSearchFrequency > 0)
        self.skipSearchFrequency = p_skipSearchFrequency
        self.skipCounter = 0


    def search(self, p_solution: TripSolution, p_feasibleComponentsDict = None):

        if(self.skipCounter == self.skipSearchFrequency):
            self.skipCounter = 0
            return p_solution
        else:
            self.skipCounter += 1

        v_newSolution = p_solution.copyCustom()
        v_steps = 0
        while (v_steps < self.maxSteps):
            v_steps += 1
            v_oldId = v_newSolution.id
            v_newSolution = self.search1Step(v_newSolution, p_feasibleComponentsDict=p_feasibleComponentsDict)
            if(v_newSolution.id == v_oldId):    # interrupts if solution is unchanged
                break

        return v_newSolution


    @abstractmethod
    def search1Step(self, p_solution: TripSolution, p_feasibleComponentsDict = None):
        raise Exception('Method not implemented')


    def getInfoDict(self):
        v_dict = {}
        v_dict['Name'] = self.name
        v_dict['MaxSteps'] = self.maxSteps
        v_dict['SkipSearchFrequency'] = self.skipSearchFrequency
        if(self.details):
            v_dict['Details'] = self.details

        return v_dict
