from random import shuffle
from enum import Enum

from Models.TripOptimizers.LocalSearch.LocalSearchBase import LocalSearchBase
from Models.TripSolution import TripSolution


class LocalSearch1OptFirst(LocalSearchBase):

    def search1Step(self, p_solution: TripSolution, p_feasibleComponentsDict = None):

        return self.__search1StepFirst(p_solution, p_feasibleComponentsDict=p_feasibleComponentsDict)
        # return self.__search1StepDeprecated(p_solution)


    def __search1StepFirst(self, p_solution: TripSolution, p_feasibleComponentsDict = None):
        """
        Check all solution neighbors. The first that brings improvement and is feasible is returned
        :param p_solution:
        :param p_feasibleComponentsDict:
        :return:
        """

        v_componentsDf = p_solution.componentsDf
        v_replacedComponentsIds = p_solution.componentsIdsList.copy()
        shuffle(v_replacedComponentsIds)

        # loop through possible replacement positions
        for i_replacedId in v_replacedComponentsIds:

            i_partialSolution = p_solution.copyCustom()
            i_partialSolution.trimBeforeComponent(p_componentId = i_replacedId)
            v_replacementIdsList = list(i_partialSolution.getFeasibleComponentsIds(p_feasibleComponentsDict))
            v_replacementIdsList.remove(i_replacedId)

            v_oldObjValues = v_componentsDf.loc[i_replacedId, p_solution.objectives].values
            if(i_replacedId != p_solution.componentsIdsList[-1]):
                v_nextId = p_solution.componentsIdsList[p_solution.componentsIdsList.index(i_replacedId)+1]
            else:
                v_nextId = None

            # loop through possible replacement components for that position
            for i_newId in v_replacementIdsList:

                # checks if new component improves solution
                v_newObjValues = v_componentsDf.loc[i_newId, p_solution.objectives].values
                if( not ((v_newObjValues<v_oldObjValues).any() and (v_newObjValues<=v_oldObjValues).all())):
                    continue

                # check if components maintains solution feasible
                if (v_nextId is not None):
                    i_alternativeSolution = i_partialSolution.copyCustom()
                    i_alternativeSolution.addComponent(i_newId)

                    i_nextComponents = i_alternativeSolution.getFeasibleComponentsIds(p_feasibleComponentsDict)
                    if (v_nextId not in i_nextComponents):
                        continue

                p_solution.replaceComponent(p_oldId=i_replacedId, p_newId=i_newId)
                return p_solution

        return p_solution


    def __search1StepDeprecated(self, p_solution: TripSolution):
        v_componentsDf = p_solution.componentsDf
        v_componentsReplacementIds = p_solution.componentsIdsList.copy()
        shuffle(v_componentsReplacementIds)

        for i_replacementId in v_componentsReplacementIds:

            i_alternativeFeasibleComponentsIds = TripSolution.getFeasibleComponentsAlternativesIds(p_solution, i_replacementId)

            for i_obj in p_solution.objectives:
                i_alternativeFeasibleComponentsIds = [i_componentId for i_componentId in i_alternativeFeasibleComponentsIds if v_componentsDf.at[i_componentId, i_obj] <= v_componentsDf.at[i_replacementId, i_obj]]

            if (len(i_alternativeFeasibleComponentsIds) > 0):
                shuffle(i_alternativeFeasibleComponentsIds)
                i_chosenComponent = i_alternativeFeasibleComponentsIds[0]
                p_solution.replaceComponent(p_oldId=i_replacementId, p_newId=i_chosenComponent)

        return p_solution

