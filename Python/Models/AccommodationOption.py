import pandas as pd

from Models.SolutionComponent import SolutionComponent
from Models.Accommodation import Accommodation

class AccommodationOption(SolutionComponent):

    def __init__(self, p_accommodation, p_dateArrival, p_dateDeparture, **kwargs):
        self.accommodation = p_accommodation
        self._dateArrival = p_dateArrival
        self._dateDeparture = p_dateDeparture
        # self._totalPrice = p_totalPrice

        super().__init__(**kwargs)



    def _getDestinationFinal(self):
        return self.accommodation.destination


    def _getDestinationInitial(self):
        return self.accommodation.destination


    def _getCostValue(self):
        v_priceSeries = self.accommodation.priceSeries[self.dateStart:self.dateEnd]
        v_priceSeries = v_priceSeries.iloc[:-1]
        return v_priceSeries.values.sum()

    def _getTimeValue(self):
        return 0


    def _getDateStart(self):
        return self._dateArrival


    def _getDateEnd(self):
        return self._dateDeparture


    @staticmethod
    def getOptionsList(p_accommodation: Accommodation, p_dateArrivalIndexes: pd.DatetimeIndex,  p_daysMin: int, p_daysMax: int, p_dateEndMax: pd.Timestamp):

        v_optionsList = []

        for i_daysStay in range(p_daysMin, p_daysMax+1):
            for i_startDate in p_dateArrivalIndexes:
                i_endDate = i_startDate + pd.DateOffset(days=i_daysStay)
                if(i_endDate > p_dateEndMax):
                    continue
                # i_totalPrice = p_accommodation.priceSeries.loc[i_startDate:i_endDate].sum()
                i_option = AccommodationOption(p_accommodation=p_accommodation, p_dateArrival=i_startDate, p_dateDeparture=i_endDate)
                v_optionsList.append(i_option)

        return v_optionsList


