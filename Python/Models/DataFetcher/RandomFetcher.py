import numpy as np
import pandas as pd
from math import modf

from Models.Trip import Trip
from Models.DataFetcher.FetcherBase import DataFetcherBase
from Models.Destination import Destination
from Models.Accommodation import Accommodation
from Models.AccommodationOption import AccommodationOption
from Models.TransportOption import TransportOption
from Models.TransportBranch import TransportBranch

class RandomFetcher(DataFetcherBase):

    def __init__(self, p_seed = None, p_numberAccommodations: int = 8, p_numberTransports: int = 8,
                 p_priceMinTransport = 300, p_priceMaxTransport = 3000,
                 p_priceMinAccommodationDay= 50, p_priceMaxAccommodationDay = 500
                 ):
        super().__init__()

        self.numberAccommodations = p_numberAccommodations
        self.numberTransports = p_numberTransports

        self.priceMinTransport = p_priceMinTransport
        self.priceMaxTransport = p_priceMaxTransport
        self.priceMinAccommodationDay = p_priceMinAccommodationDay
        self.priceMaxAccommodationDay = p_priceMaxAccommodationDay

        self.seed = p_seed


    def getInfoDict(self):
        v_dict = {}
        v_dict['name'] = self.name
        v_dict['seed'] = self.seed
        v_dict['numberAccommodations'] = self.numberAccommodations
        v_dict['numberTransports'] = self.numberTransports
        v_dict['priceMinTransport'] = self.priceMinTransport
        v_dict['priceMaxTransport'] = self.priceMaxTransport
        v_dict['priceMinAccommodationDay'] = self.priceMinAccommodationDay
        v_dict['priceMaxAccommodationDay'] = self.priceMaxAccommodationDay

        return v_dict


    def fetchComponents(self, p_trip: Trip):
        np.random.seed(self.seed)
        v_components = super().fetchComponents(p_trip)
        np.random.seed(None)
        return v_components


    def fetchAccommodations(self, p_destination: Destination, p_dateArrivalIndexes: pd.DatetimeIndex, p_daysMin: int, p_daysMax: int, p_dateEndMax: pd.Timestamp):

        v_allDatesIndex = pd.DatetimeIndex(start=p_dateArrivalIndexes[0], end=p_dateArrivalIndexes[-1]+pd.DateOffset(days=p_daysMax), freq=pd.DateOffset(days=1))
        v_prices = np.random.random(size=(len(v_allDatesIndex), self.numberAccommodations)) * (self.priceMaxAccommodationDay - self.priceMinAccommodationDay)  + self.priceMinAccommodationDay
        v_accommodationPricesDf = pd.DataFrame(v_prices, index=v_allDatesIndex)

        v_accommodationOptions = []
        for i_accIndex in range(self.numberAccommodations):
            i_accName = 'acc' + str(i_accIndex)
            i_accommodation = Accommodation(p_id=i_accIndex, p_name=i_accName, p_destination=p_destination,
                                            p_type='Hotel', p_checkInTime=pd.Timestamp('14:00:00'), p_checkOutTime=pd.Timestamp('12:00:00'),
                                            p_priceSeries = v_accommodationPricesDf.loc[:, i_accIndex])
            i_accommodationOptionsList = AccommodationOption.getOptionsList(p_accommodation=i_accommodation, p_dateArrivalIndexes=p_dateArrivalIndexes,
                                                                       p_daysMin=p_daysMin, p_daysMax=p_daysMax, p_dateEndMax= p_dateEndMax)
            v_accommodationOptions.extend(i_accommodationOptionsList)

        return v_accommodationOptions


    def fetchTransports(self, p_destinationStart: Destination, p_destinationEnd: Destination, p_dateIndexes: pd.DatetimeIndex):

        v_prices = np.random.random(size=(len(p_dateIndexes), self.numberTransports)) * (
                    self.priceMaxTransport - self.priceMinTransport) + self.priceMinTransport
        v_transportPricesDf = pd.DataFrame(v_prices, index=p_dateIndexes)

        v_transportOptions = []
        v_hoursTravelMean = np.random.randint(3,7)
        v_departureHours = np.arange(start=4, stop=22, step=(24-6)/self.numberTransports) + (np.random.random(self.numberTransports)*1.5)

        for i_date in p_dateIndexes:
            v_hoursTravelList = (np.ones(self.numberTransports) * v_hoursTravelMean) + (np.random.random(self.numberTransports) * 1.5)
            for i_transpIndex in range(self.numberTransports):
                i_price = v_transportPricesDf.loc[i_date, i_transpIndex]

                i_departureMinutes, i_departurelHours = modf(v_departureHours[i_transpIndex])
                i_departurelHours = int(i_departurelHours)
                i_departureMinutes = int(i_departureMinutes * 60)
                i_departureTime = i_date.replace(hour=i_departurelHours, minute=i_departureMinutes)

                i_travelMinutes, i_travelHours = modf(v_hoursTravelList[i_transpIndex])
                i_travelHours = int(i_travelHours)
                i_travelMinutes = int(i_travelMinutes * 60)
                i_arrivalTime = i_departureTime + pd.DateOffset(hours= i_travelHours, minutes=i_travelMinutes)
                i_branchList = [TransportBranch(p_type='Plane', p_timeDeparture=i_departureTime, p_timeArrival= i_arrivalTime, p_destinationDeparture=p_destinationStart, p_destinationArrival=p_destinationEnd)]
                i_option = TransportOption(p_branchesList= i_branchList, p_price=i_price)
                v_transportOptions.append(i_option)

        return v_transportOptions