from abc import ABC, abstractmethod
import pandas as pd

from Models.Trip import Trip
from Models.Destination import Destination
from Models.DestinationSpecification import DestinationSpecification

class DataFetcherBase(ABC):

    def __init__(self):
        self.name = self.__class__.__name__
        pass


    # Todo: filter by restrictions
    # OBS: there is some slight redundancy regarding days count. Usually it get an extra day just to be sure.
    def fetchComponents(self, p_trip: Trip):

        v_destinationsDatesDf = self.__getDestinationsPossibleDates(p_trip)
        v_transportDatesDf = self.__getTransportDates(v_destinationsDatesDf, p_trip)
        v_accommodationOptions = []
        v_transportOptions = []

        for i_destinationId in v_destinationsDatesDf.index:
            if(v_destinationsDatesDf.loc[i_destinationId, 'accommodationRequired']):
                i_destination = v_destinationsDatesDf.loc[i_destinationId, 'destination']
                i_datesArrivalIndexes = v_destinationsDatesDf.loc[i_destinationId, 'possibleDatesArrival']
                i_daysMin = int(v_destinationsDatesDf.loc[i_destinationId, 'daysMin'])
                i_daysMax = int(v_destinationsDatesDf.loc[i_destinationId, 'daysMax'])
                v_accommodationOptions.extend(self.fetchAccommodations(p_destination = i_destination, p_dateArrivalIndexes=i_datesArrivalIndexes, p_daysMin=i_daysMin, p_daysMax=i_daysMax, p_dateEndMax=p_trip.dateEndMax))
            else:
                raise Exception('not implemented')

        for i_index in v_transportDatesDf.index:
            i_destinationStart = v_transportDatesDf.loc[i_index, 'destinationStart']
            i_destinationEnd = v_transportDatesDf.loc[i_index, 'destinationEnd']
            i_dateIndexes = v_transportDatesDf.loc[i_index, 'possibleDates']
            v_transportOptions.extend(self.fetchTransports(p_destinationStart = i_destinationStart, p_destinationEnd=i_destinationEnd, p_dateIndexes=i_dateIndexes))

        return v_accommodationOptions, v_transportOptions


    @abstractmethod
    def getInfoDict(self):
        raise Exception('Method not implemented')


    @abstractmethod
    def fetchAccommodations(self, p_destination: Destination, p_dateArrivalIndexes: pd.DatetimeIndex, p_daysMin: int, p_daysMax: int, p_dateEndMax: pd.Timestamp):
        raise Exception('Method not implemented')


    @abstractmethod
    def fetchTransports(self, p_destinationStart: Destination, p_destinationEnd: Destination, p_dateIndexes: pd.DatetimeIndex):
        raise Exception('Method not implemented')


    def __getDestinationsPossibleDates(self, p_trip: Trip):
        v_baseIndex = pd.DatetimeIndex(start=p_trip.dateStartMin, end=p_trip.dateEndMax, freq=pd.DateOffset(days=1))
        p_trip.destinationsSpecifications.sort(key=lambda i_specification: (i_specification.absoluteOrder or 999))


        v_specificationsDf = DestinationSpecification.getSpecificationsDf(p_trip.destinationsSpecifications)
        v_specificationsDf['possibleDatesArrival'] = [v_baseIndex] * len(p_trip.destinationsSpecifications)
        v_specificationsDf['possibleDatesDeparture'] = [v_baseIndex] * len(p_trip.destinationsSpecifications)
        v_specificationsDf.loc[:, ['daysMin','daysMax']] = v_specificationsDf.loc[:, ['daysMin','daysMax']].astype(int)

        p_destinationsMinStay = v_specificationsDf['daysMin'].min()

        for i_destId in v_specificationsDf.index:
            i_row = v_specificationsDf.loc[i_destId, :]

            # Filter based on trip.dateStartMax:
            # if date is on beginning (no time to fit any destination before) and greater than trip.dateStartMax, remove it from possibleDatesArrival
            if (p_trip.dateStartMax is not None):
                v_datesArrival = i_row['possibleDatesArrival']
                v_datesRemoved = v_datesArrival[(v_datesArrival > p_trip.dateStartMax) &
                                                ((v_datesArrival - pd.DateOffset(days=int(p_destinationsMinStay))) < p_trip.dateStartMin)]
                v_specificationsDf.at[i_destId,'possibleDatesArrival'] = v_datesArrival.drop(v_datesRemoved)
                i_row = v_specificationsDf.loc[i_destId, :]

            # filter based on dateMin and dateMax
            if (i_row['dateMin'] is not None):
                v_specificationsDf.at[i_destId,'possibleDatesArrival'] = i_row['possibleDatesArrival'][i_row['possibleDatesArrival'] >= i_row['dateMin']]
                v_specificationsDf.at[i_destId,'possibleDatesDeparture'] = i_row['possibleDatesDeparture'][i_row['possibleDatesDeparture'] >= i_row['dateMin']]
                i_row = v_specificationsDf.loc[i_destId, :]
            if (i_row['dateMax'] is not None):
                v_specificationsDf.at[i_destId,'possibleDatesArrival'] = i_row['possibleDatesArrival'][i_row['possibleDatesArrival'] <= i_row['dateMax']]
                v_specificationsDf.at[i_destId,'possibleDatesDeparture'] = i_row['possibleDatesDeparture'][i_row['possibleDatesDeparture'] <= i_row['dateMax']]
                i_row = v_specificationsDf.loc[i_destId, :]

            # filter based on absolute order
            if (i_row['absoluteOrder'] is not None):
                # cut from end
                v_maxDestinationsAfter = v_specificationsDf.shape[0] - i_row['absoluteOrder']
                if (v_maxDestinationsAfter > 0):
                    v_possibleDestinationsAfterDf = v_specificationsDf[~(v_specificationsDf['absoluteOrder'] <= i_row['absoluteOrder'])]
                    v_possibleDestinationsAfterDf = v_possibleDestinationsAfterDf.sort_values(['absoluteOrder', 'daysMin'], ascending=[True, True]).iloc[:v_maxDestinationsAfter]
                    v_minDaysAfter = int(v_possibleDestinationsAfterDf['daysMin'].sum())
                    v_upperLimitDate = p_trip.dateEndMax - pd.DateOffset(days=v_minDaysAfter - 1)
                else:
                    v_upperLimitDate = p_trip.dateEndMax

                # cut from start
                v_maxDestinationsBefore = i_row['absoluteOrder'] - 1
                if (v_maxDestinationsBefore > 0):
                    v_possibleDestinationsBeforeDf = v_specificationsDf[~(v_specificationsDf['absoluteOrder'] >= i_row['absoluteOrder'])]
                    v_possibleDestinationsBeforeDf = v_possibleDestinationsBeforeDf.sort_values(['absoluteOrder', 'daysMin'], ascending=[True, True]).iloc[:v_maxDestinationsBefore]
                    v_minDaysBefore = int(v_possibleDestinationsBeforeDf['daysMin'].sum())
                    v_lowerLimitDate = p_trip.dateStartMin + pd.DateOffset(days=v_minDaysBefore - 1)
                else:
                    v_lowerLimitDate = p_trip.dateStartMin

                if (v_upperLimitDate > v_lowerLimitDate):
                    v_possibleDays = pd.DatetimeIndex(start=v_lowerLimitDate, end=v_upperLimitDate, freq=pd.DateOffset(days=1))
                    v_specificationsDf.at[i_destId,'possibleDatesArrival'] = i_row['possibleDatesArrival'][i_row['possibleDatesArrival'].isin(v_possibleDays)]
                    v_specificationsDf.at[i_destId,'possibleDatesDeparture'] = i_row['possibleDatesDeparture'][i_row['possibleDatesDeparture'].isin(v_possibleDays)]
                    i_row = v_specificationsDf.loc[i_destId, :]

            # filter based on daysMin, daysMax, arrival and departure
            v_maxDateArrival = i_row['possibleDatesDeparture'][-1] - pd.DateOffset(days=int(i_row['daysMin']))
            v_minDateDeparture = i_row['possibleDatesArrival'][0] + pd.DateOffset(days=int(i_row['daysMin']))
            v_possibleDatesArrivalUpdated = i_row['possibleDatesArrival'][i_row['possibleDatesArrival'] <= v_maxDateArrival]
            v_possibleDatesDepartureUpdated = i_row['possibleDatesDeparture'][i_row['possibleDatesDeparture'] >= v_minDateDeparture]
            v_specificationsDf.at[i_destId, 'possibleDatesArrival'] = v_possibleDatesArrivalUpdated
            v_specificationsDf.at[i_destId, 'possibleDatesDeparture'] = v_possibleDatesDepartureUpdated

        # filter from others based on possible dates
        v_specificationsDf['possibleDatesMin'] = [i_dateIndex[0] for i_dateIndex in v_specificationsDf['possibleDatesArrival']]
        v_specificationsDf['possibleDatesMax'] = [i_dateIndex[-1] for i_dateIndex in v_specificationsDf['possibleDatesDeparture']]
        v_specificationsDf.sort_values(by=['possibleDatesMin', 'possibleDatesMax'], ascending=[True, True])
        for i_destId in v_specificationsDf.index:
            i_row = v_specificationsDf.loc[i_destId, :]
            v_upperLimitDate = i_row['possibleDatesMin'] + pd.DateOffset(days=int(i_row['daysMin']) - 1)
            v_lowerLimitDate = i_row['possibleDatesMax'] - pd.DateOffset(days=int(i_row['daysMin']) - 1)
            if (v_upperLimitDate > v_lowerLimitDate):
                v_removeDays = pd.DatetimeIndex(start=v_lowerLimitDate, end=v_upperLimitDate, freq=pd.DateOffset(days=1))
                v_specificationsDf.loc[~(v_specificationsDf.index == i_destId), 'possibleDatesArrival'] = v_specificationsDf.loc[~(v_specificationsDf.index == i_destId), 'possibleDatesArrival'].apply(lambda i_index: i_index.drop(v_removeDays, errors='ignore'))
                v_specificationsDf.loc[~(v_specificationsDf.index == i_destId), 'possibleDatesDeparture'] = v_specificationsDf.loc[~(v_specificationsDf.index == i_destId), 'possibleDatesDeparture'].apply(lambda i_index: i_index.drop(v_removeDays, errors='ignore'))
        # todo: after this, possibleDates might have gaps. Check if minDays fit in all gaps. If not, remove dates where it does not fit.

        # filter based on daysMin, daysMax, arrival and departure AGAIN
        for i_destId in v_specificationsDf.index:
            i_row = v_specificationsDf.loc[i_destId, :]
            v_maxDateArrival = i_row['possibleDatesDeparture'][-1] - pd.DateOffset(days=int(i_row['daysMin']))
            v_minDateDeparture = i_row['possibleDatesArrival'][0] + pd.DateOffset(days=int(i_row['daysMin']))
            v_possibleDatesArrivalUpdated = i_row['possibleDatesArrival'][i_row['possibleDatesArrival'] <= v_maxDateArrival]
            v_possibleDatesDepartureUpdated = i_row['possibleDatesDeparture'][i_row['possibleDatesDeparture'] >= v_minDateDeparture]
            v_specificationsDf.at[i_destId, 'possibleDatesArrival'] = v_possibleDatesArrivalUpdated
            v_specificationsDf.at[i_destId, 'possibleDatesDeparture'] = v_possibleDatesDepartureUpdated

        v_specificationsDf['possibleDatesMin'] = [i_dateIndex[0] for i_dateIndex in v_specificationsDf['possibleDatesArrival']]
        v_specificationsDf['possibleDatesMax'] = [i_dateIndex[-1] for i_dateIndex in v_specificationsDf['possibleDatesDeparture']]

        return v_specificationsDf


    # todo: simplify the logic using possibleDatesDeparture (?)
    def __getTransportDates(self, v_destinationsDatesDf: pd.DataFrame, p_trip: Trip):

        v_transportDatesDict = {'destinationStart': [], 'destinationEnd': [], 'possibleDates': []}

        for i_destStartId in v_destinationsDatesDf.index:

            i_destStartRow = v_destinationsDatesDf.loc[i_destStartId, :]

            # check if possible starting destination
            if (i_destStartRow['possibleDatesMin'] <= p_trip.dateStartMax):
                i_destStartDateMax = i_destStartRow['possibleDatesMin'] + pd.DateOffset(days=int(i_destStartRow['daysMin']))
                v_transportDatesDict['destinationStart'].append(p_trip.destinationStart)
                v_transportDatesDict['destinationEnd'].append(i_destStartRow['destination'])
                v_transportDatesDict['possibleDates'].append(
                    pd.DatetimeIndex(start=i_destStartRow['possibleDatesMin'],
                                     end=min(p_trip.dateStartMax, i_destStartDateMax),
                                     freq=pd.DateOffset(days=1)
                                     )
                    )
            # check if possible final destination
            if (i_destStartRow['possibleDatesMax'] >= p_trip.dateEndMin):
                i_destEndDateMin = i_destStartRow['possibleDatesMin'] + pd.DateOffset(days=int(i_destStartRow['daysMin']))
                v_transportDatesDict['destinationStart'].append(i_destStartRow['destination'])
                v_transportDatesDict['destinationEnd'].append(p_trip.destinationStart)
                v_transportDatesDict['possibleDates'].append(
                    pd.DatetimeIndex(start=max(p_trip.dateEndMin, i_destEndDateMin),
                                     end=i_destStartRow['possibleDatesMax'],
                                     freq=pd.DateOffset(days=1)
                                     )
                    )

            i_minEndDest1 = i_destStartRow['possibleDatesMin'] + pd.DateOffset(days=int(i_destStartRow['daysMin']))
            i_finalDestinationsDf = v_destinationsDatesDf[(v_destinationsDatesDf['possibleDatesMin'] <= i_minEndDest1) &
                                                          (v_destinationsDatesDf['possibleDatesMax'] >= i_minEndDest1) &
                                                          (v_destinationsDatesDf.index != i_destStartId)]

            for i_destFinalId in i_finalDestinationsDf.index:
                # if(i_destStartRow['destination'].id)
                i_destFinalRow = i_finalDestinationsDf.loc[i_destFinalId, :]
                i_maxStartDest2 = i_destFinalRow['possibleDatesMax']-pd.DateOffset(days=int(i_destFinalRow['daysMin']))
                i_dateEnd = min(i_destStartRow['possibleDatesMax'] + pd.DateOffset(days=1), i_maxStartDest2)

                v_transportDatesDict['destinationStart'].append(i_destStartRow['destination'])
                v_transportDatesDict['destinationEnd'].append(i_destFinalRow['destination'])
                v_transportDatesDict['possibleDates'].append(pd.DatetimeIndex(start=i_minEndDest1,
                                                                                   end=i_dateEnd,
                                                                                   freq=pd.DateOffset(days=1)
                                                                                   )
                                                                  )

        v_transportDatesDf = pd.DataFrame(v_transportDatesDict)

        return v_transportDatesDf
