import pandas as pd
import numpy as np

from Models.Destination import Destination
from Models.DestinationSpecification import DestinationSpecification

class Trip():

    def __init__(self, p_id: int, p_name: str,
                 p_destinationStart: Destination, p_destinationsSpecifications: list,
                 p_dateStartMin: pd.Timestamp, p_dateStartMax: pd.Timestamp,
                 p_dateEndMin: pd.Timestamp = None, p_dateEndMax: pd.Timestamp = None,
                 p_accommodationRestrictions: list = None, p_transportRestrictions:list = None,
                 p_maxTransportWaitHours = 12, p_isRound = True
                 ):

        self.id = p_id
        self.name = p_name
        self.destinationsSpecifications = p_destinationsSpecifications
        self.dateStartMin = p_dateStartMin
        self.dateStartMax = p_dateStartMax
        self.dateEndMin = p_dateEndMin
        self.dateEndMax = p_dateEndMax
        self.destinationStart = p_destinationStart
        self.accommodationRestrictions = p_accommodationRestrictions
        self.transportRestrictions = p_transportRestrictions
        self.isRound = p_isRound
        self.maxTransportWaitHours = p_maxTransportWaitHours

        if((self.dateStartMax.hour == 0) and (self.dateStartMax.minute == 0)):
            self.dateStartMax = self.dateStartMax.replace(hour=23, minute=59, second=59)

        if(self.dateEndMin is None and self.dateEndMax is None):
            v_daysMinTrip = sum([i_specification.daysMin for i_specification in self.destinationsSpecifications])
            v_daysMaxTrip = sum([i_specification.daysMax for i_specification in self.destinationsSpecifications])
            self.dateEndMin = self.dateStartMin + pd.DateOffset(days=v_daysMinTrip)
            self.dateEndMax = self.dateStartMax + pd.DateOffset(days=v_daysMaxTrip)
            self.dateEndMax = self.dateEndMax.replace(hour=23, minute=59, second=59)

        self.__destinationsSpecificationsDf = None

        # todo: check trip consistency

    @property
    def destinationNumber(self):
        return len(self.destinationsSpecifications)

    @property
    def destinationsSpecificationsDf(self):
        if(self.__destinationsSpecificationsDf is None):
            self.__destinationsSpecificationsDf = DestinationSpecification.getSpecificationsDf(self.destinationsSpecifications)
        return self.__destinationsSpecificationsDf


    @staticmethod
    def generateTrip(p_numberDestinations: int = 3, p_ordered=False, p_daysStayMaxDiff = 0, p_dateStartDaysVariation=0, p_maxTransportWaitHours=8):

        v_destinationStart = Destination(p_id=999, p_name='destStart')
        v_destinations = []
        v_destinationSpecifications = []
        for i in range(p_numberDestinations):
            i_destination = Destination(p_id = i, p_name='dest' + str(i))
            v_destinations.append(i_destination)
            i_order = (p_ordered *(i+1)) or None
            i_destSpecification = DestinationSpecification(p_destination=i_destination, p_daysMax=4 + p_daysStayMaxDiff, p_daysMin=4,
                                                           p_dateMax = None, p_dateMin = None,
                                                           p_absoluteOrder = i_order, p_relativeOrderDestinations = None,
                                                           p_accommodationRequired= True
                                                           )
            v_destinationSpecifications.append(i_destSpecification)

        v_startDateMin = pd.Timestamp('2014-01-01')
        v_startDateMax = v_startDateMin + pd.DateOffset(days=p_dateStartDaysVariation)

        v_tripId = 1000*p_numberDestinations + 10*p_daysStayMaxDiff + 1*p_dateStartDaysVariation
        v_trip = Trip(p_id = v_tripId, p_name = 'randomTrip_id=' + str(v_tripId),
                      p_destinationStart = v_destinationStart, p_destinationsSpecifications = v_destinationSpecifications,
                      p_dateStartMin = v_startDateMin, p_dateStartMax = v_startDateMax,
                      p_dateEndMin = None, p_dateEndMax = None,
                      p_accommodationRestrictions = None, p_transportRestrictions = None,
                      p_maxTransportWaitHours = p_maxTransportWaitHours, p_isRound = True
                      )

        return v_trip


    @staticmethod
    def getSavedTrip(p_destinations: int, p_components: int, p_fetcherSeed: int, p_daysStay: int, p_approximated: bool, p_ordered: bool):

        from Scripts.problemData import loadProblemInstance

        v_fileName = 'trip-d' + str(p_destinations) + '-ds' + str(p_daysStay) + '-or'+ str(p_ordered)[0] + '-s0' + '-a' + str(p_components) + '-t' + str(p_components) + '-sf' + str(p_fetcherSeed)
        if(p_approximated):
            v_fileName += '-approx'
        v_fileName += '.json'

        return loadProblemInstance(v_fileName)

