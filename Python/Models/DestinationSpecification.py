import pandas as pd

from Models.Destination import Destination


class DestinationSpecification():

    def __init__(self, p_destination: Destination,
                 p_daysMax: int, p_daysMin: int,
                 p_dateMax: pd.Timestamp = None, p_dateMin: pd.Timestamp = None,
                 p_absoluteOrder: int = None, p_relativeOrderDestinations: list = None,
                 p_accommodationRequired: bool = True
                 ):
        """

        :param p_destination:
        :param p_daysMax:
        :param p_daysMin: Minimum days SLEEPING on the destination
        :param p_dateMax:
        :param p_dateMin:
        :param p_absoluteOrder:
        :param p_relativeOrderDestinations:
        :param p_accommodationRequired:
        """

        if(p_daysMin > p_daysMax):
            raise Exception('daysMax must be greater or equal daysMin')
        if((p_dateMin is not None) and (p_dateMax is not None) and (p_dateMin > p_dateMax)):
            raise Exception('dateMax must be greater or equal dateMin')

        self.id = p_destination.id
        self.destination = p_destination
        self.dateMax = p_dateMax
        self.dateMin = p_dateMin
        self.daysMax = p_daysMax
        self.daysMin = p_daysMin
        self.absoluteOrder = p_absoluteOrder
        self.relativeOrderDestinations = p_relativeOrderDestinations
        self.accommodationRequired = p_accommodationRequired


    @staticmethod
    def getSpecificationsDf(p_specificationsList):

        v_specificationsDf = pd.DataFrame(data={'absoluteOrder': [i_specification.absoluteOrder for i_specification in p_specificationsList],
                                                'dateMin': [i_specification.dateMin for i_specification in p_specificationsList],
                                                'dateMax': [i_specification.dateMax for i_specification in p_specificationsList],
                                                'daysMin': [int(i_specification.daysMin) for i_specification in p_specificationsList],
                                                'daysMax': [i_specification.daysMax for i_specification in p_specificationsList],
                                                'destination': [i_specification.destination for i_specification in p_specificationsList],
                                                'accommodationRequired': [i_specification.accommodationRequired for i_specification in p_specificationsList],
                                                },
                                          index=[i_specification.destination.id for i_specification in p_specificationsList]
                                          )
        v_specificationsDf.loc[:, ['daysMin', 'daysMax']] = v_specificationsDf.loc[:, ['daysMin', 'daysMax']].astype(int)

        return v_specificationsDf
