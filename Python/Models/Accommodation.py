import pandas as pd

class Accommodation():

    accommodationTypes = ('Hotel', 'Hostel','House','Apartment','Couchsurfing','Other')

    def __init__(self, p_id: int, p_name: str, p_destination, p_type, p_checkInTime: pd.Timestamp, p_checkOutTime: pd.Timestamp, p_reputation: float = None, p_location=None, p_company: str = None, p_priceSeries: pd.Series = None):
        if (p_type not in self.accommodationTypes):
            raise Exception('Wrong accommodation type')

        self.id = p_id
        self.name = p_name
        self.destination = p_destination
        self.type = p_type
        self.reputation = p_reputation
        self.location = p_location
        self.checkInTime = p_checkInTime
        self.checkOutTime = p_checkOutTime
        self.company = p_company
        self.priceSeries = p_priceSeries

