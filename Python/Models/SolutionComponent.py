from abc import ABC, abstractmethod
import pandas as pd
import numpy as np

class SolutionComponent(ABC):

    def __init__(self, p_id=None, p_isEmpty=False):

        self.id = p_id
        self.isEmpty = p_isEmpty

        self.dateStart = self._getDateStart()
        self.dateEnd = self._getDateEnd()
        self.destinationFinal = self._getDestinationFinal()
        self.destinationInitial = self._getDestinationInitial()
        self.timeValue = self._getTimeValue()
        self.costValue = self._getCostValue()


    @abstractmethod
    def _getDestinationFinal(self):
        raise Exception('Method not implemented')


    @abstractmethod
    def _getDestinationInitial(self):
        raise Exception('Method not implemented')


    @abstractmethod
    def _getCostValue(self):
        raise Exception('Method not implemented')


    @abstractmethod
    def _getTimeValue(self):
        raise Exception('Method not implemented')


    @abstractmethod
    def _getDateStart(self):
        raise Exception('Method not implemented')


    @abstractmethod
    def _getDateEnd(self):
        raise Exception('Method not implemented')


    def getObjectiveValue(self, p_objective):
        if(p_objective == 'time'):
            return self.timeValue
        elif(p_objective == 'cost'):
            return self.costValue
        else:
            raise Exception('Objective not implemented')


    @staticmethod
    def getComponentsDf(p_accommodationOptions, p_transportOptions, p_objectives, p_normalized: bool = True):
        """
        Get all accommodationOptions and transportOptions components and build a pandas DataFrame from them.
        :param p_accommodationOptions:
        :param p_transportOptions:
        :param p_objectives:
        :param p_normalized:
        :return:
        """
        v_ids = ['a' + str(i) for i in range(len(p_accommodationOptions))] + ['t' + str(i) for i in range(len(p_transportOptions))]

        v_mergedList = p_accommodationOptions + p_transportOptions

        v_dfInfo = {}
        v_dfInfo['type'] = (['accommodation'] * len(p_accommodationOptions)) + (['transport'] * len(p_transportOptions))
        v_dfInfo['dateStart'] = [i_component.dateStart for i_component in v_mergedList]
        v_dfInfo['dateEnd'] = [i_component.dateEnd for i_component in v_mergedList]
        v_dfInfo['destinationInitialId'] = [i_component.destinationInitial.id for i_component in v_mergedList]
        v_dfInfo['destinationFinalId'] = [i_component.destinationFinal.id for i_component in v_mergedList]

        v_normalizationRef = {}
        for i_obj in p_objectives:
            i_objValues = [i_component.getObjectiveValue(i_obj) for i_component in v_mergedList]
            if(p_normalized):
                i_maxValue = max(i_objValues)
                v_normalizationRef[i_obj] = i_maxValue
                i_objValues = [i_value/i_maxValue for i_value in i_objValues]
            v_dfInfo[i_obj] = i_objValues

        return pd.DataFrame(v_dfInfo, index=v_ids), v_normalizationRef

