import pandas as pd

class TransportBranch():

    transportTypes = ('Plane', 'Bus','Train')

    def __init__(self, p_type, p_timeDeparture: pd.Timestamp, p_timeArrival: pd.Timestamp, p_destinationDeparture, p_destinationArrival, p_id: int = None, p_stationDepature = None, p_stationArrival = None, p_company: str = None):

        if (p_type not in self.transportTypes):
            raise Exception('Wrong transport type')

        self.id = p_id
        self.type = p_type
        self.timeDeparture = p_timeDeparture
        self.timeArrival = p_timeArrival
        self.stationDepature = p_stationDepature
        self.stationArrival = p_stationArrival
        self.destinationDeparture = p_destinationDeparture
        self.destinationArrival = p_destinationArrival
        self.company = p_company


