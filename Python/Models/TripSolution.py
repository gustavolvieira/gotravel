import pandas as pd
import numpy as np
from collections import namedtuple

from Models.Trip import Trip
from Models.Util import getParetoIndexes
from Models.DestinationSpecification import DestinationSpecification

from Util.Timer import Timer


class TripSolution():

    def __init__(self, p_trip: Trip, p_componentsDf, p_objectives):
        """

        :param p_id:
        :param p_trip:
        :param p_componentsDf: pandas DataFrame of solutions components. Indexed by id and with columns: type, dateStart, dateEnd, destinationFinalId, destinationInitialId, timeValue, costValue
        :param p_objectives:
        """

        self.trip = p_trip
        self.objectives = p_objectives
        self.componentsDf = p_componentsDf

        self.unfeasible = None

        # self.componentsList = []
        self.__componentsIdsList = []

        self.__objectiveValues = {}
        self.__objOutdated = True


        self.__id = None
        self.__idOutdated = False


    @property
    def objectiveValues(self):
        if(self.__objOutdated):
            assert(len(self.componentsIdsList) > 0)
            for i_obj in self.objectives:
                self.__objectiveValues[i_obj] = self.componentsDf.loc[self.componentsIdsList, i_obj].values.sum()
            self.__objOutdated = False

        return self.__objectiveValues


    @property
    def id(self):
        if(self.__idOutdated):
            self.__id = '-'.join([str(i_id) for i_id in self.componentsIdsList])
            self.__idOutdated = False
        return self.__id


    @property
    def componentsIdsList(self):
        return self.__componentsIdsList


    def addComponent(self, p_componentId: str):
        self.__componentsIdsList.append(p_componentId)
        self.__idOutdated = True
        self.__objOutdated = True


    def replaceComponent(self, p_oldId: str, p_newId: str):
        self.__componentsIdsList[self.__componentsIdsList.index(p_oldId)] = p_newId
        self.__idOutdated = True
        self.__objOutdated = True


    def trimBeforeComponent(self, p_componentId):
        self.__componentsIdsList = self.__componentsIdsList[:self.__componentsIdsList.index(p_componentId)]
        self.__idOutdated = True
        self.__objOutdated = True


    def isComplete(self):
        if(self.unfeasible):
            return False
        elif(self.trip.isRound):
            return len(self.componentsIdsList) == len(self.trip.destinationsSpecifications)*2 + 1
        else:
            return len(self.componentsIdsList) == len(self.trip.destinationsSpecifications)*2


    def isFeasible(self):
        v_feasible = True
        v_message = self.id
        if(not self.isComplete()):
            v_feasible = False
            v_message += ' - Not complete'

        # check feasible components
        v_testSolution = TripSolution(p_trip= self.trip, p_componentsDf=self.componentsDf, p_objectives=self.objectives)
        for i_componentId in self.componentsIdsList:
            i_feasibleComponentsIds = v_testSolution.getFeasibleComponentsIds()
            if(i_componentId not in i_feasibleComponentsIds):
                v_feasible = False
                v_message += ' - Unfeasible component: ' + i_componentId
            v_testSolution.addComponent(i_componentId)

        print(v_message)
        return v_feasible


    def getFeasibleComponentsIds(self, p_feasibleComponentsDict = None):

        if((p_feasibleComponentsDict is not None) and (self.id in p_feasibleComponentsDict.keys())):
            return p_feasibleComponentsDict[self.id]

        v_specificationsDf = self.trip.destinationsSpecificationsDf
        if(len(self.componentsIdsList) == 0):
            v_feasibleComponents = self.__getInitialComponent(v_specificationsDf)
        else:
            v_lastComponent = self.componentsDf.loc[self.componentsIdsList[-1]]
            v_currentDate = v_lastComponent['dateEnd']
            v_currentDestinationId = v_lastComponent['destinationFinalId']

            if(v_lastComponent['type'] == 'transport'):
                v_feasibleComponents = self.__getFeasibleAccommodationsImproved(p_currentDate=v_currentDate,
                                                                        p_currentDestinationId=v_currentDestinationId
                                                                        )
            else:
                v_feasibleComponents = self.__getFeasibleTransportsImproved(p_currentDate=v_currentDate,
                                                                         p_currentDestinationId=v_currentDestinationId,
                                                                         p_destinationSpecificationsDf = v_specificationsDf
                                                                         )

        if (p_feasibleComponentsDict is not None):
            p_feasibleComponentsDict[self.id] = v_feasibleComponents.index.values.copy()
            return p_feasibleComponentsDict[self.id]
        else:
            return v_feasibleComponents.index.values.copy()


    # testgetComponentsImproved
    # def getFeasibleComponentsIds(self, p_feasibleComponentsDict = None):
    #
    #     if((p_feasibleComponentsDict is not None) and (self.id in p_feasibleComponentsDict.keys())):
    #         return p_feasibleComponentsDict[self.id]
    #
    #     v_specificationsDf = self.trip.destinationsSpecificationsDf
    #     if(len(self.componentsIdsList) == 0):
    #         v_feasibleComponents = self.__getInitialComponent(v_specificationsDf)
    #     else:
    #         v_lastComponent = self.componentsDf.loc[self.componentsIdsList[-1]]
    #         v_currentDate = v_lastComponent['dateEnd']
    #         v_currentDestinationId = v_lastComponent['destinationFinalId']
    #
    #         if(v_lastComponent['type'] == 'transport'):
    #             v_feasibleComponents = self.__getFeasibleAccommodations(p_currentDate=v_currentDate,
    #                                                                     p_currentDestinationId=v_currentDestinationId
    #                                                                     )
    #             v_feasibleComponents2 = self.__getFeasibleAccommodationsImproved(p_currentDate=v_currentDate,
    #                                                                     p_currentDestinationId=v_currentDestinationId
    #                                                                     )
    #         else:
    #             v_feasibleComponents = self.__getFeasibleTransports(p_currentDate=v_currentDate,
    #                                                                      p_currentDestinationId=v_currentDestinationId,
    #                                                                      p_destinationSpecificationsDf=v_specificationsDf
    #                                                                      )
    #             v_feasibleComponents2 = self.__getFeasibleTransportsImproved(p_currentDate=v_currentDate,
    #                                                                      p_currentDestinationId=v_currentDestinationId,
    #                                                                      p_destinationSpecificationsDf = v_specificationsDf
    #                                                                      )
    #         assert((v_feasibleComponents.index.values == v_feasibleComponents2.index.values).all())
    #         v_feasibleComponents = v_feasibleComponents2
    #
    #     if (p_feasibleComponentsDict is not None):
    #         p_feasibleComponentsDict[self.id] = v_feasibleComponents.index.values.copy()
    #         return p_feasibleComponentsDict[self.id]
    #     else:
    #         return v_feasibleComponents.index.values.copy()


    @staticmethod
    def getFeasibleComponentsAlternativesIds(p_tripSolution, p_alternativeId: str):

        v_testSolution = TripSolution(p_trip= p_tripSolution.trip, p_componentsDf=p_tripSolution.componentsDf, p_objectives=p_tripSolution.objectives)
        v_partialComponents = p_tripSolution.componentsIdsList[:p_tripSolution.componentsIdsList.index(p_alternativeId)].copy()
        for i_componentId in v_partialComponents:
            v_testSolution.addComponent(i_componentId)

        v_alternativeComponentsIds = list(v_testSolution.getFeasibleComponentsIds())
        v_alternativeComponentsIds.remove(p_alternativeId)
        # v_alternativeComponentsIds = [i_componentId for i_componentId in v_alternativeComponentsIds if i_componentId != p_alternativeId]

        # if(p_tripSolution.componentsIdsList.index(p_alternativeId) <= len(p_tripSolution.componentsIdsList) - 2):
        if(p_tripSolution.componentsIdsList[-1] != p_alternativeId):

            v_nextComponentId = p_tripSolution.componentsIdsList[p_tripSolution.componentsIdsList.index(p_alternativeId)+1]
            v_alternativeComponentsIdsCopy = v_alternativeComponentsIds.copy()
            for i_alternativeId in v_alternativeComponentsIdsCopy:
                i_alternativeSolution = v_testSolution.copyCustom()
                i_alternativeSolution.addComponent(i_alternativeId)

                i_feasibleNextComponents = i_alternativeSolution.getFeasibleComponentsIds()
                if(v_nextComponentId not in i_feasibleNextComponents):
                    v_alternativeComponentsIds.remove(i_alternativeId)

        return v_alternativeComponentsIds


    def __getInitialComponent(self, p_destinationSpecificationsDf):
        v_feasibleComponents = self.componentsDf[self.componentsDf['type'] == 'transport']
        v_feasibleComponents = v_feasibleComponents[(v_feasibleComponents['dateStart'] >= self.trip.dateStartMin) &
                                                    (v_feasibleComponents['dateStart'] <= self.trip.dateStartMax)]
        v_feasibleComponents = v_feasibleComponents[v_feasibleComponents['destinationFinalId'] != self.trip.destinationStart.id]

        if (1 in p_destinationSpecificationsDf['absoluteOrder'].values):
            v_nextDestinationId = int(p_destinationSpecificationsDf[p_destinationSpecificationsDf['absoluteOrder'] == 1].index.values[0])
            v_feasibleComponents = v_feasibleComponents[v_feasibleComponents['destinationFinalId'] == v_nextDestinationId]
        else:
            v_destinationsWithoutOrderIds = p_destinationSpecificationsDf[p_destinationSpecificationsDf['absoluteOrder'].values == None].index.values
            v_feasibleComponents = v_feasibleComponents[v_feasibleComponents['destinationFinalId'].isin(v_destinationsWithoutOrderIds)]

        # TODO: continue for relative order

        return v_feasibleComponents


    def __getFeasibleAccommodations(self, p_currentDate, p_currentDestinationId):
        v_feasibleComponents = self.componentsDf[self.componentsDf['type'] == 'accommodation']
        v_feasibleComponents = v_feasibleComponents[[i.date() == p_currentDate.date() for i in v_feasibleComponents['dateStart']]]
        v_feasibleComponents = v_feasibleComponents[v_feasibleComponents['destinationInitialId'] == p_currentDestinationId]
        return v_feasibleComponents


    def __getFeasibleAccommodationsImproved(self, p_currentDate, p_currentDestinationId):

        v_feasibleIds = getFeasibleAccommodationsImprovedAux(p_currentDate = np.datetime64(p_currentDate.date()),
                                       p_currentDestinationId=p_currentDestinationId,
                                       p_idsArray = self.componentsDf.index.values,
                                       p_dateStartArray = self.componentsDf['dateStart'].values,
                                       p_typeArray = self.componentsDf['type'].values,
                                       p_destinationInitArray = self.componentsDf['destinationInitialId'].values
                                       )

        v_feasibleComponents = self.componentsDf.loc[v_feasibleIds]
        return v_feasibleComponents


    def __getFeasibleTransportsImproved(self, p_currentDate, p_currentDestinationId, p_destinationSpecificationsDf):

        v_feasibleIds = getFeasibleTransportsImprovedAux(p_currentDate = np.datetime64(p_currentDate.date()),
                                                        p_currentDestinationId=p_currentDestinationId,
                                                        p_idsArray = self.componentsDf.index.values,
                                                        p_dateStartArray = self.componentsDf['dateStart'].values,
                                                        p_typeArray = self.componentsDf['type'].values,
                                                        p_destinationInitArray = self.componentsDf['destinationInitialId'].values,
                                                        p_destinationFinalArray = self.componentsDf['destinationFinalId'].values,
                                                        p_visitedDestinationsIds= list(set(self.componentsDf.loc[self.componentsIdsList, 'destinationFinalId'].values)),
                                                        p_roundTrip= self.trip.isRound,
                                                        p_destinationStartId = self.trip.destinationStart.id,
                                                        p_dateDepartureMax = np.datetime64(p_currentDate + pd.Timedelta(hours=self.trip.maxTransportWaitHours)),
                                                        p_specificationsOrderArray= p_destinationSpecificationsDf['absoluteOrder'].values,
                                                        p_specificationsDestIdsArray= p_destinationSpecificationsDf.index.values
                                                        )

        v_feasibleComponents = self.componentsDf.loc[v_feasibleIds]
        return v_feasibleComponents


    def __getFeasibleTransports(self, p_currentDate, p_currentDestinationId, p_destinationSpecificationsDf):
        v_feasibleComponents = self.componentsDf[self.componentsDf['type'] == 'transport']
        v_feasibleComponents = v_feasibleComponents[v_feasibleComponents['destinationInitialId'] == p_currentDestinationId]

        v_visitedDestinationsIds = set(self.componentsDf.loc[self.componentsIdsList, 'destinationFinalId'].values)
        v_feasibleComponents = v_feasibleComponents[~v_feasibleComponents['destinationFinalId'].isin(v_visitedDestinationsIds)]

        if(self.trip.isRound and (len(v_visitedDestinationsIds)==len(self.trip.destinationsSpecifications))):
            v_feasibleComponents = v_feasibleComponents[v_feasibleComponents['destinationFinalId'] == self.trip.destinationStart.id]
            v_returnTransport = True
        else:
            v_feasibleComponents = v_feasibleComponents[v_feasibleComponents['destinationFinalId'] != self.trip.destinationStart.id]
            v_returnTransport = False

        v_dateDepartureMax = p_currentDate + pd.Timedelta(hours=self.trip.maxTransportWaitHours)
        v_feasibleComponents = v_feasibleComponents[(v_feasibleComponents['dateStart'] >= p_currentDate) &
                                                    (v_feasibleComponents['dateStart'] <= v_dateDepartureMax)]

        if(not v_returnTransport):
            v_nextDestinationOrder = len(v_visitedDestinationsIds) + 1
            if(v_nextDestinationOrder in p_destinationSpecificationsDf['absoluteOrder'].values):
                v_nextDestinationId = int(p_destinationSpecificationsDf[p_destinationSpecificationsDf['absoluteOrder'] == v_nextDestinationOrder].index.values[0])
                v_feasibleComponents = v_feasibleComponents[v_feasibleComponents['destinationFinalId'] == v_nextDestinationId]
            else:
                v_destinationsWithoutOrderIds = p_destinationSpecificationsDf[p_destinationSpecificationsDf['absoluteOrder'].values == None].index.values
                v_feasibleComponents = v_feasibleComponents[v_feasibleComponents['destinationFinalId'].isin(v_destinationsWithoutOrderIds)]

        # TODO: continue for relative order

        return v_feasibleComponents


    def getDestinationString(self, p_componentsDf):

        v_destinationDict = {}
        for i_specification in self.trip.destinationsSpecifications:
            i_destId = i_specification.destination.id
            i_destName = i_specification.destination.name
            v_destinationDict[i_destId] = i_destName
        v_destinationDict[self.trip.destinationStart.id] = self.trip.destinationStart.name

        v_destinationNames = [self.trip.destinationStart.name]
        for i_id in self.componentsIdsList:
            i_component = p_componentsDf.loc[i_id]
            if(i_component['type'] == 'accommodation'):
                continue

            v_destinationId = i_component['destinationFinalId']
            i_destinationName = v_destinationDict[v_destinationId]
            v_destinationNames.append(i_destinationName)

        return '->'.join(v_destinationNames)


    @staticmethod
    def updateParetoSet(p_solutionsList: list, p_newSolutions: list):

        # p_solutionsList = TripSolution.getUniqueSolutions(p_solutionsList)
        p_newSolutions = TripSolution.getUniqueSolutions(p_newSolutions)

        v_originalSolutionsIds = [i_solution.id for i_solution in p_solutionsList]
        assert(len(v_originalSolutionsIds) == len(set(v_originalSolutionsIds)))

        v_newSolutionsFiltered = [i_solution for i_solution in p_newSolutions if i_solution.id not in v_originalSolutionsIds]

        v_aggSolutions = p_solutionsList + v_newSolutionsFiltered
        v_aggSolutionsDf = TripSolution.builtSolutionsDf(v_aggSolutions)

        v_paretoIds = v_aggSolutionsDf.iloc[getParetoIndexes(v_aggSolutionsDf.loc[:, ['cost', 'time']].values)].index
        v_paretoSolutions = [i_solution for i_solution in v_aggSolutions if i_solution.id in v_paretoIds]

        v_addedSolutions = [i_solution for i_solution in v_newSolutionsFiltered if i_solution.id in v_paretoIds]
        # v_newSolutionsNumber = len(v_paretoSolutions) - np.array([i_id in v_originalSolutionsIds for i_id in v_paretoIds]).sum()

        return v_paretoSolutions, v_addedSolutions


    @staticmethod
    def updateParetoDf(p_solutionsDf: pd.DataFrame, p_newSolutionsDf: pd.DataFrame):

        # p_solutionsList = TripSolution.getUniqueSolutions(p_solutionsList)
        p_newSolutionsDf.drop_duplicates(inplace=True)

        v_originalSolutionsIds = list(p_solutionsDf.index)
        assert(len(v_originalSolutionsIds) == len(set(v_originalSolutionsIds)))

        v_newSolutionsFilteredDf = p_newSolutionsDf[~p_newSolutionsDf.index.isin(v_originalSolutionsIds)]

        v_aggSolutionsDf = pd.concat([p_solutionsDf, v_newSolutionsFilteredDf])

        v_paretoIds = v_aggSolutionsDf.iloc[getParetoIndexes(v_aggSolutionsDf.loc[:, ['cost', 'time']].values)].index
        v_paretoDf = v_aggSolutionsDf[v_aggSolutionsDf.index.isin(v_paretoIds)]

        v_addedSolutionsDf = v_newSolutionsFilteredDf[v_newSolutionsFilteredDf.index.isin(v_paretoIds)]
        # v_newSolutionsNumber = len(v_paretoSolutions) - np.array([i_id in v_originalSolutionsIds for i_id in v_paretoIds]).sum()

        return v_paretoDf, v_addedSolutionsDf


    @staticmethod
    def getUniqueSolutions(p_solutionList: list):

        v_uniqueSolutions = []
        v_uniqueIds = []

        for i_solution in p_solutionList:
            if(i_solution.id not in v_uniqueIds):
                v_uniqueIds.append(i_solution.id)
                v_uniqueSolutions.append(i_solution)

        return v_uniqueSolutions


    @staticmethod
    def calcHypervolume(p_nadirPoint: dict, p_solutionsList: list = None, p_solutionsDf: pd.DataFrame = None):
        assert ((p_solutionsList is None) or (p_solutionsDf is None))
        assert ((p_solutionsList is not None) or (p_solutionsDf is not None))

        if(p_solutionsDf is None):
            v_solutionsDf = TripSolution.builtSolutionsDf(p_solutionsList)
        elif('destinations' in p_solutionsDf.columns):
            v_solutionsDf = p_solutionsDf.drop(['destinations'], axis =1)
        else:
            v_solutionsDf = p_solutionsDf

        if(len(v_solutionsDf.columns) == 2):
            return TripSolution.__calcHypervolume2d(v_solutionsDf, p_nadirPoint)
        else:
            raise ('Hypervolume not implemented for dimentions higher than 2')


    @staticmethod
    def __calcHypervolume2d(p_solutionsDf: pd.DataFrame, p_nadirPoint: dict):

        v_obj1, v_obj2 = p_solutionsDf.columns
        p_solutionsDf.sort_values(by=v_obj2, axis=0, ascending=False, inplace=True)

        v_referenceValueObj2 = p_nadirPoint[v_obj2]
        v_hypervolume = 0

        for i_index, i_row in p_solutionsDf.iterrows():
            v_hypervolume += (p_nadirPoint[v_obj1] - i_row[v_obj1]) * (v_referenceValueObj2 - i_row[v_obj2])
            v_referenceValueObj2 = i_row[v_obj2]

        return v_hypervolume


    @staticmethod
    def __removeDuplicates(p_solutionsList, p_newSolutions):
        v_originalSolutionsIds = [i_solution.id for i_solution in p_solutionsList]
        p_newSolutions = [i_solution for i_solution in p_newSolutions if i_solution.id not in v_originalSolutionsIds]
        return p_newSolutions


    @staticmethod
    def builtSolutionsDf(p_solutionsList, p_componentsDf=None):

        v_ids = []
        v_values = []
        for i_solution in p_solutionsList:
            v_ids.append(i_solution.id)
            v_values.append(list(i_solution.objectiveValues.values()))
            if(p_componentsDf is not None):
                v_values[-1].append(i_solution.getDestinationString(p_componentsDf))

        if (p_componentsDf is None):
            v_df = pd.DataFrame(data=v_values, index=v_ids, columns=list(p_solutionsList[0].objectives))
        else:
            v_df = pd.DataFrame(data=v_values, index=v_ids, columns=list(p_solutionsList[0].objectives) + ['destinations'])

        return v_df


    def copyCustom(self):
        v_newSolution = TripSolution(p_trip=self.trip, p_componentsDf=self.componentsDf, p_objectives=self.objectives)
        v_newSolution.__componentsIdsList = self.componentsIdsList.copy()
        v_newSolution.__idOutdated = self.__idOutdated
        v_newSolution.__id = self.__id
        return v_newSolution


    @staticmethod
    def getAllSolutions(p_trip, p_componentsDf, p_paretoOnly = False):

        Return = namedtuple('return_getAllSolutions', ['solutionsList', 'numberSolutions'])

        v_objectives = ['cost', 'time']
        v_tripSolution = TripSolution(p_trip=p_trip, p_componentsDf=p_componentsDf, p_objectives=v_objectives)

        v_returnDict = getAllSolutions_dispy(v_tripSolution, p_paretoOnly=p_paretoOnly, p_componentsDf=p_componentsDf)
        v_allSolutions = v_returnDict['solutionsList']

        if(p_paretoOnly):
            v_allSolutions, _ = TripSolution.updateParetoSet([], v_allSolutions)

        return Return(solutionsList= v_allSolutions, numberSolutions=v_returnDict['numberSolutions'])


    @staticmethod
    def getAllSolutionsParallel(p_trip, p_componentsDf, p_verbose=True, p_paretoOnly=True):

        Return = namedtuple('return_getAllSolutions', ['solutionsList', 'numberSolutions'])

        import dispy, dispy.httpd
        import os

        # dispy initialization
        v_localIp = '192.168.0.28'
        v_nodesIps = ['192.168.0.28']
        ROOT_DIR = os.getcwd().split('Python')[0] + 'Python'
        v_cluster = dispy.JobCluster(getAllSolutions_dispy, nodes=v_nodesIps, ip_addr=v_localIp, secret='351786', depends=[], dest_path=ROOT_DIR)
        v_httpServer = dispy.httpd.DispyHTTPServer(v_cluster, host=v_localIp)
        v_jobs = []

        v_allSolutions = []
        v_objectives = ['cost', 'time']
        v_emptySolution = TripSolution(p_trip=p_trip, p_componentsDf=p_componentsDf, p_objectives=v_objectives)
        v_partialSolutions = getNextSolutions(v_emptySolution)
        v_numberSolutions = 0

        i_jobId = 0
        for i_solutionPartial in v_partialSolutions:
            i_job = v_cluster.submit(i_solutionPartial, p_paretoOnly, p_componentsDf)
            i_job.id = i_jobId
            i_jobId += 1
            v_jobs.append(i_job)

        # run jobs
        for i_job in v_jobs:
            if (p_verbose):
                print('Waiting for job ' + str(i_job.id) + ' of ' + str(len(v_jobs)))
            i_returnDict = i_job()
            i_solutions = i_returnDict['solutionsList']
            v_allSolutions.extend(i_solutions)
            v_numberSolutions += i_returnDict['numberSolutions']

        if (p_paretoOnly):
            v_allSolutions, _ = TripSolution.updateParetoSet([], v_allSolutions)

        # dispy closure
        v_cluster.print_status()
        v_cluster.close()
        v_httpServer.shutdown(wait=False)

        return Return(solutionsList= v_allSolutions, numberSolutions=v_numberSolutions)


def getNextSolutions(p_solutionPartial):
    v_nextSolutions = []
    v_feasibleStepsIds = p_solutionPartial.getFeasibleComponentsIds()
    for i_componentId in v_feasibleStepsIds:
        i_newSolution = p_solutionPartial.copyCustom()
        i_newSolution.addComponent(i_componentId)
        v_nextSolutions.append(i_newSolution)

    return v_nextSolutions


def getAllSolutions_dispy(p_solutionPartial, p_paretoOnly, p_componentsDf):
    from Models.TripSolution import TripSolution

    v_objectives = p_solutionPartial.objectives
    v_solutionsPartialQueue = [p_solutionPartial.copyCustom()]
    v_solutionsComplete = []

    while (len(v_solutionsPartialQueue) > 0):
        i_solutionPartial = v_solutionsPartialQueue.pop()

        i_nextSolutions = []
        v_feasibleStepsIds = i_solutionPartial.getFeasibleComponentsIds()
        for i_componentId in v_feasibleStepsIds:
            i_newSolution = i_solutionPartial.copyCustom()
            i_newSolution.addComponent(i_componentId)
            i_nextSolutions.append(i_newSolution)

        for i_solution in i_nextSolutions:
            if (i_solution.isComplete()):
                v_solutionsComplete.append(i_solution)
            else:
                v_solutionsPartialQueue.append(i_solution)

    v_numberSolutions = len(v_solutionsComplete)
    if(v_numberSolutions > 0):
        v_solutionsDf = TripSolution.builtSolutionsDf(v_solutionsComplete, p_componentsDf)
        if (p_paretoOnly):
            v_solutionsComplete, _ = TripSolution.updateParetoSet([], v_solutionsComplete)
    else:
        v_nadirPoint = {}
        for i_obj in v_objectives:
            v_nadirPoint[i_obj] = 0

    return dict(solutionsList=v_solutionsComplete, numberSolutions=v_numberSolutions)


def getFeasibleAccommodationsImprovedAux(p_currentDate: np.datetime64, p_currentDestinationId: int, p_idsArray: np.array, p_dateStartArray: np.array, p_typeArray: np.array, p_destinationInitArray: np.array):

    # filter type
    p_idsArray = p_idsArray[p_typeArray == 'accommodation']
    p_dateStartArray = p_dateStartArray[p_typeArray == 'accommodation']
    p_destinationInitArray = p_destinationInitArray[p_typeArray == 'accommodation']

    # filter dates
    p_idsArray = p_idsArray[p_dateStartArray == p_currentDate]
    p_destinationInitArray = p_destinationInitArray[p_dateStartArray == p_currentDate]

    # filter destination
    p_idsArray = p_idsArray[p_destinationInitArray == p_currentDestinationId]

    return p_idsArray


def getFeasibleTransportsImprovedAux(p_currentDate: np.datetime64, p_currentDestinationId: int, p_idsArray: np.array, p_dateStartArray: np.array, p_typeArray: np.array, p_destinationInitArray: np.array, p_destinationFinalArray: np.array, p_visitedDestinationsIds : list, p_roundTrip: bool, p_destinationStartId: int, p_dateDepartureMax: np.datetime64, p_specificationsOrderArray: np.array, p_specificationsDestIdsArray: np.array):

    # filter type
    p_idsArray = p_idsArray[p_typeArray == 'transport']
    p_dateStartArray = p_dateStartArray[p_typeArray == 'transport']
    p_destinationInitArray = p_destinationInitArray[p_typeArray == 'transport']
    p_destinationFinalArray = p_destinationFinalArray[p_typeArray == 'transport']

    # filterCurrentDestination
    p_idsArray = p_idsArray[p_destinationInitArray == p_currentDestinationId]
    p_dateStartArray = p_dateStartArray[p_destinationInitArray == p_currentDestinationId]
    p_destinationFinalArray = p_destinationFinalArray[p_destinationInitArray == p_currentDestinationId]

    # filter visited ids
    p_idsArray = p_idsArray[np.isin(p_destinationFinalArray, p_visitedDestinationsIds, invert=True)]
    p_dateStartArray = p_dateStartArray[np.isin(p_destinationFinalArray, p_visitedDestinationsIds, invert=True)]
    p_destinationFinalArray = p_destinationFinalArray[np.isin(p_destinationFinalArray, p_visitedDestinationsIds, invert=True)]

    # filter starting id
    if (p_roundTrip and (len(p_visitedDestinationsIds) == len(p_specificationsDestIdsArray))):
        p_idsArray = p_idsArray[p_destinationFinalArray == p_destinationStartId]
        p_dateStartArray= p_dateStartArray[p_destinationFinalArray == p_destinationStartId]
        p_destinationFinalArray = p_destinationFinalArray[p_destinationFinalArray == p_destinationStartId]
        v_returnTransport = True
    else:
        p_idsArray = p_idsArray[p_destinationFinalArray != p_destinationStartId]
        p_dateStartArray = p_dateStartArray[p_destinationFinalArray != p_destinationStartId]
        p_destinationFinalArray = p_destinationFinalArray[p_destinationFinalArray != p_destinationStartId]
        v_returnTransport = False

    # filter departure time
    p_idsArray = p_idsArray[(p_dateStartArray >= p_currentDate) & (p_dateStartArray <= p_dateDepartureMax)]
    p_destinationFinalArray = p_destinationFinalArray[(p_dateStartArray >= p_currentDate) & (p_dateStartArray <= p_dateDepartureMax)]

    # filter absolute destination order
    if(not v_returnTransport):
        v_nextDestinationOrder = len(p_visitedDestinationsIds) + 1
        if (v_nextDestinationOrder in p_specificationsOrderArray):
            v_nextDestinationId = int(p_specificationsDestIdsArray[p_specificationsOrderArray == v_nextDestinationOrder][0])
            p_idsArray = p_idsArray[p_destinationFinalArray == v_nextDestinationId]
        else:
            v_destinationsWithoutOrderIds = p_specificationsDestIdsArray[p_specificationsOrderArray == None]
            p_idsArray = p_idsArray[np.isin(p_destinationFinalArray, v_destinationsWithoutOrderIds)]

    return p_idsArray