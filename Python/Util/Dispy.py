import os
import dispy, dispy.httpd


class DispyAux():

    def __init__(self, p_localIp: str = '192.168.0.28', p_nodesIps: list = None, ):

        self.localIp = p_localIp
        self.path = os.getcwd().split('Python')[0] + 'Python'

        if(p_nodesIps is not None):
            self.nodesIps = p_nodesIps
        else:
            self.nodesIps = [self.localIp]


    def initDispy(self, p_jobFunc):
        self.cluster = dispy.JobCluster(p_jobFunc, nodes=self.nodesIps, ip_addr=self.localIp, secret='351786', depends=[], dest_path=self.path)
        self.httpServer = dispy.httpd.DispyHTTPServer(self.cluster, host=self.localIp)


    def closeDispy(self):
        self.cluster.print_status()
        self.cluster.close()
        self.httpServer.shutdown(wait=False)