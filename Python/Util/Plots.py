import plotly.graph_objs as py_go
import plotly.offline as py_off
import pandas as pd
import numpy as np
import colorlover as cl

class CustomPlots():

    colorsMain = cl.scales['9']['qual']['Set1']
    colorsPastel = cl.scales['9']['qual']['Pastel1']

    def __init__(self, p_notebook=False):

        self.colorsPastel = [i_colorString.replace('rgb', 'rgba').replace(')', ',0.25)') for i_colorString in self.colorsPastel]

        if(p_notebook):
            self.plot = py_off.iplot
            py_off.init_notebook_mode()
        else:
            self.plot = py_off.plot


    def plotSolutions(self, p_paretoEstimatedDf: pd.DataFrame, p_allSolutionsDf: pd.DataFrame = None, p_nadirPoint: dict = None, p_paretoRealDf: pd.DataFrame = None, p_fileName = 'temp-solutions.html'):
        # todo: include more solutions info
        p_paretoEstimatedDf.sort_values(by=list(p_paretoEstimatedDf.columns), inplace=True)
        if(p_allSolutionsDf is not None):
            p_allSolutionsDf = p_allSolutionsDf[~p_allSolutionsDf.index.isin(p_paretoEstimatedDf.index)]
            if (p_paretoRealDf is not None):
                p_allSolutionsDf = p_allSolutionsDf[~p_allSolutionsDf.index.isin(p_paretoRealDf.index)]
        if (p_paretoRealDf is not None):
            p_paretoRealDf.sort_values(by=list(p_paretoRealDf.columns), inplace=True)

        self.__plotSolutions2d(p_paretoEstimatedDf, p_allSolutionsDf, p_nadirPoint, p_paretoRealDf, p_fileName)


    def __plotSolutions2d(self, p_paretoEstimatedDf: pd.DataFrame, p_allSolutionsDf: pd.DataFrame = None, p_nadirPoint: dict = None, p_paretoRealDf: pd.DataFrame = None, p_fileName = 'temp-Solutions'):

        v_objX, v_objY = 'cost', 'time'
        v_plotData = []
        if(p_allSolutionsDf is not None):
            v_textAllSolution = np.full(len(p_allSolutionsDf), '<b>destinations:</b> ') + p_allSolutionsDf.loc[:, 'destinations'].values + np.full(len(p_allSolutionsDf), '<br><b>components:</b> ') + p_allSolutionsDf.index.values
            v_plotData.append(py_go.Scatter(
                x=p_allSolutionsDf[v_objX],
                y=p_allSolutionsDf[v_objY],
                fill='None',
                mode='markers',
                name='All Solutions',
                text= v_textAllSolution,
                marker={'size': 5,
                        'color': 'rgba(180, 170, 170, 0.7)'
                        },
                hoverinfo='x+y+text'
            ))

        if (p_nadirPoint is not None):
            v_plotData.append(py_go.Scatter(
                x=[p_nadirPoint[v_objX]],
                y=[p_nadirPoint[v_objY]],
                mode='markers',
                name='NadirPoint',
                marker={'size': 12,
                        'color': 'rgba(0, 30, 120, 1)',
                        'symbol': 'x'
                        },
                visible= "legendonly"
            ))

        v_textParetoSolution = np.full(len(p_paretoEstimatedDf), '<b>destinations:</b> ') + p_paretoEstimatedDf.loc[:, 'destinations'].values + np.full(len(p_paretoEstimatedDf), '<br><b>components:</b> ') + p_paretoEstimatedDf.index.values
        v_plotData.append(py_go.Scatter(
            x=p_paretoEstimatedDf[v_objX],
            y=p_paretoEstimatedDf[v_objY],
            fill='None',
            mode='lines+markers',
            name='Pareto Solutions',
            text=v_textParetoSolution,
            line={'color': 'rgba(190,10,10, 0.8)',
                  'width': 1.5,
                  'dash': 'solid',
                  'shape': 'hv'
                  },
            marker={'size': 14,
                    'color': 'rgba(190,10,10,1)',
                    'symbol': 'diamond'
                    },
            hoverinfo='x+y+text'
        ))

        if (p_paretoRealDf is not None):
            v_textParetoOptimal = np.full(len(p_paretoRealDf), '<b>destinations:</b> ') + p_paretoRealDf.loc[:, 'destinations'].values + np.full(len(p_paretoRealDf), '<br><b>components:</b> ') + p_paretoRealDf.index.values
            v_plotData.append(py_go.Scatter(
                x=p_paretoRealDf[v_objX],
                y=p_paretoRealDf[v_objY],
                fill='None',
                mode='lines+markers',
                name='Estimated Pareto Optimal Solutions',
                text=v_textParetoOptimal,
                line={'color': 'rgba(50,50,50, 0.2)',
                      'width': 7,
                      'dash': 'solid',
                      'shape': 'hv'
                      },
                marker={'size': 6,
                        'color': 'rgba(40,40,40,1)',
                        'symbol': 'diamond'
                        },
                hoverinfo='x+y+text'
            ))

        v_layout = {'title': 'Trip Solutions',
                    'xaxis': {'title': v_objX.capitalize() + ' (R$)'},
                    'yaxis': {'title': v_objY.capitalize() + ' (Hours)'},
                    'showlegend': True
                    }
        v_fig = {'data': v_plotData,
                 'layout': v_layout
                 }

        self.plot(v_fig, filename=p_fileName)


    def plotHypervolumeSeries(self, p_seriesList: list, p_fileName = 'temp-hypervolume.html'):

        v_plotData = []
        for i_hvSeries in p_seriesList:
            v_plotData.append(py_go.Scatter(
                x=i_hvSeries.index,
                y=i_hvSeries.values,
                fill='None',
                mode='lines+markers',
                name=i_hvSeries.name,
                line={'width': 1.5,
                      #'color': 'rgba(190,10,10, 0.8)',
                      'dash': 'solid',
                      'shape': 'hv'
                      },
                marker={'size': 5,
                        #'color': 'rgba(240,180,20,1)',
                        #'symbol': 'diamond'
                        },
            ))

        v_layout = {'title': 'Trip Solutions',
                    'xaxis': {'title': 'Time (s)'},
                    'yaxis': {'title': 'Relative Hypervolume'},
                    'showlegend': True
                    }
        v_fig = {'data': v_plotData,
                 'layout': v_layout
                 }

        self.plot(v_fig, filename = p_fileName)


    def plotHypervolumeAverage(self, p_optimizerSeriesList: list, p_fileName = 'temp-hypervolumeAverage.html', p_showLegend=True, p_showTitle=True):

        v_plotData = []
        for i in range(len(p_optimizerSeriesList)):
            i_seriesList = p_optimizerSeriesList[i]
            i_name = i_seriesList[0].name if i_seriesList[0].name else ''
            i_averageDf = self.__buildAverageSeries(i_seriesList)
            i_averageSeries = py_go.Scatter(
                x=i_averageDf.index,
                y=i_averageDf['average'],
                fill='None',
                mode='lines',
                name=i_name,
                line={'width': 1.5,
                      'color': self.colorsMain[i],
                      'dash': 'solid',
                      'shape': 'hv',
                      # 'color': [0]*len(i_averageDf)
                      }
            )
            i_rangeMaxSeries = py_go.Scatter(
                x=i_averageDf.index,
                y=i_averageDf['rangeMax'].values,
                fill="None",
                fillcolor=self.colorsPastel[i],
                mode='lines',
                name='rangeMax_' + i_name,
                line={'color': "transparent",
                      'shape': 'hv'
                      },
                marker={'color': self.colorsPastel[i]},
                showlegend= False,
                # hoverinfo= 'none'
            )
            i_rangeMinSeries = py_go.Scatter(
                x=i_averageDf.index,
                y=i_averageDf['rangeMin'].values,
                fill="tonexty",
                fillcolor= self.colorsPastel[i],
                mode='lines',
                name='rangeMin_' + i_name,
                line={'color': "transparent",
                      'shape': 'hv'
                      },
                showlegend= False,
                # hoverinfo= 'none'
            )
            v_plotData.append(i_rangeMaxSeries)
            v_plotData.append(i_rangeMinSeries)
            v_plotData.append(i_averageSeries)


        v_layout = {'title': 'Average hypervolume over time' if p_showTitle else '',
                    'xaxis': {'title': 'Time (s)'},
                    'yaxis': {'title': 'Relative Hypervolume'},
                    'showlegend': p_showLegend
                    }
        v_fig = {'data': v_plotData,
                 'layout': v_layout
                 }

        self.plot(v_fig, filename = p_fileName)


    def __buildAverageSeries(self, p_seriesList: list):

        v_timeIndex = []
        [v_timeIndex.extend(i_series.index) for i_series in p_seriesList]
        v_timeIndex.sort()

        v_df = []
        for i_time in v_timeIndex:
            i_valuesArray = np.array([i_series.loc[:i_time].iloc[-1] for i_series in p_seriesList])
            i_row = {}
            i_row['average'] = i_valuesArray.mean()
            i_row['deviation'] = i_valuesArray.std()
            i_row['rangeMax'] = min(i_row['average'] + 2*i_row['deviation'], 1)
            i_row['rangeMin'] = max(i_row['average'] - 2*i_row['deviation'], 0)
            v_df.append(i_row)

        v_df = pd.DataFrame(v_df, index=v_timeIndex)
        return v_df


    def plotBarChartGroups(self, p_groupsDict: dict, p_xLabel=None, p_yLabel=None):

        v_plotData = []
        for i_key, i_values in p_groupsDict.items():
            i_trace = py_go.Bar(
                x = i_values['x'],
                y = i_values['y'],
                name = i_key
            )
            v_plotData.append(i_trace)

        v_layout = {
            'xaxis': {'title': p_xLabel},
            'yaxis': {'title': p_yLabel},
            'barmode': 'group'
        }

        v_fig = {'data': v_plotData,
                 'layout': v_layout,
                 }

        self.plot(v_fig, filename = 'temp-barGroupChart.html')


    def plotHvPerGroupsSeries(self, p_dfList, p_title='Average hypervolume by number of Groups'):

        v_plotData = []
        for i in range(len(p_dfList)):
            i_df = p_dfList[i]
            i_name = i_df.name if i_df.name else ''
            i_trace = py_go.Scatter(
                x=i_df['NumberGroups'],
                y=i_df['HV at 60'],
                fill='None',
                mode='lines',
                name=i_name,
                line={'width': 1.5,
                      'dash': 'solid',
                      'color': self.colorsMain[i],
                      # 'shape': 'hv',
                      # 'color': [0]*len(i_averageDf)
                      }
            )
            v_plotData.append(i_trace)

        v_layout = {'title': p_title,
                    'xaxis': {'title': 'Number of Groups'},
                    'yaxis': {'title': 'Relative Hypervolume'},
                    'showlegend': True
                    }
        v_fig = {'data': v_plotData,
                 'layout': v_layout
                 }

        self.plot(v_fig, filename='temp-plot.html')