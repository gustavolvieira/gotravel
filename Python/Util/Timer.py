from time import time, strftime, localtime
from datetime import timedelta


class Timer:

    def __init__(self, p_contextName = 'Timer', p_verbose = False):
        self.__start = time()
        self.__contextName = p_contextName
        self.__verbose = p_verbose

        self.__line = "="*40
        self.__log("Start " + self.__contextName)


    def __enter__(self):
        return self


    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__log("End " + self.__contextName, self.getTimeElapsedString())


    def __log(self, p_string, p_timeString=None):
        if(self.__verbose):
            print(self.__line)
            print(self.getDatetimeString(), '-', p_string)
            if p_timeString:
                print("Elapsed time:", p_timeString)
            print(self.__line)
            print()


    def getSecondsElapsed(self):
        v_time = time() - self.__start
        return v_time


    def getTimeElapsedString(self):
        return str(timedelta(seconds=self.getSecondsElapsed()))


    def getDatetimeString(self):
        return strftime("%Y-%m-%d %H:%M:%S", localtime())