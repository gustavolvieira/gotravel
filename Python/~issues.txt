ISSUES

- Priorities:
    . >>> = High
    . >> = Medium
    . > = Low

--------------
---- Bugs ----

> getParetoIndexes
    . if two solutions share the same obj values, only one is included on the pareto set



---------------
---- Tasks ----

> Add Restrictions
> filterComponents function

> Test for bigger problems

> Robustness tests: vary solution randomly and check variation

>> Decision making methods to order Pareto solutions
    - use robustness as secondary criteria?

> Crowding distance as second criteria ??

>> Any-time result for optimization process
    - stream intermediary results (only ids) to another process??
    - Visualization

> Interface for tests




----------------------
---- Improvements ----


> improve Objectives struct: https://stackoverflow.com/questions/35988/c-like-structures-in-python

>> improve performance on getFeasibleSteps
    - https://pandas.pydata.org/pandas-docs/stable/enhancingperf.html
    - https://stackoverflow.com/questions/49936557/pandas-dataframe-loc-vs-query-performance
    - options: cython, numba, numexpr+eval
        . eval not useful for small dataframes (<10000 rows)
            -> trip with 4 destinations and 6 components has 1728
        . numba does not support sets, lists, dictionaries, string or objects => not useful