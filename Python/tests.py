import pandas as pd
import numpy as np

from Models.Util import getParetoIndexes
from Models.Trip import Trip
from Models.DataFetcher.RandomFetcher import RandomFetcher
from Models.SolutionComponent import SolutionComponent
from Models.TripSolution import TripSolution
from Models.TripOptimizers.LocalSearch.LocalSearch1OptFirst import LocalSearch1OptFirst
from Models.TripOptimizers.ACO.OptimizerACO import OptimizerACO
from Models.TripOptimizers.ACO.OptimizerMOEAD_ACO import OptimizerMOEAD_ACO


from Scripts.problemData import loadProblemInstance
from Util.Plots import CustomPlots
from Util.Timer import Timer

from Scripts.optimizationExperiments import runOptimizationExperiment


def testGetParetoIndexes():

    v_points = {'obj1': list(np.arange(0.1, 0.9, 0.1)) + list(np.random.rand(10) + 0.5) + [0.1],
                'obj2': list(np.flip(np.arange(0.1, 0.9, 0.1), 0)) + list(np.random.rand(10) + 0.5) + [1]
                }
    v_solutionsDf = pd.DataFrame(v_points,
                                 index=['i-' + str(i) for i in range(len(v_points['obj1']))]
                                 )
    v_paretoIndexes = getParetoIndexes(v_solutionsDf.values)
    v_paretoSolutionsDf = v_solutionsDf.iloc[v_paretoIndexes]

    v_plotter = CustomPlots()
    v_plotter.plotSolutions(v_paretoSolutionsDf, v_solutionsDf)

    pass



def testGetTrip():
    v_trip = Trip.generateTrip(p_numberDestinations=4, p_seed=0)
    pass


def testRandomFetcher():
    v_fetcher = RandomFetcher(p_seed=0, p_numberAccommodations=1, p_numberTransports=1)
    v_trip = Trip.generateTrip(p_numberDestinations=3, p_seed=0, p_daysStayMaxDiff=1)
    v_objectives = ['cost', 'time']

    v_accommodationOptions, v_transportOptions = v_fetcher.fetchComponents(v_trip)

    v_componentsDf, v_normalizatonReference = SolutionComponent.getComponentsDf(v_accommodationOptions, v_transportOptions, v_objectives)
    v_transportDf = v_componentsDf[v_componentsDf['type'] == 'transport']
    v_accommodationDf = v_componentsDf[v_componentsDf['type'] == 'accommodation']

    pass


def testOptimization(p_plot=True, p_maxOptimizationTime=60, p_distributed=False, p_improvedGetComponents = True, p_numbaGetComponents = True, p_verbose=False):

    p_problemInstance = Trip.getSavedTrip(p_destinations=5,
                                          p_components=6,
                                          p_fetcherSeed=0,
                                          p_daysStay=1,
                                          p_approximated=True,
                                          p_ordered=False
                                          )

    v_localSearchModuleSteps = LocalSearch1OptFirst()
    v_localSearchModuleFinal = None
    v_optimizer = OptimizerACO(p_objectives=p_problemInstance.objectives, p_maxStagnantGenerations=1000, p_localSearchModuleSteps=v_localSearchModuleSteps, p_localSearchModuleFinal=v_localSearchModuleFinal, p_distributed=p_distributed, p_improvedGetComponents=p_improvedGetComponents, p_verbose=p_verbose)
    # v_optimizer.setPresetParams('random-pure')
    # v_optimizer.setPresetParams('deterministic-greedy')
    # v_optimizer.setPresetParams('random-greedy')
    # v_optimizer = OptimizerMOEAD_ACO(p_objectives=v_objectives, p_maxStagnantGenerations=1000, p_localSearch=p_localSearch, p_distributed=p_distributed, p_improvedGetComponents=p_improvedGetComponents, p_verbose=p_verbose, p_numberGroups=4, p_neighbourhoodSize=8, p_limitPheromones=True, p_minPheromoneLimitPercentage=1/250, p_currentSolutionInfluence = 0.1, p_chooseBestComponentProb = 0.9)
    # v_optimizer = OptimizerACO(p_objectives=v_objectives, p_maxStagnantGenerations=1000, p_localSearch=p_localSearch, p_distributed=p_distributed, p_improvedGetComponents=p_improvedGetComponents, p_verbose=p_verbose, p_chooseBestComponentProb = 0.9)


    with Timer('optimization', p_verbose=True):
        v_optimizationResults = v_optimizer.optimize(p_trip=p_problemInstance.trip, p_maxTimeSeconds=p_maxOptimizationTime , p_componentsDf=p_problemInstance.componentsDf, p_nadirPoint=p_problemInstance.nadirHeuristic)
    v_solutionsDf = TripSolution.builtSolutionsDf(v_optimizationResults.solutionsList, v_optimizationResults.componentsDf)

    v_paretoHV = TripSolution.calcHypervolume(p_solutionsDf=p_problemInstance.paretoDf, p_nadirPoint=p_problemInstance.nadirHeuristic)

    print('Total Solutions: ' + str(p_problemInstance.totalSolutions))
    print('Total Components: ' + str(len(p_problemInstance.componentsDf)))
    print('Iterations: ' + str(v_optimizationResults.iterations))
    print('NadirPoint: ' + str(v_optimizationResults.nadirPoint))
    print('StopCondition: ' + str(v_optimizationResults.stopCondition))
    print('generationsWithoutImprovement: ' + str(v_optimizationResults.generationsWithoutImprovement))

    print('ParetoHypervolume: ' + str(v_paretoHV))
    print('Hypervolume: ' + str(v_optimizationResults.hypervolumeSeries.iloc[-1]))

    v_relativeHypervolume = v_optimizationResults.hypervolumeSeries/v_paretoHV
    print('RelativeHypervolume: ' + str(v_relativeHypervolume.iloc[-1]))

    if(p_plot):
        v_plotter = CustomPlots()
        v_plotter.plotSolutions(v_solutionsDf, p_allSolutionsDf=None, p_nadirPoint=v_optimizationResults.nadirPoint, p_paretoRealDf=p_problemInstance.paretoDf)
        v_plotter.plotHypervolumeSeries([v_relativeHypervolume])

    return v_optimizationResults


def testGetAllSolutions(p_maxOptimizationTime=10):
    v_fetcher = RandomFetcher(p_seed=0, p_numberAccommodations=3, p_numberTransports=3)
    v_trip = Trip.generateTrip(p_numberDestinations=2, p_seed=0, p_daysStayMaxDiff=1)
    v_objectives = ['cost', 'time']

    v_optimizer = OptimizerACO(p_objectives=v_objectives)
    v_optimizer.setPresetParams('random-pure')

    with Timer('optimization'):
        v_optimizationResults = v_optimizer.optimize(p_trip=v_trip, p_maxTimeSeconds=p_maxOptimizationTime , p_componentsDf=None, p_nadirPoint=None)
    v_solutionsParetoDf = TripSolution.builtSolutionsDf(v_optimizationResults.solutionsList, v_optimizationResults.componentsDf)

    with Timer('getAllSolutions'):
        v_allSolutions = TripSolution.getAllSolutions(v_trip, v_optimizationResults.componentsDf)
    v_allSolutionsDf = TripSolution.builtSolutionsDf(v_allSolutions, v_optimizationResults.componentsDf)

    assert(len(set(v_allSolutionsDf.index.values)) == len(v_allSolutions))
    print('Total Solutions: ' + str(len(v_allSolutions)))

    v_plotter = CustomPlots()
    v_plotter.plotSolutions(v_solutionsParetoDf, v_allSolutionsDf)

    pass


def testPlotHypervolumeAverage():

    from Scripts.optimizationExperiments import loadExperimentData

    v_hvSeries = []
    v_experimentsFiles = ['2018-06-26_21-23-16_optimization_trip-d2-s0-a3-t3.json',
                          '2018-06-26_19-02-59_optimization_trip-d2-s0-a3-t3.json'
                          ]

    for i_experiment in v_experimentsFiles:
        i_data = loadExperimentData(i_experiment)
        v_trip, v_componentsDf, v_objectives, v_nadirPointHeuristic, v_paretoDf, v_totalSolutions, v_fileName = Trip.getSavedTrip(i_data['tripNumber'])
        v_nadir = v_nadirPointHeuristic
        i_paretoHV = TripSolution.calcHypervolume(p_solutionsDf=v_paretoDf, p_nadirPoint=v_nadir)

        v_hvSeries.append([i_run['hypervolume']/ i_paretoHV for i_run in i_data['runs']])

    v_plotter = CustomPlots()
    v_plotter.plotHypervolumeSeries(p_seriesList=v_hvSeries[0])
    v_plotter.plotHypervolumeAverage(p_optimizerSeriesList=v_hvSeries)
    pass


def testLocalSearch(p_tripNumber=6.6101, p_plot=True, p_maxOptimizationTime = 40, p_localSearch=False, p_distributed=False):

    from Models.TripOptimizers.LocalSearchUtil import searchLocal

    v_trip, v_componentsDf, v_objectives, v_nadir, v_paretoDf, v_totalSolutions, v_fileName = Trip.getSavedTrip(p_tripNumber)

    v_optimizer = OptimizerACO(p_objectives=v_objectives, p_maxStagnantGenerations=1000, p_localSearch=p_localSearch, p_distributed=p_distributed)
    # v_optimizer.setPresetParams('random-pure')
    # v_optimizer.setPresetParams('deterministic-greedy')
    # v_optimizer.setPresetParams('random-greedy')

    with Timer('optimization', p_verbose=True):
        v_optimizationResults = v_optimizer.optimize(p_trip=v_trip, p_maxTimeSeconds=p_maxOptimizationTime , p_componentsDf=v_componentsDf, p_nadirPoint=v_nadir)
    v_solutionsDf = TripSolution.builtSolutionsDf(v_optimizationResults.solutionsList, v_componentsDf)

    v_paretoHV = TripSolution.calcHypervolume(p_solutionsDf=v_paretoDf, p_nadirPoint=v_nadir)

    v_solutionListLocal = [searchLocal(i_solution) for i_solution in v_optimizationResults.solutionsList]
    v_solutionLocalDf = TripSolution.builtSolutionsDf(v_solutionListLocal, v_componentsDf)

    print('Total Solutions: ' + str(v_totalSolutions))
    print('Iterations: ' + str(v_optimizationResults.iterations))
    print('NadirPoint: ' + str(v_optimizationResults.nadirPoint))
    print('StopCondition: ' + str(v_optimizationResults.stopCondition))
    print('generationsWithoutImprovement: ' + str(v_optimizationResults.generationsWithoutImprovement))

    print('ParetoHypervolume: ' + str(v_paretoHV))
    print('Hypervolume: ' + str(v_optimizationResults.hypervolumeSeries.iloc[-1]))

    v_relativeHypervolume = v_optimizationResults.hypervolumeSeries/v_paretoHV
    print('RelativeHypervolume: ' + str(v_relativeHypervolume.iloc[-1]))

    if(p_plot):
        v_plotter = CustomPlots()
        v_plotter.plotSolutions(v_solutionsDf, p_allSolutionsDf=None, p_nadirPoint=v_optimizationResults.nadirPoint, p_paretoRealDf=v_paretoDf)
        # v_plotter.plotHypervolumeSeries([v_relativeHypervolume])

        v_plotter.plotSolutions(v_solutionLocalDf, p_allSolutionsDf=None, p_nadirPoint=v_optimizationResults.nadirPoint, p_paretoRealDf=v_paretoDf, p_fileName='temp-solutionsLocal.html')

    assert (np.array([i_solution.isFeasible() for i_solution in v_solutionListLocal]).prod() == 1)

    pass


def testBottleneck():

    v_infoList = []
    v_trips = [2.30, 2.60, 3.30, 3.60, 4.60]
    # v_trips = [2.2,3.2]
    for i_trip in v_trips:

        for i_improvedGetComponents in [True, False]:
            # TripSolution.improvedGetNextStep = i_improved
            # TripSolution.g_getFeasibleStepsTime = 0
            # TripSolution.g_getFeasibleStepsCount = {}
            # TripSolution.g_getFeasibleStepsDict = {}

            with Timer() as v_timer:
                v_optimizationResults = testOptimization(p_tripNumber=i_trip, p_plot=False, p_maxOptimizationTime=20, p_localSearch=False, p_distributed=False, p_improvedGetComponents=i_improvedGetComponents)
                v_totalTime = v_timer.getSecondsElapsed()

            # i_timeProportion = TripSolution.g_getFeasibleStepsTime/v_totalTime
            # i_seriesCount = pd.Series(TripSolution.g_getFeasibleStepsCount)
            # i_repeatedCallsProportion = (i_seriesCount.sum()-len(i_seriesCount))/i_seriesCount.sum()

            v_infoList.append({'trip': i_trip,
                               # 'timeProportion': i_timeProportion,
                               # 'repeatedCallsProportion': i_repeatedCallsProportion,
                               'improvedGetComponents': i_improvedGetComponents,
                               'iterations': v_optimizationResults.iterations
                               })

    v_infoDf = pd.DataFrame(v_infoList)
    print(v_infoDf)

    pass


def viewSavedTrip():

    v_instanceReturn = loadProblemInstance('trip-d4-ds1-orF-s0-a6-t6-sf0.json')
    # v_trip, v_componentsDf, v_objectives, v_nadirPointHeuristic, v_nadirPointExactAprox, v_paretoDfAprox, v_totalSolutions = loadProblemInstance('trip-d4-ds1-orF-s0-a6-t6-sf0-approx.json')

    v_plotter = CustomPlots()
    v_plotter.plotSolutions(p_paretoEstimatedDf=v_instanceReturn.paretoDf, p_allSolutionsDf=v_instanceReturn.totalSolutions, p_nadirPoint=v_instanceReturn.nadirHeuristic, p_paretoRealDf=None)

    pass



def runOptimizationExperimentsVarying():
    p_verbose = False
    p_improvedGetComponents = True
    p_numberRuns = 40
    p_maxStagnantGenerations = 1000

    # [(2, False,70), (3, False,70), (4,False,70), (5, True,70), (6, True,90), (7,True,90)]
    for i_destinations, i_approx, i_time in [(6, True,90), (7, True,90)]:
        v_problemInstance = Trip.getSavedTrip(p_destinations=i_destinations,
                                              p_components=6,
                                              p_fetcherSeed=0,
                                              p_daysStay=1,
                                              p_approximated=i_approx,
                                              p_ordered=False
                                              )

        v_localSearchModuleSteps = LocalSearch1OptFirst()
        v_localSearchModuleFinal = None

        for i_localSearchSteps in [None]:

            for i_distributed in [False]:

                for i_alpha, i_beta in [(1,2)]:
                    v_pheromoneModifierDict = {'cost': i_alpha, 'time': i_alpha}
                    v_heuristicModifierDict = {'cost': i_beta, 'time': i_beta}

                    for i_groups, i_neighbourhoodSize in [(5,4), (1,4), (10,4), (15,4), (20,4), (25,4), (30,4)]:

                        v_optimizerACO = OptimizerACO(p_objectives=v_problemInstance.objectives, p_maxStagnantGenerations=1000, p_localSearchModuleSteps=i_localSearchSteps, p_localSearchModuleFinal=v_localSearchModuleFinal, p_distributed=i_distributed, p_improvedGetComponents=p_improvedGetComponents, p_verbose=p_verbose, p_pheromoneModifierDict=v_pheromoneModifierDict, p_heuristicModifierDict=v_heuristicModifierDict)

                        v_optimizerMOEA = OptimizerMOEAD_ACO(p_objectives=v_problemInstance.objectives, p_maxStagnantGenerations=1000, p_localSearchModuleSteps=i_localSearchSteps, p_localSearchModuleFinal=v_localSearchModuleFinal, p_distributed=i_distributed, p_improvedGetComponents=p_improvedGetComponents, p_verbose=p_verbose, p_numberGroups=i_groups, p_neighbourhoodSize=i_neighbourhoodSize, p_limitPheromones=True, p_minPheromoneLimitPercentage=1/250, p_currentSolutionInfluence = 0.1, p_chooseBestComponentProb = 0.9, p_pheromoneModifierDict=v_pheromoneModifierDict, p_heuristicModifierDict=v_heuristicModifierDict)

                        for i_optimizer in [v_optimizerMOEA]:

                            runOptimizationExperiment(p_numberRuns= p_numberRuns, p_maxOptimizationSeconds = i_time, p_maxStagnantGenerations = p_maxStagnantGenerations, p_distributed = i_distributed, p_improvedGetComponents = p_improvedGetComponents, p_verbose = p_verbose, p_problemInstance = v_problemInstance, p_optimizer = i_optimizer)


