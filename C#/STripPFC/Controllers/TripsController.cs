﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using STripPFC.Models;
using STripPFC.Models.TripOptimizers;
using STripPFC.Models.TripOptimizers.ACO;
using Deedle;
using STripPFC.Models.Util;
using STripPFC.Models.Util;

namespace STripPFC.Controllers
{
    public class TripsController : Controller
    {
        private STripDbContext db = new STripDbContext();

        // GET: Trips
        public ActionResult Index()
        {
            return View(db.Trips.ToList());
        }

        // GET: Trips/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View(trip);
        }

        // GET: Trips/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Trips/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RoundTrip")] Trip trip)
        {
            if (ModelState.IsValid)
            {
                db.Trips.Add(trip);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(trip);
        }

        // GET: Trips/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View(trip);
        }

        // POST: Trips/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RoundTrip")] Trip trip)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trip).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(trip);
        }

        // GET: Trips/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            return View(trip);
        }

        // POST: Trips/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Trip trip = db.Trips.Find(id);
            db.Trips.Remove(trip);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: Trips/Optimize/1
        public ActionResult Optimize(int id, bool p_fetch = false)
        {
            
            Trip v_trip = Trip.GetCompleteTrip(id, this.db);
            if (v_trip == null)
            {
                return HttpNotFound();
            }

            if (p_fetch)
            {
                v_trip.FetchOptions(this.db);
                this.db.SaveChanges();
            }

            return View(v_trip);
        }

        public ActionResult OptimizeAgain(int id, float? p_pheromoneMod = null, float? p_heuristicMod = null, int? p_numAnts = null, int? p_maxGenerations = null, int? p_paretoSize = null, float? p_evaporationRate = null, float? p_pheromoneDepositRate = null, float? p_maxExecutionTime = null, bool p_paretoWindow = false, int p_repetitions = 1, int? p_localSearch = null)
        {
            Trip trip = Trip.GetCompleteTrip(id, this.db);
            if (trip == null)
            {
                return HttpNotFound();
            }

            NamoaOptimizer v_optimizerACO = new NamoaOptimizer();
//            AntColonyOptimizer v_optimizerACO = new AntColonyOptimizer();

//            if (p_numAnts != null)
//                v_optimizerACO.Parameters.NumberAnts = p_numAnts.Value;
//            if (p_maxGenerations != null)
//                v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement = p_maxGenerations.Value;
//            if (p_pheromoneMod != null && p_heuristicMod != null)
//                v_optimizerACO.Parameters.SetProbabilityParameters(p_pheromoneMod.Value, p_heuristicMod.Value);
//            if (p_paretoSize != null)
//                v_optimizerACO.Parameters.ParetoSetSize = p_paretoSize.Value;
//            if (p_evaporationRate != null)
//                v_optimizerACO.Parameters.EvaporationRate = p_evaporationRate.Value;
//            if (p_pheromoneDepositRate != null)
//                v_optimizerACO.Parameters.PheromoneDepositRate = p_pheromoneDepositRate.Value;
//            if (p_maxExecutionTime != null)
//                v_optimizerACO.Parameters.MaxExecutionTime = TimeSpan.FromMinutes(p_maxExecutionTime.Value);
//            if (p_localSearch != null)
//                v_optimizerACO.Parameters.LocalSearch = p_localSearch > 0;

            trip.FetchOptions(this.db);

            List<TripSolution> v_solutions = new List<TripSolution>();
            for (int i = 0; i < p_repetitions; i++)
            {
                trip.DetermineSolutions(v_optimizerACO);
                v_solutions.AddRange(v_optimizerACO.Solutions);
                this.db.TripSolutions.RemoveRange(this.db.TripSolutions.Where(s => s.DominanceRank > 1));
                this.db.SaveChanges();
            }

            System.Media.SystemSounds.Exclamation.Play();

            if (p_paretoWindow)
            {
//                return View("ParetoEvolutionPopup", v_optimizerACO);
                return View("ParetoEvolutionPopup", new AntColonyOptimizer());;
            }
            else
            {
                var v_jsonReturn = new
                {
                    solutions = v_solutions.Select(s => new
                    {
                        Cost = s.ObjectiveValues["Cost"],
                        Time = s.ObjectiveValues["Time"],
                        DominanceRank = s.DominanceRank,
                        SolutionComponentsIds = s.SolutionComponentsIds,
                        SolutionSummary = String.Join("->", s.SolutionComponents.Where(c => c.TimeValue != 0).Select(c => c.FinalDestination.Name + c.FinalDate.ToString("(dd-MM)")).ToList()),
                        CrowdingDistance = s.CrowdingDistance
                    }),
                    time = v_optimizerACO.OptimizationTime,
                    numberGenerations = v_optimizerACO.numberGenerations,
                    hypervolume = v_optimizerACO.ParetoHypervolume
                };
                var v_return = Json(v_jsonReturn, JsonRequestBehavior.AllowGet);
                v_return.MaxJsonLength = int.MaxValue;
                return v_return;
            }
            
        }

        public ActionResult ClearNotOptimalSolutions(int id)
        {
            this.db.TripSolutions.RemoveRange(this.db.TripSolutions.Where(s => s.DominanceRank > 1));
            this.db.SaveChanges();

            return Content("Success!");
        }

        public ActionResult GenerateOptimalParetoSets(int id)
        {
            this.db.Database.CommandTimeout = 300;

            Stopwatch v_stopwatch = new Stopwatch();
            string v_filePath = @"C:\Users\Gustavo\SkyDrive\PFC\LogClearNotOptimalSolutions" + DateTime.Now.ToString("_yyyy.MM.dd_HH.mm") + ".txt";


            int[] v_tripIds;
            if (id == 0)
                v_tripIds = new int[7] {1, 2, 3, 4, 5, 6, 7};
            else
                v_tripIds = new int[] {id};

            foreach (int i_id in v_tripIds)
            {
                using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
                {
                    sw.WriteLine("Trip " + i_id + ": Start" + DateTime.Now);
                }


                Trip trip = Trip.GetCompleteTrip(i_id, this.db);
                if (trip == null)
                {
                    return HttpNotFound();
                }
                trip.FetchOptions(this.db);
                AntColonyOptimizer v_optimizerACO = new AntColonyOptimizer();

                v_optimizerACO.Parameters.NumberAnts = 50;
                v_optimizerACO.Parameters.SetProbabilityParameters(0.3f, 1);
                v_optimizerACO.Parameters.ParetoSetSize = 50;
                v_optimizerACO.Parameters.EvaporationRate = 0.1F;
                v_optimizerACO.Parameters.PheromoneDepositRate = 0.3F;


//                v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement = 200;
//                v_optimizerACO.Parameters.MaxExecutionTime = TimeSpan.FromMinutes(5);
//                for (int i = 0; i < 8; i++)
//                {
//                    v_stopwatch.Restart();
//                    trip.DetermineSolutions(v_optimizerACO, p_onlyPareto: true);
//                    this.SaveDb(v_filePath);
//
//                    v_stopwatch.Stop();
//
//                    using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
//                    {
//                        sw.WriteLine("Trip " + i_id + " - Iteration " + i + " w/ timeMax=" + v_optimizerACO.Parameters.MaxExecutionTime + " and maxGen=" + v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement + " (Time =" + v_stopwatch.Elapsed. + ")");
//                    }
//                }

//                v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement = 500;
//                v_optimizerACO.Parameters.MaxExecutionTime = TimeSpan.FromMinutes(10);
//                for (int i = 0; i < 4; i++)
//                {
//                    v_stopwatch.Restart();
//                    trip.DetermineSolutions(v_optimizerACO, p_onlyPareto: true);
//                    this.SaveDb(v_filePath);
//
//                    v_stopwatch.Stop();
//
//                    using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
//                    {
//                        sw.WriteLine("Trip " + i_id + " - Iteration " + i + " w/ timeMax=" + v_optimizerACO.Parameters.MaxExecutionTime + " and maxGen=" + v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement + " (Time =" + v_stopwatch.Elapsed. + ")");
//                    }
//                }
//
//                v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement = 1000;
//                v_optimizerACO.Parameters.MaxExecutionTime = TimeSpan.FromMinutes(20);
//                for (int i = 0; i < 3; i++)
//                {
//                    v_stopwatch.Restart();
//                    trip.DetermineSolutions(v_optimizerACO, p_onlyPareto: true);
//                    this.SaveDb(v_filePath);
//
//                    v_stopwatch.Stop();
//
//                    using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
//                    {
//                        sw.WriteLine("Trip " + i_id + " - Iteration " + i + " w/ timeMax=" + v_optimizerACO.Parameters.MaxExecutionTime + " and maxGen=" + v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement + " (Time =" + v_stopwatch.Elapsed + ")");
//                    }
//                }

                v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement = 2000;
                v_optimizerACO.Parameters.MaxExecutionTime = TimeSpan.FromMinutes(30);
                for (int i = 0; i < 3; i++)
                {
                    v_stopwatch.Restart();
                    trip.DetermineSolutions(v_optimizerACO, p_onlyPareto: true);
                    this.SaveDb(v_filePath);

                    v_stopwatch.Stop();

                    using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
                    {
                        sw.WriteLine("Trip " + i_id + " - Iteration " + i + " w/ timeMax=" + v_optimizerACO.Parameters.MaxExecutionTime + " and maxGen=" + v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement + " (Time =" + v_stopwatch.Elapsed + ")");
                    }
                }

                System.Media.SystemSounds.Exclamation.Play();
                this.db.TripSolutions.RemoveRange(this.db.TripSolutions.Where(s => s.DominanceRank > 1));
                this.SaveDb(v_filePath);
                System.Media.SystemSounds.Exclamation.Play();

            }

            return Content("Success!");
        }

        private void SaveDb(string p_filePath)
        {

            try
            {
                this.db.SaveChanges();
            }
            catch (OptimisticConcurrencyException)
            {
                using (StreamWriter sw = System.IO.File.AppendText(p_filePath))
                {
                    sw.WriteLine("OptimisticConcurrencyException");
                }
                System.Media.SystemSounds.Exclamation.Play();
            }

        }

        public ActionResult HypervolumePlot(int id, float p_timeMax = 1, int p_numberExperiments = 8)
        {
            this.db.Database.CommandTimeout = 300;
            Stopwatch v_stopwatch = new Stopwatch();
//            string v_filePath = @"C:\Users\Gustavo\SkyDrive\PFC\HypervolumeTest" + DateTime.Now.ToString("_yyyy.MM.dd_HH.mm") + ".txt";

            int[] v_tripIds = new int[5] { 1, 2, 3, 4, 5 };
//            int[] v_tripIds = new int[] {3, 5, 9 };

//            float[] v_times= new float[] { 0.25f, 0.5f, 1, 3};
//            int i_id = 4;

            List<List<double>> v_tripHypervolumes1 = new List<List<double>>();
            List<List<double>> v_tripHypervolumes2 = new List<List<double>>();
//            foreach (float i_time in v_times)
            foreach (int i_id in v_tripIds)
            {
//                using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
//                {
//                    sw.WriteLine("Trip " + i_id + ": Start" + DateTime.Now);
//                }


                Trip v_trip = Trip.GetCompleteTrip(i_id, this.db);
                if (v_trip == null)
                {
                    return HttpNotFound();
                }
                v_trip.FetchOptions(this.db);

                AntColonyOptimizer v_optimizerACO1 = new AntColonyOptimizer();
                v_optimizerACO1.Parameters.NumberAnts = 50;
                v_optimizerACO1.Parameters.SetProbabilityParameters(0.3f, 1.5f);
                v_optimizerACO1.Parameters.ParetoSetSize = 50;
                v_optimizerACO1.Parameters.EvaporationRate = 0.1F;
                v_optimizerACO1.Parameters.PheromoneDepositRate = 0.3F;
                v_optimizerACO1.Parameters.MaxGenerationsWithoutImprovement = 2000;
                v_optimizerACO1.Parameters.MaxExecutionTime = TimeSpan.FromMinutes(p_timeMax);
                v_optimizerACO1.Parameters.LocalSearch = false;

                AntColonyOptimizer v_optimizerACO2 = new AntColonyOptimizer();
                v_optimizerACO2.Parameters.NumberAnts = 50;
                v_optimizerACO2.Parameters.SetProbabilityParameters(0.3f, 1.5f);
                v_optimizerACO2.Parameters.ParetoSetSize = 50;
                v_optimizerACO2.Parameters.EvaporationRate = 0.1F;
                v_optimizerACO2.Parameters.PheromoneDepositRate = 0.3F;
                v_optimizerACO2.Parameters.MaxGenerationsWithoutImprovement = 2000;
                v_optimizerACO2.Parameters.MaxExecutionTime = TimeSpan.FromMinutes(p_timeMax);
                v_optimizerACO2.Parameters.LocalSearch = true;


                List<double> v_hypervolumesList1 = new List<double>();
                List<double> v_hypervolumesList2 = new List<double>();

                for (int i = 0; i < p_numberExperiments; i++)
                {
                    v_stopwatch.Restart();
                    v_trip.DetermineSolutions(v_optimizerACO1);
                    v_trip.DetermineSolutions(v_optimizerACO2);
                    v_stopwatch.Stop();

                    //                    using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
                    //                    {
                    //                        sw.WriteLine("Trip " + i_id + " - Iteration " + i + " - timeMax=" +
                    //                                     v_optimizerACO.Parameters.MaxExecutionTime + "(exec=" + v_stopwatch.Elapsed + "), maxGen=" +
                    //                                     v_optimizerACO.Parameters.MaxGenerationsWithoutImprovement + ": HV = " +
                    //                                     v_optimizerACO.ParetoHypervolume/v_trip.ParetoHypervolume);
                    //                    }

                    v_hypervolumesList1.Add(v_optimizerACO1.HypervolumeList.Last() / v_trip.ParetoHypervolume);
                    v_hypervolumesList2.Add(v_optimizerACO2.HypervolumeList.Last() / v_trip.ParetoHypervolume);

//                    v_hypervolumesList1.Add(v_optimizerACO1.ParetoHypervolume/v_trip.ParetoHypervolume);
//                    v_hypervolumesList2.Add(v_optimizerACO2.ParetoHypervolume/v_trip.ParetoHypervolume);
                }
                v_tripHypervolumes1.Add(v_hypervolumesList1);
                v_tripHypervolumes2.Add(v_hypervolumesList2);
            }

            System.Media.SystemSounds.Exclamation.Play();

            ViewBag.HVList1 = v_tripHypervolumes1;
            ViewBag.HVList2 = v_tripHypervolumes2;
            ViewBag.Time = p_timeMax;

            return View();
        }


        public ActionResult SearchTimes(int id, int p_numberExperiments = 3)
        {
//            this.db.Database.CommandTimeout = 300;
//            Stopwatch v_stopwatch = new Stopwatch();
//            string v_filePath = @"C:\Users\Gustavo\SkyDrive\PFC\SearchTimeTest" + DateTime.Now.ToString("_yyyy.MM.dd_HH.mm") + ".txt";
//
//            int[] v_tripIds = new int[7] { 1, 2, 3, 4, 5, 6, 7 };
//
//            List<List<double>> v_searchTimes = new List<List<double>>();
//            foreach (int i_id in v_tripIds)
//            {
//                using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
//                {
//                    sw.WriteLine("Trip " + i_id + ": Start" + DateTime.Now);
//                }
//
//                List<double> v_timeList = new List<double>();
//
//                for (int i = 0; i < p_numberExperiments; i++)
//                {
//                    Trip v_trip = Trip.GetCompleteTrip(i_id, this.db);
//                    if (v_trip == null)
//                    {
//                        return HttpNotFound();
//                    }
//
//                    v_stopwatch.Restart();
//                    v_trip.FetchOptions(this.db);
//                    v_stopwatch.Stop();
//
//                    using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
//                    {
//                        sw.WriteLine("Trip " + i_id + " - Iteration " + i + " - searchTime=" +
//                                     v_stopwatch.Elapsed.TotalSeconds + " s");
//                    }
//                    v_timeList.Add(v_stopwatch.Elapsed.TotalSeconds);
//                }
//                v_searchTimes.Add(v_timeList);
//            }
//
//            System.Media.SystemSounds.Exclamation.Play();
//       
//            ViewBag.TimeList = v_searchTimes;

            return View();
        }


        public ActionResult QualityTimePlot(int id, float p_timeMax = 1, int p_numberExperiments = 3, int p_maxGenWithoutImprovement = 1000)
        {
            this.db.Database.CommandTimeout = 300;
            Stopwatch v_stopwatch = new Stopwatch();
//            string v_filePath = @"C:\Users\Gustavo\SkyDrive\PFC\Logs\QualityTimeTest" + DateTime.Now.ToString("_yyyy.MM.dd_HH.mm") + ".txt";
            
            List<List<double>> v_hypervolumes1 = new List<List<double>>();
            List<List<double>> v_times1 = new List<List<double>>();
            ParametersACO v_parameters1 = new ParametersACO();
            v_parameters1.NumberAnts = 50;
            v_parameters1.SetProbabilityParameters(0.3f, 1.5f);
            v_parameters1.ParetoSetSize = 50;
            v_parameters1.EvaporationRate = 0.1F;
            v_parameters1.PheromoneDepositRate = 0.3F;
            v_parameters1.MaxGenerationsWithoutImprovement = p_maxGenWithoutImprovement;
            v_parameters1.MaxExecutionTime = TimeSpan.FromMinutes(p_timeMax);
            v_parameters1.LocalSearch = false;


            List<List<double>> v_hypervolumes2 = new List<List<double>>();
            List<List<double>> v_times2 = new List<List<double>>();
            ParametersACO v_parameters2 = new ParametersACO();
            v_parameters2.NumberAnts = 50;
            v_parameters2.SetProbabilityParameters(0.3f, 1);
            v_parameters2.ParetoSetSize = 50;
            v_parameters2.EvaporationRate = 0.1F;
            v_parameters2.PheromoneDepositRate = 0.3F;
            v_parameters2.MaxGenerationsWithoutImprovement = p_maxGenWithoutImprovement;
            v_parameters2.MaxExecutionTime = TimeSpan.FromMinutes(p_timeMax);
            v_parameters2.LocalSearch = true;

            Trip v_trip = Trip.GetCompleteTrip(id, this.db);
            if (v_trip == null)
            {
                return HttpNotFound();
            }
            double v_referenceHV = v_trip.ParetoHypervolume;


//            using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
//            {
//                sw.WriteLine("Trip " + id + ": Start" + DateTime.Now);
//            }

            v_trip.FetchOptions(this.db);


            for (int i = 0; i < p_numberExperiments; i++)
            {
                AntColonyOptimizer v_optimizer1 = new AntColonyOptimizer();
                v_optimizer1.Parameters = v_parameters1;

                AntColonyOptimizer v_optimizer2 = new AntColonyOptimizer();
                v_optimizer2.Parameters = v_parameters2;

                v_stopwatch.Restart();
                v_trip.DetermineSolutions(v_optimizer1);
                v_trip.DetermineSolutions(v_optimizer2);
                v_stopwatch.Stop();

                v_hypervolumes1.Add(v_optimizer1.HypervolumeList.Select(v => v/v_referenceHV).ToList());
                v_times1.Add(v_optimizer1.IterationTimesList);
                v_hypervolumes2.Add(v_optimizer2.HypervolumeList.Select(v => v / v_referenceHV).ToList());
                v_times2.Add(v_optimizer2.IterationTimesList);

//                using (StreamWriter sw = System.IO.File.AppendText(v_filePath))
//                {
//                    sw.WriteLine("Trip " + id + " - Iteration " + i + " - timeMax=" + 
//                                    v_optimizer1.Parameters.MaxExecutionTime + "(exec=" + v_stopwatch.Elapsed + "), 
//                }
            }

            System.Media.SystemSounds.Exclamation.Play();


            ViewBag.Series = new List<Frame<int, string>> {GetAverageTimeSeries(v_hypervolumes1, v_times1, p_timeMax),
                GetAverageTimeSeries(v_hypervolumes2, v_times2, p_timeMax) };
            ViewBag.BoxPlots = new List<List<double>> { v_hypervolumes1.Select(i_list => i_list.Last()).ToList(), v_hypervolumes2.Select(i_list => i_list.Last()).ToList() } ;
            ViewBag.TimeMax = p_timeMax;
            ViewBag.TripSize = v_trip.DestinationSpecifications.Count;

            return View();
        }

        private static Frame<int, string> GetAverageTimeSeries(List<List<double>> p_hVolumes, List<List<double>> p_times, float p_maxTimeMin)
        {
            // Organize info as lists of series
            List<Series<double, double>> v_seriesList = new List<Series<double, double>>();
            for (int i = 0; i < p_hVolumes.Count; i++)
            {
                p_hVolumes[i].Insert(0,0);
                p_times[i].Insert(0,0);
                Series<double, double> v_series = Enumerable.Range(0, p_hVolumes[i].Count)
                        .Select(j => KeyValue.Create(p_times[i][j], p_hVolumes[i][j] )).ToSeries();
                v_seriesList.Add(v_series);
            }

            double v_timeStep = p_maxTimeMin*60 / (p_times.Max(i_list => i_list.Count)*2);
            int v_numberDivisions = p_times.Max(i_list => i_list.Count)*2;

            List<float> v_timeSteps =
                Enumerable.Range(0, v_numberDivisions)
                    .Select(v => (v*p_maxTimeMin*60)/v_numberDivisions)
                    .ToList();

            var v_rows = (from i_time in v_timeSteps
                let v_volumes = v_seriesList.Select(i_series => i_series.Get(i_time, Lookup.ExactOrSmaller)).ToList()
                let v_average = v_volumes.Sum()/v_volumes.Count
                let v_deviation = Math.Sqrt(v_volumes.Sum(i_vol => Math.Pow(i_vol - v_average, 2))/v_volumes.Count)
                select new {Time = i_time, Average = v_average, Deviation = v_deviation}
                );

            var v_dataFrame = Frame.FromRecords(v_rows);
            return v_dataFrame;
        }


        public ActionResult GenerateDataset(int id, int p_numExperiments = 8)
        {

            ParametersACO v_parameters1 = new ParametersACO();
            v_parameters1.NumberAnts = 50;
            v_parameters1.SetProbabilityParameters(0.3f, 1.5f);
            v_parameters1.ParetoSetSize = 50;
            v_parameters1.EvaporationRate = 0.1F;
            v_parameters1.PheromoneDepositRate = 0.3F;
            v_parameters1.MaxGenerationsWithoutImprovement = 100;
            v_parameters1.MaxExecutionTime = TimeSpan.FromMinutes(10);
            v_parameters1.LocalSearch = false;

            ParametersACO v_parameters2 = new ParametersACO();
            v_parameters2.NumberAnts = 50;
            v_parameters2.SetProbabilityParameters(0.3f, 1);
            v_parameters2.ParetoSetSize = 50;
            v_parameters2.EvaporationRate = 0.1F;
            v_parameters2.PheromoneDepositRate = 0.3F;
            v_parameters2.MaxGenerationsWithoutImprovement = 100;
            v_parameters2.MaxExecutionTime = TimeSpan.FromMinutes(10);
            v_parameters2.LocalSearch = true;

            var v_csvFile = new CsvExport();
            int v_instance = 1;
            for (int i_dest = 2; i_dest <= 4; i_dest++)
            {
                for (int i_transp = 1; i_transp <= 5; i_transp++)
                {
                    for (int i_acc = 1; i_acc <= 5; i_acc++)
                    {
                        Trip v_trip = Trip.GetRandomTrip(p_destinations: i_dest,
                            p_tranportationAvg: i_transp,
                            p_accommodationAvg: i_acc,
                            p_dbContext: this.db
                            );

                        int v_size = (int)(Utilities.Factorial(i_dest) * Math.Pow(i_transp, i_dest + 1)*Math.Pow(i_acc, i_dest));

                        AntColonyOptimizer v_optimizer1 = new AntColonyOptimizer();
                        v_optimizer1.Parameters = v_parameters1;

                        AntColonyOptimizer v_optimizer2 = new AntColonyOptimizer();
                        v_optimizer2.Parameters = v_parameters2;

                        double v_hypervolume1 = 0;
                        double v_hypervolume2 = 0;
                        double v_time1 = 0;
                        double v_time2 = 0;
                        for (int i_run = 1; i_run <= p_numExperiments; i_run++)
                        {
                            v_trip.DetermineSolutions(v_optimizer1);
                            v_trip.DetermineSolutions(v_optimizer2);

                            v_hypervolume1 = v_optimizer1.ParetoHypervolume;
                            v_hypervolume2 = v_optimizer2.ParetoHypervolume;
                            v_time1 = v_optimizer1.OptimizationTime;
                            v_time2 = v_optimizer2.OptimizationTime;

                            v_csvFile.AddRow();
                            v_csvFile["TripInstance"] = v_instance;
                            v_csvFile["Run"] = i_run;
                            v_csvFile["tripSize"] = v_size;
                            v_csvFile["avgTransportations"] = i_transp;
                            v_csvFile["avgAccommodations"] = i_acc;
                            v_csvFile["destinations"] = i_dest;
                            v_csvFile["algorithm"] = "ACO";
                            v_csvFile["quality"] = v_hypervolume1;
                            v_csvFile["time"] = v_time1;
                            
                            v_csvFile.AddRow();
                            v_csvFile["TripInstance"] = v_instance;
                            v_csvFile["Run"] = i_run;
                            v_csvFile["tripSize"] = v_size;
                            v_csvFile["avgTransportations"] = i_transp;
                            v_csvFile["avgAccommodations"] = i_acc;
                            v_csvFile["destinations"] = i_dest;
                            v_csvFile["algorithm"] = "ACO_Local";
                            v_csvFile["quality"] = v_hypervolume2;
                            v_csvFile["time"] = v_time2;
                        }
                        v_instance++;
                        //                        v_hypervolume1 = v_hypervolume1/p_numExperiments;
                        //                        v_hypervolume2 = v_hypervolume2/p_numExperiments;
                        //                        v_time1 = v_time1 / p_numExperiments;
                        //                        v_time2 = v_time2 / p_numExperiments;

                    }
                }
            }

            var v_file = File(v_csvFile.ExportToBytes(), "text/csv", "results.csv");

            return v_file;
        }

    }
}