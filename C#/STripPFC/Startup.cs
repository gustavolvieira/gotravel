﻿using System.Data.Entity;
using System.Data.SqlClient;
using Microsoft.Owin;
using Owin;
using STripPFC.Models;

[assembly: OwinStartupAttribute(typeof(STripPFC.Startup))]
namespace STripPFC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            STripDbContext v_StripDbContext = STripDbContext.Create();
            v_StripDbContext.Populate();

            ConfigureAuth(app);
        }
    }
}
