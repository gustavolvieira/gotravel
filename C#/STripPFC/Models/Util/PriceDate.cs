﻿using System;

namespace STripPFC.Models.Util
{
    public class PriceDate
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Money Price { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}