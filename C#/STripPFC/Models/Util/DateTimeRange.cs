﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.EntitySql;

namespace STripPFC.Models.Util
{
    public class DateTimeRange
    {
        public int Id { get; set; }
        public DateTime Initial { get; set; }
        public DateTime Final { get; set; }

        public static List<DateTime> GetDaysList(DateTime p_initialDate, DateTime p_finalDate)
        {
            int v_numDays = (int)Math.Ceiling((p_finalDate - p_initialDate).TotalDays);

            List<DateTime> v_dateTimeList = new List<DateTime>();
            for (int i = 0; i <= v_numDays; i++)
            {
                v_dateTimeList.Add(p_initialDate.AddDays(i));
            }
            return v_dateTimeList;
        }

        public int LengthDays => (this.Final.Date - this.Initial.Date).Days;
    }
}