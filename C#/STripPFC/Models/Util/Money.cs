﻿namespace STripPFC.Models.Util
{
    public class Money
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
    }
}
