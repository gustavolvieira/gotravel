﻿namespace STripPFC.Models.Util
{
    public class Utilities
    {
        public static int Factorial(int x)
        {
            if (x >= 2)
                return x * Factorial(x - 1);
            return 1;
        }
    }
}