﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace STripPFC.Models.Util
{
    public class DestinationDates
    {
        public Destination Destination { get; set; }
        public List<DateTimeRange> DateRanges { get; set; }
        public int MinStay { get; set; }
        public int MaxStay { get; set; }

        public DestinationDates()
        {
            this.DateRanges = new List<DateTimeRange>();
        }

        public DestinationDates(DestinationSpecification p_destinationSpecification)
        {
            this.Destination = p_destinationSpecification.Destination;
            this.MaxStay = p_destinationSpecification.MaxStayDays;
            this.MinStay = p_destinationSpecification.MinStayDays;
            this.DateRanges = new List<DateTimeRange>();
        }

        public void AddDateRange(DateTimeRange p_dateRange)
        {
            //new date already inside a existing date
            DateTimeRange v_DateWholeInside = this.DateRanges
                .Where(d => d.Initial.Date <= p_dateRange.Initial.Date)
                .Where(d => d.Final.Date >= p_dateRange.Final.Date)
                .SingleOrDefault();

            if (v_DateWholeInside != null)
                return;

            DateTimeRange v_DateInitialInside = this.DateRanges
                .Where(d => d.Initial.Date <= p_dateRange.Initial.Date)
                .Where(d => d.Final.Date < p_dateRange.Final.Date)
                .Where(d => d.Final.Date >= p_dateRange.Initial.Date)
                .SingleOrDefault();

            DateTimeRange v_DateFinalInside = this.DateRanges
                .Where(d => d.Initial.Date > p_dateRange.Initial.Date)
                .Where(d => d.Final.Date >= p_dateRange.Final.Date)
                .Where(d => d.Initial.Date <= p_dateRange.Final.Date)
                .SingleOrDefault();

            //new date connects two disjointed dates
            if (v_DateInitialInside != null && v_DateFinalInside != null)
            {
                v_DateInitialInside.Final = v_DateFinalInside.Final;
                this.DateRanges.Remove(v_DateFinalInside);
                return;
            }

            if (v_DateInitialInside != null)
            {
                v_DateInitialInside.Final = p_dateRange.Final;
                return;
            }

            if (v_DateFinalInside != null)
            {
                v_DateFinalInside.Initial = p_dateRange.Initial;
                return;
            }

            this.DateRanges.Add(p_dateRange);
        }
    }
}