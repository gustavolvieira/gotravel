﻿namespace STripPFC.Models.Util
{
    public class Location
    {
        public int Id { get; set; }
        public string City {get; set;}
        public string Street {get; set;}
        public string StateOrProvince {get; set;}
        public string Country {get; set;}
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public float? RadiusKm { get; set; }

        public virtual AccommodationRestriction AccommodationRestriction { get; set; }
    }
}
