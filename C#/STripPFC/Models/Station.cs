﻿using STripPFC.Models.Util;

namespace STripPFC.Models
{
    public class Station
    {
        public int Id { get; set; }
        public string StationName { get; set; }
        public string Code { get; set; }
        public Location StationLocation { get; set; }
    }
}