﻿using System.Collections.Generic;
using Microsoft.Ajax.Utilities;

namespace STripPFC.Models.TripOptimizers
{
    public interface ITripOptimizer
    {
        List<TripSolution> Optimize(Trip p_trip);
    }
}

