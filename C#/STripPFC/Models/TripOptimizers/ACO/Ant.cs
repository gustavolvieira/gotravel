﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using STripPFC.Models.Util;

namespace STripPFC.Models.TripOptimizers.ACO
{
    [NotMapped]
    public class Ant
    {
        private Trip trip { get; }
        private List<SolutionComponent> accommodationOptions { get; }
        private List<SolutionComponent> transportationOptions { get; }
//        private TripSolution currentSolution { get; set; }

        public Ant(Trip p_trip, List<AccommodationOption> p_accommodationOptions, List<TransportationOption> p_transportationOptions)
        {
            List<SolutionComponent> v_accomodationComponents = new List<SolutionComponent>();
            v_accomodationComponents.AddRange(p_accommodationOptions);

            List<SolutionComponent> v_transportationComponents = new List<SolutionComponent>();
            v_transportationComponents.AddRange(p_transportationOptions);

            this.trip = p_trip;
            this.accommodationOptions = v_accomodationComponents;
            this.transportationOptions = v_transportationComponents;
        }

        public TripSolution BuildSolution()
        {
            TripSolution v_currentSolution = new TripSolution(
                p_trip: this.trip,
                p_accomodationOptions: this.accommodationOptions,
                p_transportationOptions: this.transportationOptions
            );

            while (!v_currentSolution.IsCompleteSolution(this.trip))
            {
                List<SolutionComponent> v_feasibleComponents = new List<SolutionComponent>();

                v_feasibleComponents.AddRange(v_currentSolution.GetFeasibleComponents());

                if (v_feasibleComponents.Count < 1)
                {
                    v_currentSolution.UnfeasibleSolution = true;
                    break;
                }

                v_currentSolution.SolutionComponents.Add(ChooseNextStep(v_feasibleComponents));
            }

            v_currentSolution.EvalSolution();

            return v_currentSolution;
        }

        private static SolutionComponent ChooseNextStep(List<SolutionComponent> p_feasibleComponents)
        {
            List<double> whell = new List<double>(p_feasibleComponents.Count) {0};

            double v_probalitySum = p_feasibleComponents.Sum(component => component.ProbabilityValue);
            p_feasibleComponents.ForEach(i_component => whell.Add(whell.Last() + i_component.ProbabilityValue/v_probalitySum));

            if (whell.Last() < 1)
            {
                whell.RemoveAt(whell.Count-1);
                whell.Add(1);
            }

            double randomValue = RandomProvider.GetThreadRandom().NextDouble();
            int chosenIndex = whell.IndexOf(whell.Last(c => c < randomValue));

            return p_feasibleComponents.ElementAt(chosenIndex);
        }
    }
}
