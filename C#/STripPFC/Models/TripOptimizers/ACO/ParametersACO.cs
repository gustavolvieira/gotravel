﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace STripPFC.Models.TripOptimizers.ACO
{
    [NotMapped]
    public class ParametersACO
    {
        //TODO determine values
        public float EvaporationRate { get; set; }
        public float PheromoneDepositRate { get; set; }
        public float PheromoneDefault { get; set; }
        public int NumberAnts { get; set; }
        public int ParetoSetSize { get; set; }
        public float MinimumParetoImprovement { get; set; }
        public int MaxGenerationsWithoutImprovement { get; set; }
        public TimeSpan MaxExecutionTime { get; set; }
        public bool LocalSearch { get; set; }

        public Dictionary<string, float> PheromoneModifier { get; set; }
        public Dictionary<string, float> HeuristicModifier { get; set; }

        public ParametersACO()
        {
            this.EvaporationRate = 0.1f;
            this.PheromoneDepositRate = 0.2f;
            this.PheromoneDefault = 1;
            this.NumberAnts = 40;
            this.ParetoSetSize = 50;
            this.MinimumParetoImprovement = 1;
            this.MaxGenerationsWithoutImprovement = 100;
            this.MaxExecutionTime = TimeSpan.FromMinutes(2);
            this.LocalSearch = false;

            this.PheromoneModifier = new Dictionary<string, float>()
                {
//                    {Objectives.Comfort, 1},
                    {Objectives.Cost.ToString(), 0.01f},
                    {Objectives.Time.ToString(), 0.01f},
                };

            /*
                Heuristic value is always between 0 and 1
                - modifier = 0 => heuristic = 1, no effect
                - modifier = 1 => heuristic effect linear
                - modifier = 2 => heuristic effect quadratic, and so on
            */
            this.HeuristicModifier = new Dictionary<string, float>()
                {
//                    {Objectives.Comfort, 1},
                    {Objectives.Cost.ToString(), 0},
                    {Objectives.Time.ToString(), 0},
                };
        }

        public void SetProbabilityParameters(float p_pheromoneModifier, float p_Greediness)
        {
            PheromoneModifier[Objectives.Time.ToString()] = p_pheromoneModifier;
            PheromoneModifier[Objectives.Cost.ToString()] = p_pheromoneModifier;
            HeuristicModifier[Objectives.Time.ToString()] = p_Greediness;
            HeuristicModifier[Objectives.Cost.ToString()] = p_Greediness;
        }

        public void SetDeterministicGreedy()
        {
            this.SetProbabilityParameters(0, 99999);
        }

        public void SetRandomGreedy()
        {
            this.SetProbabilityParameters(0, 2);
        }

        public void SetTotalRandom()
        {
            this.SetProbabilityParameters(0, 0);
        }

        public void SetStandart()
        {
            this.SetProbabilityParameters(0.1f, 1);
        }
    }
}
