using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.Ajax.Utilities;
using STripPFC.Models.TripOptimizers.ACO;

namespace STripPFC.Models
{
    public abstract partial class SolutionComponent
    {
        [NotMapped]
        public HashSet<Destination> DestinationsSet => new HashSet<Destination>() {
            this.FinalDestination,
            this.InitialDestination };

        public ParametersACO Parameters { get; set; }

        [NotMapped]
        public Dictionary<string, float> PheromoneValues;
        [NotMapped]
        public Dictionary<string, float> HeuristicValues; 

        
        public float ProbabilityValue
        {
            get
            {
                float v_value = 1;
                foreach (string objectiveKey in this.PheromoneValues.Keys)
                {
                    v_value *= (float)Math.Pow(this.PheromoneValues[objectiveKey], Parameters.PheromoneModifier[objectiveKey]);
                    v_value *= (float)Math.Pow(this.HeuristicValues[objectiveKey], Parameters.HeuristicModifier[objectiveKey]);
                }

                return v_value;
            }
        }

        public void InitComponentACO(ParametersACO p_parametersACO)
        {
            this.Parameters = p_parametersACO;

            this.PheromoneValues = new Dictionary<string, float>();
            foreach (Objectives i_objective in Enum.GetValues(typeof(Objectives)))
            {
                this.PheromoneValues.Add(i_objective.ToString(), Parameters.PheromoneDefault);
            }
            this.HeuristicValues = new Dictionary<string, float>();
            foreach (Objectives i_objective in Enum.GetValues(typeof(Objectives)))
            {
                this.HeuristicValues.Add(i_objective.ToString(), this.HeuristicValue(i_objective));
            }
        }

        public float HeuristicValue(Objectives p_objective)
        {
            return Decimal.ToSingle(this.GetQualityValue(p_objective));
        }

        public void EvaporatePheromones()
        {
            List<string> keyList = this.PheromoneValues.Keys.ToList();
            foreach (string objectiveKey in keyList)
            {
               this.PheromoneValues[objectiveKey] = (1 - Parameters.EvaporationRate)* this.PheromoneValues[objectiveKey];
            }
        }

        public void UpdatePheromones(TripSolution p_tripSolution)
        {
            foreach (Objectives i_objective in Enum.GetValues(typeof(Objectives)))
            {
                this.PheromoneValues[i_objective.ToString()] += p_tripSolution.GetQualityValue(i_objective)*Parameters.PheromoneDepositRate;
            }
        }

    }
}