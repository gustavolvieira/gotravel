﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using STripPFC.Models.TripOptimizers.ACO;

namespace STripPFC.Models.TripOptimizers
{
    [NotMapped]
    public class AntColonyOptimizer : ITripOptimizer
    {
        public ParametersACO Parameters { get; set; }
        private List<Ant> AntAgents { get; set; }
        private List<AccommodationOption> AccommodationOptions { get; set; }
        private List<TransportationOption> TransportationOptions { get; set; }

        public Trip Trip { get; set; }
        public List<TripSolution> Solutions { get; set; }
        public float ParetoChangeRatio { get; set; }
        public double ParetoHypervolume { get; set; }
//        private float ParetoImprovement { get; set; }

        private int generationWithoutImprovement;

        private Stopwatch stopwatch = new Stopwatch();
        public double OptimizationTime;
        public int numberGenerations;

        public List<double> HypervolumeList;
        public List<double> IterationTimesList;

        public List<List<TripSolution>> SolutionsPartials = new List<List<TripSolution>>();



        public AntColonyOptimizer()
        {
            this.Parameters = new ParametersACO();
            this.AccommodationOptions = new List<AccommodationOption>();
            this.TransportationOptions = new List<TransportationOption>();
            this.AntAgents = new List<Ant>();
            this.Solutions = new List<TripSolution>();
            this.ParetoChangeRatio = 0;

            this.HypervolumeList = new List<double>();
            this.IterationTimesList = new List<double>();
            //            this.ParetoImprovement = 0;
        }

        private void InitializeOptimizer(Trip p_trip)
        {
            this.Trip = p_trip;
            this.AccommodationOptions = new List<AccommodationOption>();
            this.TransportationOptions = new List<TransportationOption>();
            this.Solutions = new List<TripSolution>();
            this.ParetoChangeRatio = 0;
//            this.ParetoImprovement = 0;
            this.generationWithoutImprovement = 0;
            this.numberGenerations = 0;
            this.AntAgents = new List<Ant>();
            this.stopwatch.Reset();

            this.TransportationOptions = p_trip.TransportationOptions;
            this.AccommodationOptions = p_trip.AccommodationOptions;
            this.TransportationOptions.ForEach(o => o.InitComponentACO(this.Parameters));
            this.AccommodationOptions.ForEach(o => o.InitComponentACO(this.Parameters));

            CreateAnts();
        }

        private void CreateAnts()
        {
            for (int index = 0; index < this.Parameters.NumberAnts; index++)
            {
                this.AntAgents.Add(new Ant(this.Trip, p_accommodationOptions:this.AccommodationOptions, p_transportationOptions:this.TransportationOptions));
            }
        }

        public List<TripSolution> Optimize(Trip p_trip)
        {
            InitializeOptimizer(p_trip);

            return Optimize();
        }

        public List<TripSolution> Optimize()
        {
            this.stopwatch.Restart();

            while (!this.StopConditionMet())
            {
                this.numberGenerations++;
                List<TripSolution> tripSolutions = this.AntAgents.Select(ant => ant.BuildSolution()).ToList();
                tripSolutions = tripSolutions.Where(ts => ts.UnfeasibleSolution == false).ToList();
                if (this.Parameters.LocalSearch == true)
                {
                    tripSolutions.ForEach(s => s.LocalSearch());
                }
                UpdatePheromones(tripSolutions);
                GetBestSolutions(tripSolutions);

                this.HypervolumeList.Add(TripSolution.CalculateHypervolume(this.Solutions, this.Trip.ParetoSolutions, p_nadirCost:this.Trip.NadirSolutionCost, p_nadirTime:this.Trip.NadirSolutionTime));
                this.IterationTimesList.Add(this.stopwatch.Elapsed.TotalSeconds);
            }

            this.stopwatch.Stop();
            this.OptimizationTime = this.stopwatch.Elapsed.TotalSeconds;

            this.ParetoHypervolume = TripSolution.CalculateHypervolume(this.Solutions, this.Trip.ParetoSolutions, p_nadirCost: this.Trip.NadirSolutionCost, p_nadirTime: this.Trip.NadirSolutionTime);

            return this.Solutions;
        }

        private bool StopConditionMet()
        {
            if (this.ParetoChangeRatio == 0)
                this.generationWithoutImprovement++;
            else
            {
                this.generationWithoutImprovement = 0;
            }

            return (this.generationWithoutImprovement > Parameters.MaxGenerationsWithoutImprovement ||
                    this.stopwatch.Elapsed > Parameters.MaxExecutionTime);
        }

        private void UpdatePheromones(List<TripSolution> p_tripSolutions)
        {
            this.AccommodationOptions.ForEach(component => component.EvaporatePheromones());
            this.TransportationOptions.ForEach(component => component.EvaporatePheromones());

            foreach (var i_tripSolution in p_tripSolutions)
            {
                i_tripSolution.SolutionComponents.ForEach(i_component => i_component.UpdatePheromones(i_tripSolution));
            }
        }

        private void GetBestSolutions(List<TripSolution> p_newSolutions)
        {
            HashSet<TripSolution> v_solutionSet = TripSolution.MergeSolutionsWithoutDuplicates(this.Solutions, p_newSolutions);

            v_solutionSet = new HashSet<TripSolution>(TripSolution.EvaluateDominance(v_solutionSet));

            //TODO calculate crowding distance

            this.Solutions = 
                v_solutionSet
                .OrderBy(solution => solution.DominanceRank)
                .ThenBy(solution => solution.CrowdingDistance)
                .Take(Parameters.ParetoSetSize)
                .ToList();

//            float v_evaluation = EvaluatePareto(this.ParetoSolutions, p_newSolutions);
//            this.ParetoImprovement = v_evaluation - this.ParetoChangeRate;
            this.ParetoChangeRatio = EvaluateParetoChanges(this.Solutions, p_newSolutions); ;

//            if (this.numberGenerations%(Parameters.MaxGenerationsWithoutImprovement/5) == 0)
//            {
//                this.SolutionsPartials.Add(this.Solutions);
//            }
        }

        private float EvaluateParetoChanges(List<TripSolution> p_Solutions, List<TripSolution> p_newSolutions)
        {
            //calculate how many new dominant solutions were added to the set
            float v_numberChanges = 0;

            List<string> v_solutionsIds = p_Solutions.Where(s => s.DominanceRank == 1).ToList().Select(s => s.SolutionComponentsIds).ToList();
            foreach (TripSolution i_solution in p_newSolutions.Where(s => s.DominanceRank == 1).ToList())
            {
                if (v_solutionsIds.Contains(i_solution.SolutionComponentsIds))
                {
                    v_numberChanges++;
                }
            }

            return v_numberChanges/this.Parameters.ParetoSetSize;
        }

    }
}
