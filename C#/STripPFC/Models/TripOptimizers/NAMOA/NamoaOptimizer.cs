﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using Deedle;
using Microsoft.Ajax.Utilities;

namespace STripPFC.Models.TripOptimizers
{
    [NotMapped]
    public class NamoaOptimizer : ITripOptimizer
    {
        private List<AccommodationOption> AccommodationOptions { get; set; }
        private List<TransportationOption> TransportationOptions { get; set; }

        public Trip Trip { get; set; }
        public List<TripSolution> Solutions { get; set; }
        public double ParetoHypervolume { get; set; }

        private List<TripSolution> SolutionQueue { get; set; }

        private Frame<string, string> MinCostFrame { get; set; }

        private Stopwatch stopwatch = new Stopwatch();
        public double OptimizationTime;
        public int numberGenerations = 1;

        public List<double> HypervolumeList;
        public List<double> IterationTimesList;

        public List<List<TripSolution>> SolutionsPartials = new List<List<TripSolution>>();



        public NamoaOptimizer()
        {
            this.AccommodationOptions = new List<AccommodationOption>();
            this.TransportationOptions = new List<TransportationOption>();
            this.Solutions = new List<TripSolution>();

        }

        private void InitializeOptimizer(Trip p_trip)
        {
            this.Trip = p_trip;
            this.AccommodationOptions = new List<AccommodationOption>();
            this.TransportationOptions = new List<TransportationOption>();
            this.Solutions = new List<TripSolution>();
            this.SolutionQueue = new List<TripSolution>();

            this.stopwatch.Reset();

            this.TransportationOptions = p_trip.TransportationOptions;
            this.AccommodationOptions = p_trip.AccommodationOptions;

            this.CalculateCostFrame();
        }

        private void CalculateCostFrame()
        {
            List<Destination> v_destinations =
                this.Trip.DestinationSpecifications.Select(spec => spec.Destination).ToList();
            v_destinations.Add(this.Trip.StartingDestination);

            var v_rows = Enumerable.Range(0, v_destinations.Count).Select(i => {
                Destination i_destination = v_destinations[i];
                var sb = new SeriesBuilder<string>();
                sb.Add("TransportationTime", (float)this.TransportationOptions.Where(t => t.FinalDestination.Name == i_destination.Name).Select(t => t.TimeValue).Min());
                sb.Add("TransportationCost", (float)this.TransportationOptions.Where(t => t.FinalDestination.Name == i_destination.Name).Select(t => t.CostValue).Min());
                if(i_destination.Name != this.Trip.StartingDestination.Name) { sb.Add("AccommodationCost", (float)this.AccommodationOptions.Where(a => a.FinalDestination.Name == i_destination.Name).Select(a => a.CostValue).Min()); }
                return KeyValue.Create(i_destination.Name, sb.Series);
            });

            this.MinCostFrame = Frame.FromRows(v_rows);
        }

        public List<TripSolution> Optimize(Trip p_trip)
        {
            InitializeOptimizer(p_trip);

            return Optimize();
        }

        public List<TripSolution> Optimize()
        {
            this.stopwatch.Restart();

            List<SolutionComponent> v_accomodationComponents = new List<SolutionComponent>();
            v_accomodationComponents.AddRange(this.AccommodationOptions);
            List<SolutionComponent> v_transportationComponents = new List<SolutionComponent>();
            v_transportationComponents.AddRange(this.TransportationOptions);

            this.SolutionQueue.Add(new TripSolution(
                p_trip: this.Trip,
                p_accomodationOptions: v_accomodationComponents,
                p_transportationOptions: v_transportationComponents
                )
            );

            while (this.SolutionQueue.Count > 0)
            {
//                TripSolution v_currentSolution = this.SolutionQueue.First();
                TripSolution v_currentSolution = this.GetNondominatedSolution();

                if (v_currentSolution.IsCompleteSolution(this.Trip))
                {
                    this.Solutions.Add(v_currentSolution);
                    this.EliminateDominatedSolutions();
                    this.Solutions = TripSolution.EvaluateDominance(this.Solutions).ToList();
                    this.Solutions.RemoveAll(solution => solution.DominanceRank > 0);
                }
                else
                {
                    List<TripSolution> v_nextSolutions = v_currentSolution.GetNextSolutions();
                    v_nextSolutions.ForEach(s => s.EvalSolutionHeuristic(this.MinCostFrame));
                    this.SolutionQueue.AddRange(v_nextSolutions);
                }

            }

            this.stopwatch.Stop();
            this.OptimizationTime = this.stopwatch.Elapsed.TotalSeconds;

            this.ParetoHypervolume = TripSolution.CalculateHypervolume(this.Solutions, this.Trip.ParetoSolutions, p_nadirCost: this.Trip.NadirSolutionCost, p_nadirTime: this.Trip.NadirSolutionTime);

            return this.Solutions;
        }

        private TripSolution GetNondominatedSolution()
        {
            foreach (TripSolution tripSolution in this.SolutionQueue)
            {
                int v_dominanceRank = 0;
                foreach (TripSolution comparisonSolution in this.SolutionQueue)
                {
                    bool v_dominated = true;
                    foreach (KeyValuePair<string, float> solutionObjective in tripSolution.ObjectiveValues)
                    {
                        if (solutionObjective.Value < comparisonSolution.ObjectiveValues[solutionObjective.Key])
                        {
                            v_dominated = false;
                            break;
                        }
                    }
                    if (v_dominated)
                    {
                        v_dominanceRank++;
                        break;
                    }
                }
                if (v_dominanceRank == 0)
                {
                    this.SolutionQueue.Remove(tripSolution);
                    return tripSolution;
                }
            }
            TripSolution v_solution = this.SolutionQueue.First();
            this.SolutionQueue.RemoveAt(0);
            return v_solution;
        }

        private void EliminateDominatedSolutions()
        {
            this.Solutions = TripSolution.EvaluateDominance(this.Solutions).Where(s => s.DominanceRank == 0).ToList();

            // eliminate from Queue solutions dominated by complete ones
            foreach (TripSolution tripSolution in this.SolutionQueue)
            {
                tripSolution.DominanceRank = 0; //number of solutions that dominate solution_i (including itself)
                foreach (TripSolution comparisonSolution in this.Solutions)
                {
                    bool v_dominated = true;
                    foreach (KeyValuePair<string, float> solutionObjective in tripSolution.ObjectiveValues)
                    {
                        if (solutionObjective.Value < comparisonSolution.ObjectiveValues[solutionObjective.Key])
                        {
                            v_dominated = false;
                            break;
                        }
                    }
                    if (v_dominated)
                        tripSolution.DominanceRank++;
                }
            }

            this.SolutionQueue.RemoveAll(s => s.DominanceRank > 0);
        }




        private void GetBestSolutions(List<TripSolution> p_newSolutions)
        {
            HashSet<TripSolution> v_solutionSet = TripSolution.MergeSolutionsWithoutDuplicates(this.Solutions, p_newSolutions);

            v_solutionSet = new HashSet<TripSolution>(TripSolution.EvaluateDominance(v_solutionSet));

            this.Solutions = 
                v_solutionSet
                .Where(solution => solution.DominanceRank == 0)
                .ToList();

        }

    }
}
