﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Deedle;
using Newtonsoft.Json;

namespace STripPFC.Models
{
    public class TripSolution
    {
        //Properties
        public int Id { get; set; }

        [NotMapped]
        public List<SolutionComponent> SolutionComponents { get; set; } //n-to-n
        public virtual string SolutionComponentsIds { get; set; }

        public int DominanceRank { get; set; }
        public float CrowdingDistance { get; set; }

        [NotMapped] public Dictionary<string, float> ObjectiveValues;
        [NotMapped] public Dictionary<string, float> ObjectiveValuesNormalized;
        public virtual string ObjectiveValuesString { get; set; } //TODO: set values

        public virtual Trip Trip { get; set; }

        [NotMapped] public bool UnfeasibleSolution = false;

        [NotMapped]
        private DateTime currentDate { get; set; }

        [NotMapped]
        private Destination currentDestination { get; set; }

        [NotMapped]
        public List<SolutionComponent> accomodationOptions { get; }
        [NotMapped]
        public List<SolutionComponent> transportationOptions { get; }

        //Methods
        public TripSolution(Trip p_trip, List<SolutionComponent> p_accomodationOptions,
            List<SolutionComponent> p_transportationOptions)
        {
            this.Trip = p_trip;
            this.SolutionComponents = new List<SolutionComponent>();
            this.accomodationOptions = p_accomodationOptions;
            this.transportationOptions = p_transportationOptions;
            this.ObjectiveValues = new Dictionary<string, float>();
            this.ObjectiveValuesNormalized = new Dictionary<string, float>();
            this.DominanceRank = 0;
            this.CrowdingDistance = 0;
        }

        public TripSolution()
        {
        }

        public void InitializeSolution(Trip p_trip)
        {
            this.Trip = p_trip;
            this.ObjectiveValues = new Dictionary<string, float>();
            if (this.ObjectiveValuesString != null)
            {
                this.ObjectiveValues[Objectives.Cost.ToString()] = float.Parse(this.ObjectiveValuesString.Split(',')[0]);
                this.ObjectiveValues[Objectives.Time.ToString()] = float.Parse(this.ObjectiveValuesString.Split(',')[1]);
            }
        }

        public void EvalSolution()
        {
            this.ObjectiveValues[Objectives.Cost.ToString()] = (float) this.SolutionComponents.Sum(c => c.CostValue);
            this.ObjectiveValues[Objectives.Time.ToString()] = (float) this.SolutionComponents.Sum(c => c.TimeValue);

            this.ObjectiveValuesNormalized[Objectives.Cost.ToString()] = (float) this.SolutionComponents.Sum(c => c.CostValueNormalized)/ this.SolutionComponents.Count;
            this.ObjectiveValuesNormalized[Objectives.Time.ToString()] = (float) this.SolutionComponents.Sum(c => c.TimeValueNormalized)/ this.SolutionComponents.Count;

            this.SolutionComponentsIds = String.Join(",", this.SolutionComponents.Select(c => c.Id).ToList());
            this.ObjectiveValuesString = this.ObjectiveValues[Objectives.Cost.ToString()].ToString() + ',' +
                                         this.ObjectiveValues[Objectives.Time.ToString()];
        }

        public void EvalSolutionHeuristic(Frame<string, string> p_minCostFrame)
        {
            this.ObjectiveValues[Objectives.Cost.ToString()] = (float)this.SolutionComponents.Sum(c => c.CostValue);
            this.ObjectiveValues[Objectives.Time.ToString()] = (float)this.SolutionComponents.Sum(c => c.TimeValue);

            if (!this.IsCompleteSolution(this.Trip))
            {
                List<Destination> v_missingDestinationsAccommodations = new List<Destination>();
                List<Destination> v_missingDestinationsTransportations = new List<Destination>();
                v_missingDestinationsAccommodations.AddRange(this.Trip.DestinationSpecifications.Select(spec => spec.Destination).ToList());
                v_missingDestinationsTransportations.AddRange(this.Trip.DestinationSpecifications.Select(spec => spec.Destination).ToList());

                if (this.Trip.RoundTrip)
                {
                    v_missingDestinationsTransportations.Add(this.Trip.StartingDestination);
                }

                foreach (SolutionComponent i_component in this.SolutionComponents)
                {
                    if (i_component.GetType() == typeof(TransportationOption))
                    {
                        v_missingDestinationsTransportations.Remove(i_component.FinalDestination);
                    }
                    else
                    {
                        v_missingDestinationsAccommodations.Remove(i_component.FinalDestination);
                    }
                }

                foreach (Destination i_destination in v_missingDestinationsTransportations)
                {
                    this.ObjectiveValues[Objectives.Time.ToString()] += p_minCostFrame.GetColumn<float>("TransportationTime")[i_destination.Name];
                    this.ObjectiveValues[Objectives.Cost.ToString()] += p_minCostFrame.GetColumn<float>("TransportationCost")[i_destination.Name];
                }
                foreach (Destination i_destination in v_missingDestinationsAccommodations)
                {
                    this.ObjectiveValues[Objectives.Cost.ToString()] += p_minCostFrame.GetColumn<float>("AccommodationCost")[i_destination.Name];
                }
            }
            
            
            this.ObjectiveValuesNormalized[Objectives.Cost.ToString()] = (float)this.SolutionComponents.Sum(c => c.CostValueNormalized) / this.SolutionComponents.Count;
            this.ObjectiveValuesNormalized[Objectives.Time.ToString()] = (float)this.SolutionComponents.Sum(c => c.TimeValueNormalized) / this.SolutionComponents.Count;

            this.SolutionComponentsIds = String.Join(",", this.SolutionComponents.Select(c => c.Id).ToList());
            this.ObjectiveValuesString = this.ObjectiveValues[Objectives.Cost.ToString()].ToString() + ',' +
                                         this.ObjectiveValues[Objectives.Time.ToString()];
        }

        public float GetQualityValue(Objectives p_objective)
        {
            return Math.Abs(this.ObjectiveValuesNormalized[p_objective.ToString()] - 1);
        }

        public List<TripSolution> GetNextSolutions()
        {
            List<TripSolution> v_nextSolutions = new List<TripSolution>();

            foreach (SolutionComponent i_component in this.GetFeasibleComponents())
            {
                TripSolution i_solution = new TripSolution(
                    p_trip: this.Trip,
                    p_accomodationOptions: this.accomodationOptions,
                    p_transportationOptions: this.transportationOptions
                    );

                i_solution.SolutionComponents.AddRange(this.SolutionComponents);
                i_solution.SolutionComponents.Add(i_component);

                v_nextSolutions.Add(i_solution);
            }

            return v_nextSolutions;
        }

        public List<SolutionComponent> GetFeasibleComponents()
        {
            this.GetCurrentDestination();
            if (this.SolutionComponents.Count == 0)
            {
                List<SolutionComponent> v_feasibleComponents = new List<SolutionComponent>();
                int v_startDateRangeLength = (this.Trip.StartDateRange.Final - this.Trip.StartDateRange.Initial).Days;
                for (int i = 0; i <= v_startDateRangeLength; i++)
                {
                    this.currentDate = this.Trip.StartDateRange.Initial.AddDays(i);
                    v_feasibleComponents.AddRange(this.GetFeasibleTransportations());
                }
                return v_feasibleComponents;
            }
            else
            {
                this.currentDate = this.SolutionComponents.Last().FinalDate;

                if (this.SolutionComponents.Last().GetType() == typeof (TransportationOption))
                {
                    return this.GetFeasibleAccommodations();
                }
                else
                {
                    return this.GetFeasibleTransportations();
                }
            }
        }

        private List<SolutionComponent> GetFeasibleAccommodations()
        {
            List<SolutionComponent> feasibleAccommodations =
                (
                    from accomodationOption in this.accomodationOptions
                    where accomodationOption.InitialDate.Date == this.currentDate.Date
                    where accomodationOption.FinalDestination.Id == this.currentDestination.Id
                    select accomodationOption
                    ).ToList();

            return feasibleAccommodations;
        }

        private List<SolutionComponent> GetFeasibleTransportations()
        {
            List<int> visitedDestinationsIds =
                this.SolutionComponents.Select(component => component.FinalDestination.Id).ToList();

            HashSet<int> visitedDestinationsIdsSet = new HashSet<int>(visitedDestinationsIds);

            int numberDestinationsVisited = visitedDestinationsIdsSet.Count;
            if (!(this.Trip.RoundTrip && numberDestinationsVisited == this.Trip.DestinationSpecifications.Count))
            {
                visitedDestinationsIdsSet.Add(this.Trip.StartingDestination.Id);
            }

            List<SolutionComponent> feasibleTransportations =
                (
                    from transportation in this.transportationOptions
                    where transportation.InitialDate.Date == this.currentDate.Date
                    where transportation.InitialDestination.Id == this.currentDestination.Id
                    where !visitedDestinationsIdsSet.Contains(transportation.FinalDestination.Id)
                    let nextDestinationSpecification =
                        this.Trip.DestinationSpecifications.SingleOrDefault(
                            specification => specification.Destination.Id == transportation.FinalDestination.Id)
                    where (
                        nextDestinationSpecification?.AbsoluteOrder == null ||
                        nextDestinationSpecification.AbsoluteOrder == numberDestinationsVisited + 1
                        )
                    where (
                        nextDestinationSpecification?.RelativeOrderIds == null ||
                        visitedDestinationsIdsSet.IsSupersetOf(nextDestinationSpecification.RelativeOrderIds)
                        )
                    select transportation
                    ).ToList();

            return feasibleTransportations;
        }

        private void GetCurrentDestination()
        {
            if (this.SolutionComponents.Count == 0)
            {
                this.currentDestination = this.Trip.StartingDestination;
            }
            else
            {
                this.currentDestination =
                    this.Trip.DestinationSpecifications
                        .Select(specification => specification.Destination)
                        .First(destination => destination.Id == this.SolutionComponents.Last().FinalDestination.Id);
            }
        }

        public bool IsCompleteSolution(Trip p_trip)
        {
            //IsCompleteSolution only checks size, not correctness
            //It assumes that solution components have "empty" components when no accomodation or transportation is needed
            return this.SolutionComponents.Count ==
                   (p_trip.DestinationSpecifications.Count*2 + (p_trip.RoundTrip ? 1 : 0));
        }

//        public bool IsDuplicate(TripSolution p_comparisonSolution)
//        {
//            if (this != p_comparisonSolution && this.SolutionComponentsIds == p_comparisonSolution.SolutionComponentsIds)
//                return true;
//            else
//                return false;
//        }

        public static HashSet<TripSolution> MergeSolutionsWithoutDuplicates(ICollection<TripSolution> p_solutionSet1, ICollection<TripSolution> p_solutionSet2)
        {
            HashSet<TripSolution> v_solutionSet = new HashSet<TripSolution>(p_solutionSet1);
            v_solutionSet.UnionWith(p_solutionSet2);

            //remove duplicates
            HashSet<TripSolution> v_duplicateSet = new HashSet<TripSolution>();
            foreach (TripSolution tripSolution in v_solutionSet)
            {
                if (v_duplicateSet.Contains(tripSolution)) continue;

                foreach (TripSolution comparisonSolution in v_solutionSet)
                {
                    if (tripSolution != comparisonSolution && tripSolution.SolutionComponentsIds == comparisonSolution.SolutionComponentsIds)
                    {
                        v_duplicateSet.Add(comparisonSolution);
                    }
                }
            }
            v_solutionSet.ExceptWith(v_duplicateSet);

            return v_solutionSet;
        }

        public static ICollection<TripSolution> EvaluateDominance(ICollection<TripSolution> p_tripSolutions)
        {
            foreach (TripSolution tripSolution in p_tripSolutions)
            {
                tripSolution.DominanceRank = 0; //number of solutions that dominate solution_i (including itself)
                tripSolution.CrowdingDistance = 0;
                foreach (TripSolution comparisonSolution in p_tripSolutions)
                {
                    bool v_dominated = true;
                    foreach (KeyValuePair<string, float> solutionObjective in tripSolution.ObjectiveValues)
                    {
                        if (solutionObjective.Value < comparisonSolution.ObjectiveValues[solutionObjective.Key])
                        {
                            v_dominated = false;
                            break;
                        }
                    }
                    if (v_dominated)
                        tripSolution.DominanceRank++;
                }
            }

            return p_tripSolutions;
        }

//        public static double CalculateHypervolume(List<TripSolution> p_solutions, List<TripSolution> p_referenceSolutions)
//        {
//            List<TripSolution> v_hypervolumeSolutions = p_solutions.Where(s => s.DominanceRank == 1).OrderByDescending(s => s.ObjectiveValues["Time"]).ToList();
//            //treatment for cases when solutionsSet have best parameters than reference
//            HashSet<TripSolution> v_solutionSet = TripSolution.MergeSolutionsWithoutDuplicates(p_solutions, p_referenceSolutions);
//            v_solutionSet = new HashSet<TripSolution>(TripSolution.EvaluateDominance(v_solutionSet));
//            v_solutionSet = new HashSet<TripSolution>(v_solutionSet.Where(s => s.DominanceRank == 1));
//
//            double v_dystopianCost = (1.1 * v_solutionSet.Max(s => s.ObjectiveValues["Cost"]));
//            double v_dystopianTime = (1.1 * v_solutionSet.Max(s => s.ObjectiveValues["Time"]));
//
//            double v_hypervolume = 0;
//            double v_refTime = v_dystopianTime;
//
//            foreach (TripSolution i_solution in v_hypervolumeSolutions)
//            {
//                v_hypervolume += (v_dystopianCost - i_solution.ObjectiveValues["Cost"]) *
//                                 (v_refTime - i_solution.ObjectiveValues["Time"]);
//
//                v_refTime = i_solution.ObjectiveValues["Time"];
//            }
//
//            return v_hypervolume;
//        }

        public static double CalculateHypervolume(List<TripSolution> p_solutions, List<TripSolution> p_referenceSolutions, double p_nadirTime, double p_nadirCost)
        {
            List<TripSolution> v_hypervolumeSolutions = p_solutions.Where(s => s.DominanceRank == 1).OrderByDescending(s => s.ObjectiveValues["Time"]).ToList();

            double v_dystopianCost = p_nadirCost;
            double v_dystopianTime = p_nadirTime;

            double v_hypervolume = 0;
            double v_refTime = v_dystopianTime;

            foreach (TripSolution i_solution in v_hypervolumeSolutions)
            {
                v_hypervolume += (v_dystopianCost - i_solution.ObjectiveValues["Cost"]) *
                                 (v_refTime - i_solution.ObjectiveValues["Time"]);

                v_refTime = i_solution.ObjectiveValues["Time"];
            }

            v_hypervolume = v_hypervolume/(p_nadirCost*p_nadirTime);

            return v_hypervolume;
        }

        public TripSolution LocalSearch()
        {
            for (int i = 0; i < this.SolutionComponents.Count; i++)
            {
                SolutionComponent v_component = this.SolutionComponents[i];

                if (v_component is AccommodationOption)
                {
                    List<SolutionComponent> v_betterAccommodations =
                    (
                    from accomodationOption in this.accomodationOptions
                    where accomodationOption.InitialDate.Date == v_component.InitialDate.Date
                    where accomodationOption.FinalDate.Date == v_component.FinalDate.Date
                    where accomodationOption.FinalDestination.Id == v_component.FinalDestination.Id
                    where (accomodationOption.CostValue < v_component.CostValue)
                    select accomodationOption
                    ).ToList();

                    if (v_betterAccommodations.Count > 0)
                    {
                        Random v_rnd = new Random();
                        this.SolutionComponents[i] = v_betterAccommodations[v_rnd.Next(v_betterAccommodations.Count)];
                    }
                }
                else
                {
                    List<SolutionComponent> v_betterTransportations =
                    (
                    from transportationOption in this.transportationOptions
                    where transportationOption.InitialDate.Date == v_component.InitialDate.Date
                    where transportationOption.FinalDate.Date == v_component.FinalDate.Date
                    where transportationOption.FinalDestination.Id == v_component.FinalDestination.Id
                    where (transportationOption.CostValue <= v_component.CostValue) & 
                    (transportationOption.TimeValue <= v_component.TimeValue) & 
                    (transportationOption.TimeValue != v_component.TimeValue || transportationOption.CostValue != v_component.CostValue)
                    select transportationOption
                    ).ToList();

                    if (v_betterTransportations.Count > 0)
                    {
                        Random v_rnd = new Random();
                        this.SolutionComponents[i] = v_betterTransportations[v_rnd.Next(v_betterTransportations.Count)];
                    }
                }
            }
            this.EvalSolution();
            return this;
        }

    }
}
