﻿using System;
using System.Collections.Generic;

namespace STripPFC.Models
{
    public class TransportationBranch
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public DateTime DepartingDateTime { get; set; }
        public DateTime ArrivingDateTime { get; set; }
        public Station DepartingStation { get; set; }
        public Station ArrivingStation { get; set; }
        public TransportationType Type { get; set; }
        public Destination StartingDestination { get; set; }
        public Destination FinalDestination { get; set; }
        public string Company { get; set; }

        //nav property to TransportationOption
        public virtual ICollection<TransportationOption> TransportationOptionsCollection { get; set; }
    }
}