﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using STripPFC.Models.Util;

namespace STripPFC.Models
{
    public class TransportationOption  : SolutionComponent
    {
        public Money Price { get; set; }
        public List<TransportationBranch> BranchesList { get; set; }//n-to-n

        //TODO make gets based on BranchesList
        //public DateTime DepartingDateTime { get; set; }
        //public DateTime ArrivingDateTime { get; set; }
        //public Station DepartingStation { get; set; }
        //public Station ArrivingStation { get; set; }
        //public TransportationType Type { get; set; }
        //public string Company { get; set; }

        public override Destination FinalDestination => this.BranchesList.Last().FinalDestination;
        public override Destination InitialDestination => this.BranchesList.First().StartingDestination;

        public override decimal CostValue => this.Price.Amount;

        public override decimal TimeValue
        {
            get
            {
                return (decimal)(this.BranchesList.Last().ArrivingDateTime - this.BranchesList.First().DepartingDateTime).TotalMinutes;
            }
        }

        public override DateTime FinalDate => this.BranchesList.Last().ArrivingDateTime;
        public override DateTime InitialDate => this.BranchesList.First().DepartingDateTime;
    }
}
