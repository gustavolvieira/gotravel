﻿namespace STripPFC.Models
{
    public enum TransportationType
    {
        Plane,
        Bus,
        Train
    }
}