﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Combinatorics.Collections;
using STripPFC.Models.TripOptimizers;
using STripPFC.Models.Util;
using STripPFC.Models.WebFetchers;
using System.Data.Entity;
using System.Data.OleDb;
using Microsoft.Ajax.Utilities;

namespace STripPFC.Models
{
    public class Trip
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public User User { get; set; }
        public HashSet<DestinationSpecification> DestinationSpecifications { get; set; }//n-to-1
        public DateTimeRange StartDateRange { get; set; }
        public DateTimeRange EndDateRange { get; set; }
        public bool RoundTrip { get; set; }
        public Destination StartingDestination { get; set; }
        public virtual List<TripSolution> TripSolutions { get; set; }//n-to-1

        [NotMapped]
        public List<TripSolution> ParetoSolutions { get; set; }//n-to-1
        [NotMapped]
        public double ParetoHypervolume { get; set; }

        // temp
        [NotMapped]
        public int nTransportations { get; set; }
        [NotMapped]
        public int nAccommodations { get; set; }

        // temp
        [NotMapped]
        public double NadirSolutionCost { get; set; }
        [NotMapped]
        public double NadirSolutionTime { get; set; }


        public AccommodationRestriction AccommodationRestriction { get; set; }
        public TransportationRestriction TransportationRestriction { get; set; }

        [NotMapped]
        public List<TransportationOption> TransportationOptions { get; set; }//n-to-1 ??
        [NotMapped]
        public List<AccommodationOption> AccommodationOptions { get; set; }//n-to-1 ??

        [NotMapped]
        public STripDbContext DbContext;

        private struct DestinationTimeRange
        {
            public Destination Destination { get; set; }
            public DateTime DateStartMin { get; set; }
            public DateTime DateStartMax { get; set; }
            public DateTime DateEndMin { get; set; }
            public DateTime DateEndMax { get; set; }
            public int minStay { get; set; }
            public int maxStay { get; set; }
        }
        

        public Trip()
        {
            this.TransportationOptions = new List<TransportationOption>();
            this.AccommodationOptions = new List<AccommodationOption>();
        }

        public void DetermineSolutions(ITripOptimizer p_optimizer, bool p_onlyPareto = false)
        {
            List<TripSolution> v_resultSolutions = p_optimizer.Optimize(this);

            HashSet<TripSolution> v_solutionSet = TripSolution.MergeSolutionsWithoutDuplicates(v_resultSolutions, v_resultSolutions);
//            HashSet<TripSolution> v_solutionSet = TripSolution.MergeSolutionsWithoutDuplicates(v_resultSolutions, this.ParetoSolutions);

            this.ParetoSolutions = TripSolution.EvaluateDominance(v_solutionSet).Where(s => s.DominanceRank == 1).ToList();
            this.ParetoHypervolume = TripSolution.CalculateHypervolume(this.ParetoSolutions, this.ParetoSolutions, p_nadirCost:this.NadirSolutionCost, p_nadirTime:this.NadirSolutionTime);

            this.TripSolutions = v_solutionSet.ToList();

            if(p_onlyPareto)
                this.TripSolutions = this.ParetoSolutions;
        }

        public static Trip GetCompleteTrip(int p_id, STripDbContext p_dbContext)
        {
            var v_trip = p_dbContext.Trips
                .Include(t => t.AccommodationRestriction)
                .Include(t => t.DestinationSpecifications.Select(ds => ds.Destination))
                .Include(t => t.DestinationSpecifications.Select(ds => ds.DateInterval))
                .Include(t => t.EndDateRange)
                .Include(t => t.StartDateRange)
                .Include(t => t.StartingDestination)
                .Include(t => t.TransportationRestriction)
                .Include(t => t.TripSolutions)
                .Include(t => t.User)
                .Single(t => t.Id == p_id);

            v_trip.TripSolutions.ForEach(s => s.InitializeSolution(v_trip));

            v_trip.ParetoSolutions = v_trip.TripSolutions.Where(s => s.DominanceRank == 1).ToList();
//            if (v_trip.ParetoSolutions.Count > 0)
//                v_trip.ParetoHypervolume = TripSolution.CalculateHypervolume(v_trip.ParetoSolutions, v_trip.ParetoSolutions);

            v_trip.DbContext = p_dbContext;

            return v_trip;
        }


        public void FetchOptions(STripDbContext p_db)
        {
            this.DbContext = p_db;

            List<DestinationDates> v_destinationDatesList = new List<DestinationDates>();
            foreach (DestinationSpecification i_destinationSpecification in this.DestinationSpecifications)
            {
                v_destinationDatesList.Add(new DestinationDates(i_destinationSpecification));
            }

            List<List<DestinationTimeRange>> DestinationRangeSequencesList = this.CalculateFeasibleDestinationsSequences();
            foreach (List<DestinationTimeRange> i_destinationTimeRangeList in DestinationRangeSequencesList)
            {
                this.FetchTransportations(i_destinationTimeRangeList);

                foreach (DestinationTimeRange i_destinationTimeRange in i_destinationTimeRangeList)
                {
                    DestinationDates v_destinationDate =
                        v_destinationDatesList.Single(dd => dd.Destination == i_destinationTimeRange.Destination);
                    v_destinationDate.AddDateRange(new DateTimeRange()
                     {
                         Initial = i_destinationTimeRange.DateStartMin,
                         Final = i_destinationTimeRange.DateEndMax
                     });

                }
//                this.FetchAccommodations(i_destinationTimeRangeList);
            }
            this.FetchAccommodations(v_destinationDatesList);

            decimal v_MaxCostValue = Math.Max(this.AccommodationOptions.Select(o => o.CostValue).ToList().Max(), this.TransportationOptions.Select(o => o.CostValue).ToList().Max());
            decimal v_MaxTimeValue = Math.Max(this.AccommodationOptions.Select(o => o.TimeValue).ToList().Max(), this.TransportationOptions.Select(o => o.TimeValue).ToList().Max());

            this.AccommodationOptions.ForEach(o => o.CostValueNormalized = o.CostValue/v_MaxCostValue);
            this.TransportationOptions.ForEach(o => o.CostValueNormalized = o.CostValue/v_MaxCostValue);

            this.AccommodationOptions.ForEach(o => o.TimeValueNormalized = o.TimeValue/v_MaxTimeValue);
            this.TransportationOptions.ForEach(o => o.TimeValueNormalized = o.TimeValue/v_MaxTimeValue);

            this.NadirSolutionCost =
                Convert.ToDouble(
                this.TransportationOptions.Select(o => o.CostValue)
                    .ToList()
                    .OrderByDescending(i => i)
                    .Take(this.DestinationSpecifications.Count + 1)
                    .Sum() +
                this.AccommodationOptions.Select(o => o.CostValue)
                    .ToList()
                    .OrderByDescending(i => i)
                    .Take(this.DestinationSpecifications.Count)
                    .Sum()
                    );

            this.NadirSolutionTime  =
                Convert.ToDouble(
                this.TransportationOptions.Select(o => o.TimeValue)
                    .ToList()
                    .OrderByDescending(i => i)
                    .Take(this.DestinationSpecifications.Count + 1)
                    .Sum() +
                this.AccommodationOptions.Select(o => o.TimeValue)
                    .ToList()
                    .OrderByDescending(i => i)
                    .Take(this.DestinationSpecifications.Count)
                    .Sum()
                    );

//            this.AccommodationOptions.ForEach(o => o.CostValue = o.CostValue / v_nadirSolutionCost);
//            this.TransportationOptions.ForEach(o => o.CostValue = o.CostValue / v_nadirSolutionCost);
//
//            this.TransportationOptions.ForEach(o => o.TimeValue = o.TimeValue / this.NadirSolutionTime);
        }

        private List<List<DestinationTimeRange>> CalculateFeasibleDestinationsSequences()
        {
            var v_permutationIds = new List<int>();
            var v_absoluteOrderSpecifications = new Dictionary<int, int>();//key = order, value = specificationId

            foreach (DestinationSpecification i_specification in this.DestinationSpecifications)
            {
                if (i_specification.AbsoluteOrder == null)
                    v_permutationIds.Add(i_specification.Id);
                else
                    v_absoluteOrderSpecifications[(int)i_specification.AbsoluteOrder] = i_specification.Id;
            }

            var v_permutations = new Permutations<int>(v_permutationIds);

            List<List<DestinationTimeRange>> v_destinationsSequencesList = new List<List<DestinationTimeRange>>();

            foreach (var i_permutation in v_permutations)
            {
                List<DestinationTimeRange> v_destinationSequence = new List<DestinationTimeRange>();
                bool v_invalidPermutation = false;

                //absolute order insertion
                foreach (KeyValuePair<int, int> item in v_absoluteOrderSpecifications)
                {
                    i_permutation.Insert(item.Key-1, item.Value);
                }

                //relative order test
                HashSet<int> v_visitedDestinationsIds = new HashSet<int>();
                foreach (int i_specificationId in i_permutation)
                {
                    DestinationSpecification v_specification =
                        this.DestinationSpecifications.Single(ds => ds.Id == i_specificationId);

                    v_visitedDestinationsIds.Add(v_specification.Destination.Id);
                    if (v_specification.RelativeOrderIds != null && !v_visitedDestinationsIds.IsSupersetOf(v_specification.RelativeOrderIds))
                    {
                        v_invalidPermutation = true;
                        break;
                    }
                }
                if (v_invalidPermutation)
                    continue;

                DateTime v_currentDateMin = this.StartDateRange.Initial;
                DateTime v_currentDateMax = this.StartDateRange.Final;
                foreach (int i_specificationId in i_permutation)
                {
                    DestinationSpecification v_specification =
                        this.DestinationSpecifications.Single(ds => ds.Id == i_specificationId);

                    DestinationTimeRange v_destinationTimerange = new DestinationTimeRange();
                    v_destinationTimerange.Destination = v_specification.Destination;
                    v_destinationTimerange.DateStartMin = v_currentDateMin;
                    v_destinationTimerange.DateStartMax = v_currentDateMax;
                    v_destinationTimerange.DateEndMin = v_currentDateMin.AddDays(v_specification.MinStayDays);
                    v_destinationTimerange.DateEndMax = v_currentDateMax.AddDays(v_specification.MaxStayDays);
                    v_destinationTimerange.minStay = v_specification.MinStayDays;
                    v_destinationTimerange.maxStay = v_specification.MaxStayDays;

                    //test if date range fits on min-max dates
                    if (v_specification.DateInterval != null &&
                        (v_specification.DateInterval.Initial < v_destinationTimerange.DateStartMin ||
                        v_specification.DateInterval.Final > v_destinationTimerange.DateEndMax))
                    {
                        v_invalidPermutation = true;
                        break;
                    }

                    v_destinationSequence.Add(v_destinationTimerange);

                    v_currentDateMin = v_destinationTimerange.DateEndMin;
                    v_currentDateMax = v_destinationTimerange.DateEndMax;
                }
                if (v_invalidPermutation)
                    continue;
                else
                {
                    v_destinationsSequencesList.Add(v_destinationSequence);
                }

            }
            
            return v_destinationsSequencesList;
        }


        private void FetchTransportations(List<DestinationTimeRange> p_destinationTimeRangeList)
        {
            //From initial to first
            List<DateTime> v_dates = DateTimeRange.GetDaysList(this.StartDateRange.Initial,
                this.StartDateRange.Final);
            foreach (DateTime v_date in v_dates)
            {
                this.GetTransportationOptions(this.StartingDestination, p_destinationTimeRangeList.First().Destination, v_date);
            }


            //From last to initial
            if (this.RoundTrip)
            {
                v_dates = DateTimeRange.GetDaysList(p_destinationTimeRangeList.Last().DateEndMin,
                p_destinationTimeRangeList.Last().DateEndMax);
                foreach (DateTime v_date in v_dates)
                {
                    this.GetTransportationOptions(p_destinationTimeRangeList.Last().Destination, this.StartingDestination, v_date);
                }
            }

            //Intermediate transportations
            for (int i = 0; i < p_destinationTimeRangeList.Count-1; i++)
            {
                DestinationTimeRange i_destinationTimeRange = p_destinationTimeRangeList[i];
                Destination i_nextDestination = p_destinationTimeRangeList[i+1].Destination;
                v_dates = DateTimeRange.GetDaysList(i_destinationTimeRange.DateEndMin,
                    i_destinationTimeRange.DateEndMax);
                foreach (DateTime v_date in v_dates)
                {
                    this.GetTransportationOptions(i_destinationTimeRange.Destination, i_nextDestination, v_date);
                }
            }
        }

        private void FetchAccommodations(List<DestinationDates> p_destinationDatesList)
        {
            foreach (DestinationDates i_destinationDates in p_destinationDatesList)
            {
                foreach (DateTimeRange i_dateTimeRange in i_destinationDates.DateRanges)
                {
                    this.GetAccommodationOptions(i_destinationDates.Destination, i_dateTimeRange, i_destinationDates.MinStay, i_destinationDates.MaxStay);
                }
            }
        }

        private void GetAccommodationOptions(Destination p_destination, DateTimeRange p_dateRange, int p_minStay, int p_maxStay)
        {
//            //TODO: check if database has updated info on the pair destination-dateRange
//            var v_optionsDb = this.DbContext.AccommodationOptions
//                .Join(
//                    this.DbContext.SolutionComponents,
//                    o => o.Id, sc => sc.Id,
//                    (o, sc) => new
//                    {
//                        Id = sc.Id,
//                        FinalDestination = sc.FinalDestination,
//                        InitialDestination = sc.InitialDestination,
//                        InitialDate = sc.InitialDate,
//                        FinalDate = sc.FinalDate,
//                        CostValue = sc.CostValue,
//                        TimeValue = sc.TimeValue,
//                        Price = o.Price,
//                        UpdatedAt = sc.UpdatedAt,
//                        CreatedAt = sc.CreatedAt,
//                        IsEmpty = sc.IsEmpty,
//                        Accomodation = o.Accomodation,
//                        TimeRange = o.TimeRange
//                    }
//                )
//                .Where(o => o.InitialDestination.Id == p_destination.Id)
//                .Where(o => DbFunctions.TruncateTime(o.InitialDate)>= p_dateRange.Initial.Date)
//                .Where(o => DbFunctions.TruncateTime(o.FinalDate)<= p_dateRange.Final.Date)
//                .Where(o => DbFunctions.DiffDays(o.InitialDate, o.FinalDate) >= p_minStay)
//                .Where(o => DbFunctions.DiffDays(o.InitialDate, o.FinalDate) <= p_maxStay)
//                .ToList();
//
//
            List<AccommodationOption> v_optionsList = new List<AccommodationOption>();
//
//            v_optionsDb.ForEach(o => v_optionsList.Add(new AccommodationOption()
//            {
//                FinalDestination = o.FinalDestination,
//                InitialDestination = o.InitialDestination,
//                InitialDate = o.InitialDate,
//                FinalDate = o.FinalDate,
//                Id = o.Id,
//                Price = o.Price,
//                CreatedAt = o.CreatedAt,
//                UpdatedAt = o.UpdatedAt,
//                IsEmpty = o.IsEmpty,
//                Accomodation = o.Accomodation,
//                TimeRange = o.TimeRange
//            }));
//
//            v_optionsList.RemoveAll(o => o.UpdatedAt - DateTime.Now >= TimeSpan.FromDays(999999));
//
//            if (v_optionsList.Count > 0
//                && v_optionsList.Max(o => o.TimeRange.LengthDays) == p_maxStay
//                && v_optionsList.Min(o => o.TimeRange.LengthDays) == p_minStay
//                && v_optionsList.Max(o => o.TimeRange.Final.Date) == p_dateRange.Final.Date
//                && v_optionsList.Min(o => o.TimeRange.Initial.Date) == p_dateRange.Initial.Date
//                )
//            {
//                this.AccommodationOptions.AddRange(v_optionsList);
//                return;
//            }
            
            //TODO: get info online
            //update accommodation list
//            List<Accommodation> v_accommodations = this.DbContext.Accommodations
//                .Where(a => a.Destination.Id == p_destination.Id)
//                .Include(a => a.PriceDateList.Select(pd => pd.Price))
//                .ToList();
//            if (v_accommodations.Count > 0)
//                RandomFetcher.UpdateAccommodations(v_accommodations, p_dateRange);
//            else
//                v_accommodations = RandomFetcher.FetchAccomodations(p_destination, p_dateRange, p_nAccomodations: this.nAccommodations);

            List<Accommodation> v_accommodations = new List<Accommodation>();
            v_accommodations = RandomFetcher.FetchAccomodations(p_destination, p_dateRange, p_nAccomodations: this.nAccommodations);

            //generate new options based on new list
            List<AccommodationOption> v_newOptions = new List<AccommodationOption>();
            v_accommodations.ForEach(a => v_newOptions.AddRange(AccommodationOption.BuildOptionListFromAccomodation(a, p_dateRange.Initial,
                p_dateRange.Final, p_minStay, p_maxStay)));

            //remove from new options duplicates from v_optionslist
            foreach (AccommodationOption i_accommodationOption in v_optionsList)
            {
                v_newOptions.Remove(v_newOptions.SingleOrDefault(o => o.IsDuplicate(i_accommodationOption)));
            }

            this.AccommodationOptions.AddRange(v_optionsList);
            this.AccommodationOptions.AddRange(v_newOptions);

            this.DbContext.AccommodationOptions.AddRange(v_newOptions);
            this.DbContext.SaveChanges();
        }


        private void GetTransportationOptions(Destination p_destinationStart, Destination p_nextDestination, DateTime p_date)
        {
//            //TODO: check if the pair destination-date was not already fetched -> dictionary to register or verify trip list?
//            List<TransportationOption> v_optionsTrip = this.TransportationOptions
//                .Where(o => o.InitialDestination.Id == p_destinationStart.Id)
//                .Where(o => o.FinalDestination.Id == p_nextDestination.Id)
//                .Where(o => o.InitialDate.Date == p_date.Date)
//                .ToList();
//
//            if (v_optionsTrip.Count > 0)
//                return;
//
//            //TODO: check if database has updated info on the tuple destinationStart-destinationEnd-date
//            var v_optionsDb = this.DbContext.TransportationOptions
//                .Join(
//                    this.DbContext.SolutionComponents,
//                    o => o.Id, sc => sc.Id,
//                    (o, sc) => new
//                    {
//                        Id = sc.Id,
//                        FinalDestination = sc.FinalDestination,
//                        InitialDestination = sc.InitialDestination,
//                        InitialDate = sc.InitialDate,
//                        FinalDate = sc.FinalDate,
//                        CostValue = sc.CostValue,
//                        TimeValue = sc.TimeValue,
//                        Price = o.Price,
//                        BranchesList = o.BranchesList,
//                        UpdatedAt = sc.UpdatedAt,
//                        CreatedAt = sc.CreatedAt,
//                        IsEmpty = sc.IsEmpty
//                    }
//                )
//                .Where(o => o.InitialDestination.Id == p_destinationStart.Id)
//                .Where(o => o.FinalDestination.Id == p_nextDestination.Id)
//                .Where(o => DbFunctions.TruncateTime(o.InitialDate) == p_date.Date)
//                .ToList();
//
//
//            List<TransportationOption> v_optionsList = new List<TransportationOption>();
//
//            v_optionsDb.ForEach(o => v_optionsList.Add(new TransportationOption()
//            {
//                FinalDestination = o.FinalDestination,
//                InitialDestination = o.InitialDestination,
//                InitialDate = o.InitialDate,
//                FinalDate = o.FinalDate,
//                Id = o.Id,
//                Price = o.Price,
//                BranchesList = o.BranchesList,
//                CreatedAt = o.CreatedAt,
//                UpdatedAt = o.UpdatedAt,
//                IsEmpty = o.IsEmpty
//            }));
//
//            if (v_optionsList.Count > 0)
//            {
//                bool v_upToDate = true;
//                foreach (var i_option in v_optionsList)
//                {
//                    if (i_option.UpdatedAt - DateTime.Now >= TimeSpan.FromDays(999999))
//                    {
//                        v_upToDate = false;
//                        break;
//                    }
//                }
//                if (v_upToDate)
//                {
//                    this.TransportationOptions.AddRange(v_optionsList);
//                    return;
//                }
//            }

            //TODO: get info online
            var v_optionsFetched = RandomFetcher.FetchTransportationOptions(p_destinationStart, p_nextDestination,
                p_date, p_nTransportations: this.nTransportations);
            this.DbContext.TransportationOptions.AddRange(v_optionsFetched);
            this.DbContext.SaveChanges();
            this.TransportationOptions.AddRange(v_optionsFetched);

        }

        private bool CheckTripConsistency()
        {
            bool v_tripOk = true;
            //sum(DestinationSpecifications.minStay) <= EndDate.final - StartDate.initial
            //sum(DestinationSpecifications.maxStay) >= EndDate.initial - StartDate.final

            //DestinationSpecifications.relativeOrderIds != DestinationSpecifications.id
            //no conflict between DestinationSpecifications.relativeOrderIds (i.e. 2 after 4, 4 after 2)
            //DestinationSpecifications[i].absoluteOrder != DestinationSpecifications[j].absOrder for all i!=j
            //DestinationSpecifications.absoluteOrder <= DestinationSpecifications.length

            //startingDestinationId != DestinationSpecifications.id

            //Restrictions.PriceMin < PriceMax
            //TranspRestrictions.TimeRestrictions sum < 24h

            //DestinationSpecifications.absOrder = last && DestinationSpecifications.DateRange.final > trip.FinalDate.final
            //DestinationSpecifications.absOrder = first(1) && DestinationSpecifications.DateRange.initial < trip.StartDate.min

            return v_tripOk;
        }

        internal static Trip GetRandomTrip(int p_destinations, int p_tranportationAvg, int p_accommodationAvg, STripDbContext p_dbContext)
        {
            HashSet<DestinationSpecification> v_destinationSpecifications = new HashSet<DestinationSpecification>();
            int v_id = 10000 + p_destinations * 100 + p_tranportationAvg * 10 + p_accommodationAvg;

            for (int i = 0; i < p_destinations; i++)
            {
                Destination i_destination = new Destination() {Name = i.ToString()};
                v_destinationSpecifications.Add(new DestinationSpecification()
                {
                    Id = v_id+1000*i,
                    Destination = i_destination,
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                });
            }
            
            Trip v_trip = new Trip()
            {
                Id = v_id,
                Name = "10000",
                DestinationSpecifications = v_destinationSpecifications,
                StartingDestination = new Destination() {Name = "start"},
                StartDateRange =
                    new DateTimeRange() {Initial = DateTime.Today.AddDays(v_id), Final = DateTime.Today.AddDays(v_id) },
                EndDateRange =
                    new DateTimeRange() {Initial = DateTime.Today.AddDays(v_id), Final = DateTime.Today.AddDays(v_id+1000) },
                RoundTrip = true
            };

            v_trip.nTransportations = p_tranportationAvg;
            v_trip.nAccommodations = p_accommodationAvg;
            v_trip.FetchOptions(p_dbContext);

//            decimal v_nadirSolutionCost =
//               v_trip.TransportationOptions.Select(o => o.CostValue)
//                   .ToList()
//                   .OrderByDescending(i => i)
//                   .Take(v_trip.DestinationSpecifications.Count + 1)
//                   .Sum() +
//               v_trip.AccommodationOptions.Select(o => o.CostValue)
//                   .ToList()
//                   .OrderByDescending(i => i)
//                   .Take(v_trip.DestinationSpecifications.Count)
//                   .Sum();
//
//
//            decimal v_nadirSolutionTime =
//                v_trip.TransportationOptions.Select(o => o.TimeValue)
//                    .ToList()
//                    .OrderByDescending(i => i)
//                    .Take(v_trip.DestinationSpecifications.Count + 1)
//                    .Sum() +
//                v_trip.AccommodationOptions.Select(o => o.TimeValue)
//                    .ToList()
//                    .OrderByDescending(i => i)
//                    .Take(v_trip.DestinationSpecifications.Count)
//                    .Sum();
//
//            v_trip.AccommodationOptions.ForEach(o => o.CostValue = o.CostValue / v_nadirSolutionCost);
//            v_trip.TransportationOptions.ForEach(o => o.CostValue = o.CostValue / v_nadirSolutionCost);
//            v_trip.TransportationOptions.ForEach(o => o.TimeValue = o.TimeValue / v_nadirSolutionTime);

            return v_trip;
        }
    }
}
