﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Conventions;
using STripPFC.Models.Util;
using System.Data.Entity;
using System.Linq;

namespace STripPFC.Models
{
    public class AccommodationOption : SolutionComponent
    {
        public Accommodation Accomodation { get; set; }
        public DateTimeRange TimeRange { get; set; }
        public Money Price { get; set; }

        public AccommodationOption()
        {
        }

        public AccommodationOption(Accommodation accommodation, DateTimeRange timeRange)
        {
            this.Accomodation = accommodation;
            this.TimeRange = timeRange;
            this.Price = new Money();
            foreach (PriceDate priceDate in this.Accomodation.PriceDateList)
            {
                if ((priceDate.Date.Date >= this.TimeRange.Initial.Date) && (priceDate.Date.Date < this.TimeRange.Final.Date))
                    this.Price.Amount += priceDate.Price.Amount;
            }
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
        }

        public override Destination FinalDestination => this.Accomodation.Destination;
        public override Destination InitialDestination => this.Accomodation.Destination;

        public override decimal CostValue => this.Price.Amount;
        public override decimal TimeValue => 0;

        public override DateTime FinalDate => this.TimeRange.Final;
        public override DateTime InitialDate => this.TimeRange.Initial;

        public static List<AccommodationOption> BuildOptionListFromAccomodation(Accommodation p_accommodation, DateTime p_dateMin, DateTime p_dateMax, int p_minStay, int p_maxStay)
        {
            List<AccommodationOption> v_accommodationOptionList = new List<AccommodationOption>();
            int v_startWindowDays = (p_dateMax - p_dateMin).Days - p_minStay;

            for (int i = 0; i <= v_startWindowDays; i++)
            {
                DateTime v_startingDate = p_dateMin.AddDays(i);
                for (int i_stayDuration = p_minStay; i_stayDuration <= p_maxStay; i_stayDuration++)
                {
                    DateTimeRange v_dateTimeRange = new DateTimeRange()
                    {
                        Initial = v_startingDate,
                        Final = v_startingDate.AddDays(i_stayDuration)
                    };
                    v_accommodationOptionList.Add(new AccommodationOption(p_accommodation, v_dateTimeRange));
                }
            }
            
            return v_accommodationOptionList;
        }

        public bool IsDuplicate(AccommodationOption p_accommodationOption)
        {
            return (this.TimeRange.Initial.Date == p_accommodationOption.TimeRange.Initial.Date
                    && this.TimeRange.Final.Date == p_accommodationOption.TimeRange.Final.Date
                    && this.Accomodation.Id == p_accommodationOption.Accomodation.Id
                    && this.InitialDestination == p_accommodationOption.InitialDestination
                    && this.CostValue == p_accommodationOption.CostValue
                );
        }
    }
}
