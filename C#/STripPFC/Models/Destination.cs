﻿using System.Collections.Generic;

namespace STripPFC.Models
{
    public class Destination
    { 
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MainPhotoUrl { get; set; }
    }
}
