﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlTypes;
using System.Linq;
using Microsoft.Ajax.Utilities;
using STripPFC.Models.Util;

namespace STripPFC.Models
{
    public class STripDbContext: DbContext
    {
        public STripDbContext(): base("DefaultConnection")
        {
            Database.SetInitializer<STripDbContext>(new DropCreateDatabaseIfModelChanges<STripDbContext>());
        }

        public static STripDbContext Create()
        {
            return new STripDbContext();
        }

        public void Populate()
        {
            this.Destinations.AddOrUpdate(
                d => d.Name, 
                new Destination() {Name = "London"},
                new Destination() {Name = "Paris" },
                new Destination() {Name = "Madrid" },
                new Destination() {Name = "Rome" },
                new Destination() {Name = "Prague" },
                new Destination() {Name = "Lisbon" },
                new Destination() {Name = "Amsterdam" },
                new Destination() {Name = "Brussels" },
                new Destination() {Name = "Warsaw" },
                new Destination() {Name = "Budapest" },
                new Destination() {Name = "Edinburgh" }
            );
            this.SaveChanges();


            this.DestinationSpecifications.AddOrUpdate(
                s => s.Id,
                new DestinationSpecification()
                {
                    Id = 1,
                    Destination = this.Destinations.First(d => d.Name == "London"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 2,
                    Destination = this.Destinations.First(d => d.Name == "Paris"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 3,
                    Destination = this.Destinations.First(d => d.Name == "Madrid"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 4,
                    Destination = this.Destinations.First(d => d.Name == "Rome"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 5,
                    Destination = this.Destinations.First(d => d.Name == "Prague"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 6,
                    Destination = this.Destinations.First(d => d.Name == "Lisbon"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 7,
                    Destination = this.Destinations.First(d => d.Name == "Amsterdam"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 8,
                    Destination = this.Destinations.First(d => d.Name == "Brussels"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 9,
                    Destination = this.Destinations.First(d => d.Name == "Warsaw"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 10,
                    Destination = this.Destinations.First(d => d.Name == "Budapest"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                },
                new DestinationSpecification()
                {
                    Id = 11,
                    Destination = this.Destinations.First(d => d.Name == "Edinburgh"),
                    MaxStayDays = 3,
                    MinStayDays = 3,
                    AccommodationRequired = true
                }
            );
            this.SaveChanges();

            this.Trips.AddOrUpdate(
                t => t.Id,
                new Trip()
                {
                    Id = 1,
                    Name = "Tiny Trip (d=2), no restrictions",
                    DestinationSpecifications = new HashSet<DestinationSpecification>()
                    {
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Paris")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Rome"))
                    },
                    StartingDestination = this.Destinations.First(d => d.Name == "London"),
                    StartDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-20), Final = DateTime.Today.AddDays(-18)},
                    EndDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-14), Final = DateTime.Today.AddDays(-12)},
                    RoundTrip = true
                },
                new Trip()
                {
                    Id = 2,
                    Name = "Small Trip (d=3), no restrictions",
                    DestinationSpecifications = new HashSet<DestinationSpecification>()
                    {
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Paris")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Madrid")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Rome"))
                    },
                    StartingDestination = this.Destinations.First(d => d.Name == "London"),
                    StartDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-23), Final = DateTime.Today.AddDays(-21) },
                    EndDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-14), Final = DateTime.Today.AddDays(-12) },
                    RoundTrip = true
                },
                new Trip()
                {
                    Id = 3,
                    Name = "Medium-small Trip (d=4), no restrictions",
                    DestinationSpecifications = new HashSet<DestinationSpecification>()
                    {
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Paris")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Madrid")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Lisbon")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Rome"))
                    },
                    StartingDestination = this.Destinations.First(d => d.Name == "London"),
                    StartDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-26), Final = DateTime.Today.AddDays(-24) },
                    EndDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-14), Final = DateTime.Today.AddDays(-12) },
                    RoundTrip = true
                },
                new Trip()
                {
                    Id = 4,
                    Name = "Medium Trip (d=5), no restrictions",
                    DestinationSpecifications = new HashSet<DestinationSpecification>()
                    {
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Paris")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Madrid")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Lisbon")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Prague")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Rome"))
                    },
                    StartingDestination = this.Destinations.First(d => d.Name == "London"),
                    StartDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-29), Final = DateTime.Today.AddDays(-27) },
                    EndDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-14), Final = DateTime.Today.AddDays(-12) },
                    RoundTrip = true
                },
                new Trip()
                {
                    Id = 5,
                    Name = "Medium-Big Trip (d=6), no restrictions",
                    DestinationSpecifications = new HashSet<DestinationSpecification>()
                    {
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Paris")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Madrid")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Lisbon")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Prague")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Warsaw")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Rome"))
                    },
                    StartingDestination = this.Destinations.First(d => d.Name == "London"),
                    StartDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-32), Final = DateTime.Today.AddDays(-30) },
                    EndDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-14), Final = DateTime.Today.AddDays(-12) },
                    RoundTrip = true
                },
                new Trip()
                {
                    Id = 6,
                    Name = "Big Trip (d=7), no restrictions",
                    DestinationSpecifications = new HashSet<DestinationSpecification>()
                    {
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Paris")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Madrid")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Lisbon")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Prague")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Warsaw")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Budapest")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Rome"))
                    },
                    StartingDestination = this.Destinations.First(d => d.Name == "London"),
                    StartDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-35), Final = DateTime.Today.AddDays(-33) },
                    EndDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-14), Final = DateTime.Today.AddDays(-12) },
                    RoundTrip = true
                },
                new Trip()
                {
                    Id = 7,
                    Name = "Very Big Trip (d=8), no restrictions",
                    DestinationSpecifications = new HashSet<DestinationSpecification>()
                    {
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Paris")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Madrid")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Lisbon")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Prague")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Warsaw")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Brussels")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Budapest")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Rome"))
                    },
                    StartingDestination = this.Destinations.First(d => d.Name == "London"),
                    StartDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-38), Final = DateTime.Today.AddDays(-36) },
                    EndDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-14), Final = DateTime.Today.AddDays(-12) },
                    RoundTrip = true
                },
                new Trip()
                {
                    Id = 8,
                    Name = "Huge Trip (d=10), no restrictions",
                    DestinationSpecifications = new HashSet<DestinationSpecification>()
                    {
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Paris")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Madrid")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Lisbon")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Prague")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Warsaw")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Brussels")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Budapest")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Amsterdam")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Edinburgh")),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Rome"))
                    },
                    StartingDestination = this.Destinations.First(d => d.Name == "London"),
                    StartDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-45), Final = DateTime.Today.AddDays(-41) },
                    EndDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-14), Final = DateTime.Today.AddDays(-10) },
                    RoundTrip = true
                },
                new Trip()
                {
                    Id = 9,
                    Name = "Very Big Trip (d=8), ordered",
                    DestinationSpecifications = new HashSet<DestinationSpecification>()
                    {
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Paris"), 1),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Madrid"),2),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Lisbon"), 3),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Prague"), 4),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Warsaw"), 5),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Brussels"), 6),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Budapest"), 7),
                        new DestinationSpecification(this.DestinationSpecifications.First(specification => specification.Destination.Name == "Rome"), 8)
                    },
                    StartingDestination = this.Destinations.First(d => d.Name == "London"),
                    StartDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-38), Final = DateTime.Today.AddDays(-36) },
                    EndDateRange = new DateTimeRange() { Initial = DateTime.Today.AddDays(-14), Final = DateTime.Today.AddDays(-12) },
                    RoundTrip = true
                }
            );

            this.SaveChanges();
        }


        public DbSet<Accommodation> Accommodations { get; set; }
        public DbSet<AccommodationOption> AccommodationOptions { get; set; }
        public DbSet<AccommodationRestriction> AccommodationRestrictions { get; set; }
        public DbSet<Destination> Destinations { get; set; }
        public DbSet<DestinationSpecification> DestinationSpecifications { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<TransportationBranch> TransportationBranches { get; set; }
        public DbSet<TransportationOption> TransportationOptions { get; set; }
        public DbSet<TransportationRestriction> TransportationRestrictions { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<TripSolution> TripSolutions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<SolutionComponent> SolutionComponents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccommodationOption>().ToTable("AccommodationOptions");
            modelBuilder.Entity<TransportationOption>().ToTable("TransportationOptions");

            modelBuilder.Entity<AccommodationRestriction>()
                .HasMany<Location>(l => l.LocationRestrictions)
                .WithOptional(l => l.AccommodationRestriction);

            modelBuilder.Entity<TransportationOption>()
                .HasMany<TransportationBranch>(o => o.BranchesList)
                .WithMany(b => b.TransportationOptionsCollection)
                .Map(cs =>
                {
                    cs.MapLeftKey("TransportationOptionId");
                    cs.MapRightKey("TransportationBranchId");
                    cs.ToTable("TransportationOptionBranch");
                });

//            modelBuilder.Entity<TripSolution>()
//                .HasMany<int>(o => o.SolutionComponentsIds);
//                .WithMany(c => c.TripSolutionsIds)
//                .Map(cs =>
//                {
//                    cs.MapLeftKey("TripSolutionId");
//                    cs.MapRightKey("SolutionComponentId");
//                    cs.ToTable("TripSolutionSolutionComponent");
//                });

        }
    }
}