﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using STripPFC.Models.Util;

namespace STripPFC.Models
{
    public class AccommodationRestriction
    {
        public int Id { get; set; }
        public HashSet<Location> LocationRestrictions { get; set; }//n-to-1
        public string CompanyRestrictions { get; set; }
        public Money PriceMin { get; set; }
        public Money PriceMax { get; set; }

        public float HotelStars { get; set; }
    }
}
