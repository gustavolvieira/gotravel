﻿using System;
using System.Collections.Generic;
using System.Linq;
using STripPFC.Models.Util;
using WebGrease.Css.Extensions;

namespace STripPFC.Models.WebFetchers
{
    //Random fetcher generates random data for tests
    public static class RandomFetcher
    {
//        public const int c_numberTransportations = 8;
//        private const int c_numberAccomodations = 8;
        private static Random c_randomizer = new Random();

        public static List<Accommodation> FetchAccomodations(Destination p_destination, DateTimeRange p_dateRange, int p_nAccomodations = 8)
        {
            List<Accommodation> v_accommodationList = new List<Accommodation>();
            for (int i = 1; i <= p_nAccomodations; i++)
            {
                v_accommodationList.Add(GetRandomAccomodation(p_destination, p_dateRange.Initial, p_dateRange.Final));
            }
            return v_accommodationList;
        }

        public static void UpdateAccommodations(List<Accommodation> p_accommodationList, DateTimeRange p_dateRange)
        {
            foreach (Accommodation i_accommodation in p_accommodationList)
            {
                for (int i = 0; i <= p_dateRange.LengthDays; i++)
                {
                    DateTime p_currentDate = p_dateRange.Initial.AddDays(i);
                    if (i_accommodation.PriceDateList.SingleOrDefault(pd => pd.Date == p_currentDate) == null)
                    {
                        i_accommodation.PriceDateList.Add(new PriceDate()
                        {
                            Date = p_currentDate,
                            UpdatedAt = DateTime.Now,
                            Price = new Money() { Amount = c_randomizer.Next(50, 2000) }
                        });
                    }
                }
            }
        }

        public static List<TransportationOption> FetchTransportationOptions(Destination p_destinationStart, Destination p_destinationEnd, DateTime p_date, int p_nTransportations = 8)
        {
            List <TransportationOption> v_transportationOptions = new List<TransportationOption>();
            for (int i = 1; i <= p_nTransportations; i++)
            {
                v_transportationOptions.Add(GetRandomTransportationOption(p_destinationStart, p_destinationEnd, p_date));
            }

            return v_transportationOptions;
        }

        private static TransportationOption GetRandomTransportationOption(Destination p_destinationStart, Destination p_destinationEnd, DateTime p_date)
        {
            return new TransportationOption()
            {
                Price = new Money()
                {
                    Amount = c_randomizer.Next(300, 3000)
                },
                BranchesList = new List<TransportationBranch>()
                {
                    new TransportationBranch()
                    {
                        DepartingDateTime = p_date.AddMinutes(c_randomizer.Next(0, 2*60)),
                        ArrivingDateTime = p_date.AddMinutes(c_randomizer.Next(3*60, 10*60)),
                        StartingDestination = p_destinationStart,
                        FinalDestination = p_destinationEnd,
                        Type = TransportationType.Plane,
                        ArrivingStation = new Station() {StationName = "ArrivingStation"},
                        DepartingStation = new Station() {StationName = "Departingtation"},
                        Company = "Company Name",
                        Code = c_randomizer.Next()
                    }
                },
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        private static Accommodation GetRandomAccomodation(Destination p_destination, DateTime p_dateMin,
            DateTime p_dateMax)
        {
            List<PriceDate> v_priceDateList = new List<PriceDate>();
            DateTime v_currentDate = p_dateMin;
            while (!v_currentDate.Equals(p_dateMax))
            {
                v_priceDateList.Add(new PriceDate()
                {
                    Date = v_currentDate,
                    Price = new Money() { Amount = c_randomizer.Next(50, 2000) },
                    UpdatedAt = DateTime.Now
                });

                v_currentDate = v_currentDate.AddDays(1);
            }

            return new Accommodation()
            {
                CheckInTime = new DateTime(year: 2000, month: 1, day: 1, hour: 12, minute: 12, second: 0),
                Company = "Company Name",
                Destination = p_destination,
                Location = null,
                Name = "Hotel Name",
                ReputationValue = 0,
                Type = AccommodationType.Hotel,
                PriceDateList = v_priceDateList
            };
        }


    }
}