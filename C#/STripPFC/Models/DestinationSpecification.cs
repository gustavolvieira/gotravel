﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using STripPFC.Models.Util;

namespace STripPFC.Models
{
    public class DestinationSpecification
    {
        public int Id { get; set; }
        public Destination Destination { get; set; }//1-to-n
        public DateTimeRange DateInterval { get; set; }

        public int MinStayDays { get; set; }
        public int MaxStayDays { get; set; }

        public int? AbsoluteOrder { get; set; }

        [NotMapped]
        public HashSet<int> RelativeOrderIds { get; set; }//Ids of destinations that must be visited BEFORE visiting this destination
        public string RelativeOrdersIdsString { get; set; }

        public bool AccommodationRequired { get; set; }


        public DestinationSpecification()
        {
            if (this.RelativeOrdersIdsString != null)
            {
                this.RelativeOrderIds = new HashSet<int>();
                foreach (string c_idString in this.RelativeOrdersIdsString.Split(';'))
                {
                    this.RelativeOrderIds.Add(int.Parse(c_idString));
                }
            }
        }

        public DestinationSpecification(DestinationSpecification p_cloneDestination, int? p_absoluteOrder = null)
        {
            this.Destination = p_cloneDestination.Destination;
            this.AbsoluteOrder = p_cloneDestination.AbsoluteOrder;
            this.AccommodationRequired = p_cloneDestination.AccommodationRequired;
            this.DateInterval = p_cloneDestination.DateInterval;
            this.MaxStayDays = p_cloneDestination.MaxStayDays;
            this.MinStayDays = p_cloneDestination.MinStayDays;
            this.RelativeOrderIds = p_cloneDestination.RelativeOrderIds;
            this.RelativeOrdersIdsString = p_cloneDestination.RelativeOrdersIdsString;

            if (p_absoluteOrder != null)
            {
                this.AbsoluteOrder = p_absoluteOrder.Value;
            }
        }
    }
}
