﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace STripPFC.Models
{
    public abstract partial class SolutionComponent
    {
        public int Id { get; set; }
        public bool IsEmpty { get; set; }

        public virtual Destination FinalDestination { get; set; }//1-to-n
        public virtual Destination InitialDestination { get; set; }//1-to-n

        public virtual decimal CostValue { get; set;  }
        public virtual decimal TimeValue { get; set;  }

        public virtual DateTime FinalDate { get; set; }
        public virtual DateTime InitialDate { get; set;  }

//        public virtual ICollection<TripSolution> TripSolutions { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }

        [NotMapped] public decimal CostValueNormalized { get; set; }
        [NotMapped] public decimal TimeValueNormalized { get; set; }

        public decimal GetQualityValue(Objectives p_objective)
        {
            switch (p_objective)
            {
                case (Objectives.Cost):
                    return Math.Abs(this.CostValueNormalized - 1);

                case (Objectives.Time):
                    return Math.Abs(this.TimeValueNormalized - 1);

                default:
                    return 0;
            }
        }
    }
}
