﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using STripPFC.Models.Util;

namespace STripPFC.Models
{
    public class TransportationRestriction
    {
        public int Id { get; set; }

        [NotMapped]
        public HashSet<TransportationType> TypeRestrictions { get; set; }
        public string TypeRestrictionsString { get; set; } //db trick

        public HashSet<DateTimeRange> TimeRestrictions { get; set; }
        public string CompanyRestrictionsString { get; set; }

        public Money PriceMin { get; set; }
        public Money PriceMax { get; set; }

        public TransportationRestriction()
        {
            this.TypeRestrictions = new HashSet<TransportationType>();
            foreach (string c_idString in this.TypeRestrictionsString.Split(';'))
            {
                this.TypeRestrictions.Add((TransportationType)(int.Parse(c_idString)));
            }

        }
    }
}
