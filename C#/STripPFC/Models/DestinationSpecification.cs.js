﻿var server = server || {};
/// <summary>The DestinationSpecification class as defined in STripPFC.Models.DestinationSpecification</summary>
server.DestinationSpecification = function() {
	/// <field name="id" type="Number">The Id property as defined in STripPFC.Models.DestinationSpecification</field>
	this.id = 0;
	/// <field name="destination" type="Object">The Destination property as defined in STripPFC.Models.DestinationSpecification</field>
	this.destination = { };
	/// <field name="dateInterval" type="Object">The DateInterval property as defined in STripPFC.Models.DestinationSpecification</field>
	this.dateInterval = { };
	/// <field name="minStayDays" type="Number">The MinStayDays property as defined in STripPFC.Models.DestinationSpecification</field>
	this.minStayDays = 0;
	/// <field name="maxStayDays" type="Number">The MaxStayDays property as defined in STripPFC.Models.DestinationSpecification</field>
	this.maxStayDays = 0;
	/// <field name="absoluteOrder" type="Number">The AbsoluteOrder property as defined in STripPFC.Models.DestinationSpecification</field>
	this.absoluteOrder = 0;
	/// <field name="relativeOrderIds" type="Number[]">The RelativeOrderIds property as defined in STripPFC.Models.DestinationSpecification</field>
	this.relativeOrderIds = [];
	/// <field name="relativeOrdersIdsString" type="String">The RelativeOrdersIdsString property as defined in STripPFC.Models.DestinationSpecification</field>
	this.relativeOrdersIdsString = '';
	/// <field name="accommodationRequired" type="Boolean">The AccommodationRequired property as defined in STripPFC.Models.DestinationSpecification</field>
	this.accommodationRequired = false;
};

