using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using STrip.Models;

namespace STrip.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20160413141737_migrationTest4")]
    partial class migrationTest4
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasAnnotation("Relational:Name", "RoleNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasAnnotation("Relational:TableName", "AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasAnnotation("Relational:TableName", "AspNetUserRoles");
                });

            modelBuilder.Entity("STrip.Models.Accommodation", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CheckInTime");

                    b.Property<string>("Company");

                    b.Property<int?>("DestinationId");

                    b.Property<int?>("LocationId");

                    b.Property<string>("Name");

                    b.Property<float>("ReputationValue");

                    b.Property<int>("Type");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("STrip.Models.AccommodationRestrictions", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CompanyRestrictions");

                    b.Property<float>("HotelStars");

                    b.Property<int?>("PriceMaxId");

                    b.Property<int?>("PriceMinId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("STrip.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasAnnotation("Relational:Name", "EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .HasAnnotation("Relational:Name", "UserNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("STrip.Models.Destination", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("MainPhotoUrl");

                    b.Property<string>("Name");

                    b.Property<int?>("SolutionComponentId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:DiscriminatorProperty", "Discriminator");

                    b.HasAnnotation("Relational:DiscriminatorValue", "Destination");
                });

            modelBuilder.Entity("STrip.Models.SolutionComponent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("CostValue");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<DateTime>("FinalDate");

                    b.Property<int?>("FinalDestinationId");

                    b.Property<DateTime>("InitialDate");

                    b.Property<int?>("InitialDestinationId");

                    b.Property<bool>("IsEmpty");

                    b.Property<decimal>("TimeValue");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:DiscriminatorProperty", "Discriminator");

                    b.HasAnnotation("Relational:DiscriminatorValue", "SolutionComponent");
                });

            modelBuilder.Entity("STrip.Models.Util.DateTimeRange", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Final");

                    b.Property<DateTime>("Initial");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("STrip.Models.Util.Location", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AccommodationRestrictionsId");

                    b.Property<string>("City");

                    b.Property<string>("Country");

                    b.Property<float?>("Latitude");

                    b.Property<float?>("Longitude");

                    b.Property<float?>("MaxRadiusKm");

                    b.Property<string>("StateOrProvince");

                    b.Property<string>("Street");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("STrip.Models.Util.Money", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Amount");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("STrip.Models.Util.PriceDate", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AccommodationId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("STrip.Models.DestinationSpecification", b =>
                {
                    b.HasBaseType("STrip.Models.Destination");

                    b.Property<int>("AbsoluteOrder");

                    b.Property<bool>("AccommodationRequired");

                    b.Property<int?>("DateIntervalId");

                    b.Property<string>("RelativeOrdersIdsString");

                    b.HasAnnotation("Relational:DiscriminatorValue", "DestinationSpecification");
                });

            modelBuilder.Entity("STrip.Models.AccommodationOption", b =>
                {
                    b.HasBaseType("STrip.Models.SolutionComponent");

                    b.Property<int?>("AccomodationId");

                    b.Property<int?>("PriceId");

                    b.Property<int?>("TimeRangeId");

                    b.HasAnnotation("Relational:DiscriminatorValue", "AccommodationOption");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("STrip.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("STrip.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("STrip.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("STrip.Models.Accommodation", b =>
                {
                    b.HasOne("STrip.Models.Destination")
                        .WithMany()
                        .HasForeignKey("DestinationId");

                    b.HasOne("STrip.Models.Util.Location")
                        .WithMany()
                        .HasForeignKey("LocationId");
                });

            modelBuilder.Entity("STrip.Models.AccommodationRestrictions", b =>
                {
                    b.HasOne("STrip.Models.Util.Money")
                        .WithMany()
                        .HasForeignKey("PriceMaxId");

                    b.HasOne("STrip.Models.Util.Money")
                        .WithMany()
                        .HasForeignKey("PriceMinId");
                });

            modelBuilder.Entity("STrip.Models.Destination", b =>
                {
                    b.HasOne("STrip.Models.SolutionComponent")
                        .WithMany()
                        .HasForeignKey("SolutionComponentId");
                });

            modelBuilder.Entity("STrip.Models.SolutionComponent", b =>
                {
                    b.HasOne("STrip.Models.Destination")
                        .WithMany()
                        .HasForeignKey("FinalDestinationId");

                    b.HasOne("STrip.Models.Destination")
                        .WithMany()
                        .HasForeignKey("InitialDestinationId");
                });

            modelBuilder.Entity("STrip.Models.Util.Location", b =>
                {
                    b.HasOne("STrip.Models.AccommodationRestrictions")
                        .WithMany()
                        .HasForeignKey("AccommodationRestrictionsId");
                });

            modelBuilder.Entity("STrip.Models.Util.PriceDate", b =>
                {
                    b.HasOne("STrip.Models.Accommodation")
                        .WithMany()
                        .HasForeignKey("AccommodationId");
                });

            modelBuilder.Entity("STrip.Models.DestinationSpecification", b =>
                {
                    b.HasOne("STrip.Models.Util.DateTimeRange")
                        .WithMany()
                        .HasForeignKey("DateIntervalId");
                });

            modelBuilder.Entity("STrip.Models.AccommodationOption", b =>
                {
                    b.HasOne("STrip.Models.Accommodation")
                        .WithMany()
                        .HasForeignKey("AccomodationId");

                    b.HasOne("STrip.Models.Util.Money")
                        .WithMany()
                        .HasForeignKey("PriceId");

                    b.HasOne("STrip.Models.Util.DateTimeRange")
                        .WithMany()
                        .HasForeignKey("TimeRangeId");
                });
        }
    }
}
