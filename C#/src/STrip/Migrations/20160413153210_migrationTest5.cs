using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace STrip.Migrations
{
    public partial class migrationTest5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_AccommodationOption_Money_PriceId", table: "SolutionComponent");
            migrationBuilder.CreateTable(
                name: "Station",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(nullable: true),
                    StationLocationId = table.Column<int>(nullable: true),
                    StationName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Station", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Station_Location_StationLocationId",
                        column: x => x.StationLocationId,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "TransportationRestrictions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyRestrictions = table.Column<string>(nullable: true),
                    PriceMaxId = table.Column<int>(nullable: true),
                    PriceMinId = table.Column<int>(nullable: true),
                    TypeRestrictionsString = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransportationRestrictions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransportationRestrictions_Money_PriceMaxId",
                        column: x => x.PriceMaxId,
                        principalTable: "Money",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransportationRestrictions_Money_PriceMinId",
                        column: x => x.PriceMinId,
                        principalTable: "Money",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "TransportationBranch",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ArrivingDateTime = table.Column<DateTime>(nullable: false),
                    ArrivingStationId = table.Column<int>(nullable: true),
                    Code = table.Column<int>(nullable: false),
                    Company = table.Column<string>(nullable: true),
                    DepartingDateTime = table.Column<DateTime>(nullable: false),
                    DepartingStationId = table.Column<int>(nullable: true),
                    DestinationId = table.Column<int>(nullable: false),
                    StartingDestinationId = table.Column<int>(nullable: true),
                    TransportationOptionId = table.Column<int>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransportationBranch", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransportationBranch_Station_ArrivingStationId",
                        column: x => x.ArrivingStationId,
                        principalTable: "Station",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransportationBranch_Station_DepartingStationId",
                        column: x => x.DepartingStationId,
                        principalTable: "Station",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransportationBranch_Destination_DestinationId",
                        column: x => x.DestinationId,
                        principalTable: "Destination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TransportationBranch_Destination_StartingDestinationId",
                        column: x => x.StartingDestinationId,
                        principalTable: "Destination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransportationBranch_TransportationOption_TransportationOptionId",
                        column: x => x.TransportationOptionId,
                        principalTable: "SolutionComponent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "Trip",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccommodationRestrictionsId = table.Column<int>(nullable: true),
                    EndDateRangeId = table.Column<int>(nullable: true),
                    RoundTrip = table.Column<bool>(nullable: false),
                    StartDateRangeId = table.Column<int>(nullable: true),
                    StartingDestinationId = table.Column<int>(nullable: true),
                    TransportationRestrictionsId = table.Column<int>(nullable: true),
                    User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trip", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trip_AccommodationRestrictions_AccommodationRestrictionsId",
                        column: x => x.AccommodationRestrictionsId,
                        principalTable: "AccommodationRestrictions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Trip_DateTimeRange_EndDateRangeId",
                        column: x => x.EndDateRangeId,
                        principalTable: "DateTimeRange",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Trip_DateTimeRange_StartDateRangeId",
                        column: x => x.StartDateRangeId,
                        principalTable: "DateTimeRange",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Trip_DestinationSpecification_StartingDestinationId",
                        column: x => x.StartingDestinationId,
                        principalTable: "Destination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Trip_TransportationRestrictions_TransportationRestrictionsId",
                        column: x => x.TransportationRestrictionsId,
                        principalTable: "TransportationRestrictions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.CreateTable(
                name: "TripSolution",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CrowdingDistance = table.Column<float>(nullable: false),
                    DominanceRank = table.Column<int>(nullable: false),
                    currentDate = table.Column<DateTime>(nullable: false),
                    currentDestinationId = table.Column<int>(nullable: true),
                    tripId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripSolution", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TripSolution_DestinationSpecification_currentDestinationId",
                        column: x => x.currentDestinationId,
                        principalTable: "Destination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TripSolution_Trip_tripId",
                        column: x => x.tripId,
                        principalTable: "Trip",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AddColumn<int>(
                name: "TransportationRestrictionsId",
                table: "DateTimeRange",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "TripSolutionId",
                table: "SolutionComponent",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "TripSolutionId1",
                table: "SolutionComponent",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "TripSolutionId2",
                table: "SolutionComponent",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "PriceId",
                table: "SolutionComponent",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "TripId",
                table: "SolutionComponent",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "TripId",
                table: "SolutionComponent",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "TripId",
                table: "Destination",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_AccommodationOption_Money_PriceId",
                table: "SolutionComponent",
                column: "PriceId",
                principalTable: "Money",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_AccommodationOption_Trip_TripId",
                table: "SolutionComponent",
                column: "TripId",
                principalTable: "Trip",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_DestinationSpecification_Trip_TripId",
                table: "Destination",
                column: "TripId",
                principalTable: "Trip",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SolutionComponent_TripSolution_TripSolutionId",
                table: "SolutionComponent",
                column: "TripSolutionId",
                principalTable: "TripSolution",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SolutionComponent_TripSolution_TripSolutionId1",
                table: "SolutionComponent",
                column: "TripSolutionId1",
                principalTable: "TripSolution",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SolutionComponent_TripSolution_TripSolutionId2",
                table: "SolutionComponent",
                column: "TripSolutionId2",
                principalTable: "TripSolution",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_TransportationOption_Money_PriceId",
                table: "SolutionComponent",
                column: "PriceId",
                principalTable: "Money",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_TransportationOption_Trip_TripId",
                table: "SolutionComponent",
                column: "TripId",
                principalTable: "Trip",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_DateTimeRange_TransportationRestrictions_TransportationRestrictionsId",
                table: "DateTimeRange",
                column: "TransportationRestrictionsId",
                principalTable: "TransportationRestrictions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_AccommodationOption_Money_PriceId", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_AccommodationOption_Trip_TripId", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_DestinationSpecification_Trip_TripId", table: "Destination");
            migrationBuilder.DropForeignKey(name: "FK_SolutionComponent_TripSolution_TripSolutionId", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_SolutionComponent_TripSolution_TripSolutionId1", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_SolutionComponent_TripSolution_TripSolutionId2", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_TransportationOption_Money_PriceId", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_TransportationOption_Trip_TripId", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_DateTimeRange_TransportationRestrictions_TransportationRestrictionsId", table: "DateTimeRange");
            migrationBuilder.DropColumn(name: "TransportationRestrictionsId", table: "DateTimeRange");
            migrationBuilder.DropColumn(name: "TripSolutionId", table: "SolutionComponent");
            migrationBuilder.DropColumn(name: "TripSolutionId1", table: "SolutionComponent");
            migrationBuilder.DropColumn(name: "TripSolutionId2", table: "SolutionComponent");
            migrationBuilder.DropColumn(name: "PriceId", table: "SolutionComponent");
            migrationBuilder.DropColumn(name: "TripId", table: "SolutionComponent");
            migrationBuilder.DropColumn(name: "TripId", table: "SolutionComponent");
            migrationBuilder.DropColumn(name: "TripId", table: "Destination");
            migrationBuilder.DropTable("TransportationBranch");
            migrationBuilder.DropTable("TripSolution");
            migrationBuilder.DropTable("User");
            migrationBuilder.DropTable("Station");
            migrationBuilder.DropTable("Trip");
            migrationBuilder.DropTable("TransportationRestrictions");
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_AccommodationOption_Money_PriceId",
                table: "SolutionComponent",
                column: "PriceId",
                principalTable: "Money",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
