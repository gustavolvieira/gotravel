using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace STrip.Migrations
{
    public partial class migrationTest3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropColumn(name: "Address", table: "Location");
            migrationBuilder.CreateTable(
                name: "DateTimeRange",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Final = table.Column<DateTime>(nullable: false),
                    Initial = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DateTimeRange", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "Money",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Money", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "AccommodationRestrictions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyRestrictions = table.Column<string>(nullable: true),
                    HotelStars = table.Column<float>(nullable: false),
                    PriceMaxId = table.Column<int>(nullable: true),
                    PriceMinId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccommodationRestrictions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccommodationRestrictions_Money_PriceMaxId",
                        column: x => x.PriceMaxId,
                        principalTable: "Money",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccommodationRestrictions_Money_PriceMinId",
                        column: x => x.PriceMinId,
                        principalTable: "Money",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AlterColumn<float>(
                name: "MaxRadiusKm",
                table: "Location",
                nullable: true);
            migrationBuilder.AlterColumn<float>(
                name: "Longitude",
                table: "Location",
                nullable: true);
            migrationBuilder.AlterColumn<float>(
                name: "Latitude",
                table: "Location",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "AccommodationRestrictionsId",
                table: "Location",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Location",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Location",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "StateOrProvince",
                table: "Location",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "Street",
                table: "Location",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "SolutionComponent",
                nullable: false,
                defaultValue: "");
            migrationBuilder.AddColumn<int>(
                name: "AccomodationId",
                table: "SolutionComponent",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "PriceId",
                table: "SolutionComponent",
                nullable: true);
            migrationBuilder.AddColumn<int>(
                name: "TimeRangeId",
                table: "SolutionComponent",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_AccommodationOption_Accommodation_AccomodationId",
                table: "SolutionComponent",
                column: "AccomodationId",
                principalTable: "Accommodation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_AccommodationOption_Money_PriceId",
                table: "SolutionComponent",
                column: "PriceId",
                principalTable: "Money",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_AccommodationOption_DateTimeRange_TimeRangeId",
                table: "SolutionComponent",
                column: "TimeRangeId",
                principalTable: "DateTimeRange",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Location_AccommodationRestrictions_AccommodationRestrictionsId",
                table: "Location",
                column: "AccommodationRestrictionsId",
                principalTable: "AccommodationRestrictions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_AccommodationOption_Accommodation_AccomodationId", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_AccommodationOption_Money_PriceId", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_AccommodationOption_DateTimeRange_TimeRangeId", table: "SolutionComponent");
            migrationBuilder.DropForeignKey(name: "FK_Location_AccommodationRestrictions_AccommodationRestrictionsId", table: "Location");
            migrationBuilder.DropColumn(name: "AccommodationRestrictionsId", table: "Location");
            migrationBuilder.DropColumn(name: "City", table: "Location");
            migrationBuilder.DropColumn(name: "Country", table: "Location");
            migrationBuilder.DropColumn(name: "StateOrProvince", table: "Location");
            migrationBuilder.DropColumn(name: "Street", table: "Location");
            migrationBuilder.DropColumn(name: "Discriminator", table: "SolutionComponent");
            migrationBuilder.DropColumn(name: "AccomodationId", table: "SolutionComponent");
            migrationBuilder.DropColumn(name: "PriceId", table: "SolutionComponent");
            migrationBuilder.DropColumn(name: "TimeRangeId", table: "SolutionComponent");
            migrationBuilder.DropTable("AccommodationRestrictions");
            migrationBuilder.DropTable("DateTimeRange");
            migrationBuilder.DropTable("Money");
            migrationBuilder.AlterColumn<float>(
                name: "MaxRadiusKm",
                table: "Location",
                nullable: false);
            migrationBuilder.AlterColumn<float>(
                name: "Longitude",
                table: "Location",
                nullable: false);
            migrationBuilder.AlterColumn<float>(
                name: "Latitude",
                table: "Location",
                nullable: false);
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Location",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
