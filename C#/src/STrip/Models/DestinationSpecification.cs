﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using STrip.Models.Util;

namespace STrip.Models
{
    public class DestinationSpecification : Destination
    {
        public new int Id { get; set; }
        public DateTimeRange DateInterval { get; set; }
        public struct StayDuration {
            public int MinStay;
            public int MaxStay;
        }
        public int AbsoluteOrder { get; set; }

        [NotMapped]
        public HashSet<int> RelativeOrderIds { get; set; }
        protected string RelativeOrdersIdsString { get; set; }

        public bool AccommodationRequired { get; set; }


        public DestinationSpecification()
        {
            RelativeOrderIds = new HashSet<int>();
            foreach (string c_idString in RelativeOrdersIdsString.Split(';'))
            {
                RelativeOrderIds.Add(int.Parse(c_idString));
            }
            
        }
    }
}
