using System.Collections.Generic;

namespace STrip.Models.TripOptimizers.ACO
{
    public class TripSolutionACO : TripSolution
    {
        public List<SolutionComponentACO> ComponentsACO { get; }

        public TripSolutionACO(Trip p_trip, List<SolutionComponent> p_accomodationOptions, List<SolutionComponent> p_transportationOptions) : base(p_trip:p_trip, p_accomodationOptions:p_accomodationOptions, p_transportationOptions:p_transportationOptions)
        {
            this.ComponentsACO = new List<SolutionComponentACO>();
        }
    }
}