﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace STrip.Models.TripOptimizers.ACO
{
    public class Ant
    {
        private Trip trip { get; }
        private List<SolutionComponentACO> accommodationOptions { get; }
        private List<SolutionComponentACO> transportationOptions { get; }
        private TripSolutionACO currentSolution { get; set; }

        public Ant(Trip p_trip, List<SolutionComponentACO> p_accommodationOptions, List<SolutionComponentACO> p_transportationOptions)
        {
            this.trip = p_trip;
            this.accommodationOptions = p_accommodationOptions;
            this.transportationOptions = p_transportationOptions;
            this.currentSolution = new TripSolutionACO(
                p_trip:this.trip,
                p_accomodationOptions:this.accommodationOptions.Cast<SolutionComponent>().ToList(),
                p_transportationOptions:this.transportationOptions.Cast<SolutionComponent>().ToList()
            );
        }

        public TripSolutionACO BuildSolution()
        {
            while(!this.currentSolution.IsCompleteSolution(this.trip))
            {
                List<SolutionComponentACO> feasibleComponents = this.currentSolution.GetFeasibleComponents().Cast<SolutionComponentACO>().ToList();
                currentSolution.ComponentsACO.Add(ChooseNextStep(feasibleComponents));
            }

            currentSolution.EvalSolution();
            return currentSolution;
        }

        private static SolutionComponentACO ChooseNextStep(List<SolutionComponentACO> p_feasibleComponents)
        {
            List<float> whell = new List<float>(p_feasibleComponents.Count) {0};

            float v_probalitySum = p_feasibleComponents.Sum(component => component.ProbabilityValue); //necessary?
            p_feasibleComponents.ForEach(i_component => whell.Add(whell.Last() + i_component.ProbabilityValue/v_probalitySum));

            float maxValue = whell.Last();
            whell.ForEach(val => val = val/maxValue);

            double randomValue = new Random().NextDouble();
            int chosenIndex = whell.IndexOf(whell.First(c => c < randomValue));

            return p_feasibleComponents.ElementAt(chosenIndex);
        }
    }
}
