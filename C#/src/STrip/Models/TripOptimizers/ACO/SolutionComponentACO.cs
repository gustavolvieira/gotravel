using System;
using System.Collections.Generic;

namespace STrip.Models.TripOptimizers.ACO
{
    public class SolutionComponentACO
    {
        private SolutionComponent Component { get; }

        public int Id => this.Component.Id;
        public HashSet<Destination> DestinationsSet => this.Component.DestinationsSet;
        public decimal CostValue => this.Component.CostValue;
        public decimal TimeValue => this.Component.TimeValue;

        public Dictionary<Objectives, float> PherormoneValues; 
        public Dictionary<Objectives, float> HeuristicValues; 

        

        public float ProbabilityValue
        {
            get
            {
                float v_value = 1;
                foreach (Objectives objectiveKey in this.PherormoneValues.Keys)
                {
                    v_value *= (float)Math.Pow(this.PherormoneValues[objectiveKey], Parameters.PherormoneModifier[objectiveKey]);
                    v_value *= (float)Math.Pow(this.HeuristicValues[objectiveKey], Parameters.HeuristicModifier[objectiveKey]);
                }
                return v_value;
            }
        }

        public SolutionComponentACO(SolutionComponent p_component)
        {
            this.Component = p_component;
            this.PherormoneValues = new Dictionary<Objectives, float>();
            foreach (Objectives i_objective in Enum.GetValues(typeof(Objectives)))
            {
                this.PherormoneValues.Add(i_objective, Parameters.PherormoneDefault);
            }
            this.HeuristicValues = new Dictionary<Objectives, float>();
            foreach (Objectives i_objective in Enum.GetValues(typeof(Objectives)))
            {
                this.HeuristicValues.Add(i_objective, this.CalculateHeuristic(i_objective));
            }
        }

        private float CalculateHeuristic(Objectives p_objective)
        {
            //TODO calculate heuristics for each objective
            switch (p_objective)
            {
                case Objectives.Cost:
                    return 1;

                case Objectives.Time:
                    return 1;

//                case Objectives.Comfort:
//                    return 1;

                default:
                    return 1;
            }
        }

        public void EvaporatePherormones()
        {
            foreach (KeyValuePair<Objectives,float> pherormone in this.PherormoneValues)
            {
               this.PherormoneValues[pherormone.Key] = (1 - Parameters.EvaporationRate)* pherormone.Value;
            }
        }

        public void UpdatePherormones(TripSolution p_tripSolution)
        {
            foreach (KeyValuePair<Objectives, float> pherormone in this.PherormoneValues)
            {
                this.PherormoneValues[pherormone.Key] += p_tripSolution.GetQualityValue(pherormone.Key);
            }
        }

    }
}