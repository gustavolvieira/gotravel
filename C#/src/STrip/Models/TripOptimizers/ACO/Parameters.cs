﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Routing.Constraints;

namespace STrip.Models.TripOptimizers.ACO
{
    public static class Parameters
    {
        //TODO determine values
        public const float EvaporationRate = 1;
        public const float PherormoneDefault = 1;
        public const int NumberAnts = 40;
        public const int ParetoSetSize = 25;
        public const float MinimumParetoImprovement = 1;
        public const int MaxGenerationsWithoutImprovement = 10;
        public static readonly TimeSpan MaxExecutionTime = TimeSpan.FromMinutes(3);

        public static readonly Dictionary<Objectives, float> PherormoneModifier = new Dictionary<Objectives, float>()
        {
//            {Objectives.Comfort, 1},
            {Objectives.Cost, 1},
            {Objectives.Time, 1},
        };
        public static readonly Dictionary<Objectives, float> HeuristicModifier = new Dictionary<Objectives, float>()
        {
//            {Objectives.Comfort, 1},
            {Objectives.Cost, 1},
            {Objectives.Time, 1},
        };
    }
}
