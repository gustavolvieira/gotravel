﻿using System.Collections.Generic;

namespace STrip.Models.TripOptimizers
{
    public interface ITripOptimizer
    {
        List<TripSolution> Optimize(Trip trip);
    }
}
