﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using STrip.Models.TripOptimizers.ACO;

namespace STrip.Models.TripOptimizers
{
    public class AntColonyOptimizer : ITripOptimizer
    {
        private List<Ant> AntAgents { get; set; }
        private List<SolutionComponentACO> AccommodationOptions { get; }
        private List<SolutionComponentACO> TransportationOptions { get; }

        private Trip Trip { get; set; }
        private List<TripSolution> ParetoSolutions { get; set; }
        private float ParetoEvaluation { get; set; }
        private float ParetoImprovement { get; set; }

        private int numberAnts;
        private int generationWithoutImprovement = 0;

        private Stopwatch stopwatch = new Stopwatch();


        public AntColonyOptimizer(Trip p_trip, int p_numberAnts = Parameters.NumberAnts)
        {
            this.numberAnts = p_numberAnts;
            this.Trip = p_trip;
            this.AccommodationOptions = new List<SolutionComponentACO>();
            this.TransportationOptions = new List<SolutionComponentACO>();
            this.ParetoSolutions = new List<TripSolution>();
            this.ParetoEvaluation = 0;
            this.ParetoImprovement = 0;

        }

        private void InitializeOptimizer(Trip p_trip)
        {
            this.Trip = p_trip;
            this.AccommodationOptions.Clear();
            this.TransportationOptions.Clear();
            this.ParetoSolutions.Clear();
            this.ParetoEvaluation = 0;
            this.ParetoImprovement = 0;
            this.generationWithoutImprovement = 0;

            p_trip.TransportationOptions.ForEach(i_option => this.TransportationOptions.Add(new SolutionComponentACO(i_option)));
            p_trip.AccommodationOptions.ForEach(i_option => this.AccommodationOptions.Add(new SolutionComponentACO(i_option)));

            CreateAnts();
        }

        private void CreateAnts()
        {
            for (int index = 0; index < this.numberAnts; index++)
            {
                this.AntAgents.Add(new Ant(this.Trip, p_accommodationOptions:this.AccommodationOptions, p_transportationOptions:this.TransportationOptions));
            }
        }

        public List<TripSolution> Optimize(Trip p_trip)
        {
            InitializeOptimizer(p_trip);

            return Optimize();
        }

        public List<TripSolution> Optimize()
        {
            this.stopwatch.Start();

            while (!this.StopConditionMet())
            {
                List<TripSolutionACO> tripSolutions = this.AntAgents.Select(ant => ant.BuildSolution()).ToList();
                UpdatePherormones(tripSolutions);
                GetParetoFrontier(tripSolutions);
            }

            this.stopwatch.Stop();
            return this.ParetoSolutions;
        }

        private bool StopConditionMet()
        {
            if (this.ParetoEvaluation < Parameters.MinimumParetoImprovement)
                this.generationWithoutImprovement++;

            return (this.generationWithoutImprovement > Parameters.MaxGenerationsWithoutImprovement ||
                    this.stopwatch.Elapsed > Parameters.MaxExecutionTime);
        }

        private void UpdatePherormones(List<TripSolutionACO> p_tripSolutions)
        {
            AccommodationOptions.ForEach(component => component.EvaporatePherormones());

            foreach (var i_tripSolution in p_tripSolutions)
            {
                i_tripSolution.ComponentsACO.ForEach(i_component => i_component.UpdatePherormones(i_tripSolution));
            }
        }

        private void GetParetoFrontier(List<TripSolutionACO> p_tripSolutions)
        {
            HashSet<TripSolution> v_solutionSet = new HashSet<TripSolution>();
            v_solutionSet.UnionWith(this.ParetoSolutions);
            v_solutionSet.UnionWith(p_tripSolutions);

            foreach (TripSolution tripSolution in v_solutionSet)
            {
                tripSolution.DominanceRank = 0;
                tripSolution.CrowdingDistance = 0;
                foreach (TripSolution comparisonSolution in v_solutionSet)
                {
                    bool v_dominant = true;
                    bool v_betterValue = false;
                    foreach (KeyValuePair<Objectives,float> solutionValue in tripSolution.QualityValue)
                    {
                        if (solutionValue.Value > comparisonSolution.QualityValue[solutionValue.Key])
                        {
                            v_dominant = false;
                            break;
                        }
                        if (solutionValue.Value < comparisonSolution.QualityValue[solutionValue.Key])
                        {
                            v_betterValue = true;
                        }
                    }
                    if (v_dominant && v_betterValue)
                        tripSolution.DominanceRank++;
                }
            }
            //TODO calculate crowding distance

            this.ParetoSolutions = 
                v_solutionSet
                .OrderByDescending(solution => solution.DominanceRank)
                .ThenBy(solution => solution.CrowdingDistance)
                .Take(Parameters.ParetoSetSize)
                .ToList();

            float v_evaluation = EvaluatePareto(this.ParetoSolutions);
            this.ParetoImprovement = v_evaluation - this.ParetoEvaluation;
            this.ParetoEvaluation = v_evaluation;
        }

        private float EvaluatePareto(List<TripSolution> paretoSolutions)
        {
            //TODO implement hypervolume?
            return 1;
        }
    }
}
