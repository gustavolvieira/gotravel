﻿using System;

namespace STrip.Models.Util
{
    public class DateTimeRange
    {
        public int Id { get; set; }
        public DateTime Initial { get; set; }
        public DateTime Final { get; set; }
    }
}