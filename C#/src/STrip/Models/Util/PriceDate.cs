﻿using System;

namespace STrip.Models.Util
{
    public class PriceDate
    {
        public int Id { get; set; }
        public DateTime Date;
        public Money Price;

    }
}