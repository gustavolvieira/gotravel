﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STrip.Models.Util
{
    public class Money
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
    }
}
