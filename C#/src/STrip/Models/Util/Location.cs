﻿using GeoCoordinatePortable;

namespace STrip.Models.Util
{
    public class Location
    {
        public int Id { get; set; }
        public string City {get; set;}
        public string Street {get; set;}
        public string StateOrProvince {get; set;}
        public string Country {get; set;}
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public float? MaxRadiusKm { get; set; }
    }
}
