﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using STrip.Models.Util;

namespace STrip.Models
{
    public class AccommodationRestrictions
    {
        public int Id { get; set; }
        public HashSet<Location> LocationRestrictions { get; set; }
        public string CompanyRestrictions { get; set; }
        public Money PriceMin { get; set; }
        public Money PriceMax { get; set; }

        public float HotelStars { get; set; }
    }
}
