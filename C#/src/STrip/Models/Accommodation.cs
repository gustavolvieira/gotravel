﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using STrip.Models.Util;

namespace STrip.Models
{
    public class Accommodation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Destination Destination { get; set; }
        public AccommodationType Type { get; set; }
        public float ReputationValue { get; set; }
        public Location Location { get; set; }
        public DateTime CheckInTime { get; set; }
        public List<PriceDate> PriceDateList { get; set; }
        public string Company { get; set; }

    }
}
