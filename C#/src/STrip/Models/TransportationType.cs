﻿namespace STrip.Models
{
    public enum TransportationType
    {
        Plane,
        Bus,
        Train
    }
}