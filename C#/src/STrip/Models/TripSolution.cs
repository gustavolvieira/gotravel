﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Routing.Constraints;

namespace STrip.Models
{
    public class TripSolution
    {
        //Properties
        public int Id { get; set; }
        public List<SolutionComponent> Components { get; set; }
        private Trip trip { get; }
        private DateTime currentDate { get; set; }
        private DestinationSpecification currentDestination { get; set; }
        private List<SolutionComponent> accomodationOptions { get; }
        private List<SolutionComponent> transportationOptions { get; }
        public int DominanceRank { get; set; }
        public float CrowdingDistance { get; set; }
        public Dictionary<Objectives, float> QualityValue;

        //Methods
        public TripSolution(Trip p_trip, List<SolutionComponent> p_accomodationOptions, List<SolutionComponent> p_transportationOptions)
        {
            this.trip = p_trip;
            this.Components = new List<SolutionComponent>();
            this.accomodationOptions = p_accomodationOptions;
            this.transportationOptions = p_transportationOptions;
            this.QualityValue = new Dictionary<Objectives, float>();
            this.DominanceRank = 0;
            this.CrowdingDistance = 0;
        }

        public void EvalSolution()
        {
            QualityValue[Objectives.Cost] = (float)Components.Sum(c => c.CostValue);
            QualityValue[Objectives.Time] = (float)Components.Sum(c => c.TimeValue);
        }

        public float GetQualityValue(Objectives p_objective)
        {
            return this.QualityValue[p_objective];
        }

        public List<SolutionComponent> GetFeasibleComponents()
        {
            this.GetCurrentDestination();
            this.currentDate = this.Components.Last().FinalDate;

            if (this.Components.Last().GetType() == typeof(TransportationOption))
            {
                return GetFeasibleAccommodations();
            }
            else
            {
                return GetFeasibleTransportations();
            }
        }

        private List<SolutionComponent> GetFeasibleAccommodations()
        {
            List<SolutionComponent> feasibleAccommodations =
                (
                    from accomodation in this.accomodationOptions
                    where accomodation.InitialDate.Date == this.currentDate.Date
                    where accomodation.FinalDestination.Id == this.currentDestination.Id
                    select accomodation
                ).ToList();

            return feasibleAccommodations;
        }

        private List<SolutionComponent> GetFeasibleTransportations()
        {
            List<int> visitedDestinationsIds =
                this.Components.Select(component => component.FinalDestination.Id).ToList();

            HashSet<int> visitedDestinationsIdsSet = new HashSet<int>(visitedDestinationsIds);

            int numberDestinationsVisited = visitedDestinationsIdsSet.Count + 1;
            if (!(this.trip.RoundTrip && numberDestinationsVisited == this.trip.Destinations.Count))
            {
                visitedDestinationsIdsSet.Add(this.trip.StartingDestination.Id);
            }

            List<SolutionComponent> feasibleTransportations =
                (
                    from transportation in this.transportationOptions
                    where transportation.InitialDate.Date == this.currentDate.Date
                    where transportation.InitialDestination.Id == this.currentDestination.Id
                    where !visitedDestinationsIdsSet.Contains(transportation.FinalDestination.Id)
                    where (new List<int>() {-1,1, numberDestinationsVisited+1}.Contains(
                            this.trip.Destinations.Single(destination => destination.Id == transportation.FinalDestination.Id).AbsoluteOrder
                        ))
                    where visitedDestinationsIdsSet.IsSupersetOf(
                        this.trip.Destinations.Single(destination => destination.Id == transportation.FinalDestination.Id).RelativeOrderIds
                        )
                    select transportation
                ).ToList();

            return feasibleTransportations;
        }

        private void GetCurrentDestination()
        {
            this.currentDestination = this.trip.Destinations.Single(destination => destination.Id == this.Components.Last().FinalDestination.Id);
        }

        public bool IsCompleteSolution(Trip p_trip)
        {
            //IsCompleteSolution only checks size, not correctness
            //It assumes that solution components have "empty" components when no accomodation or transportation is needed
            return this.Components.Count == (p_trip.Destinations.Count*2 + (p_trip.RoundTrip ? 1 : 0));
        }
    }
}
