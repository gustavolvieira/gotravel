﻿using System;
using System.Collections.Generic;
using System.Linq;
using STrip.Models.Util;

namespace STrip.Models
{
    public class TransportationBranch
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public int DestinationId { get; set; }
        public DateTime DepartingDateTime { get; set; }
        public DateTime ArrivingDateTime { get; set; }
        public Station DepartingStation { get; set; }
        public Station ArrivingStation { get; set; }
        public TransportationType Type { get; set; }
        public Destination StartingDestination { get; set; }
        public Destination FinalDestination { get; set; }
        public string Company { get; set; }
    }

    public class Station
    {
        public int Id { get; set; }
        public string StationName { get; set; }
        public string Code { get; set; }
        public Location StationLocation { get; set; }
    }

    public class TransportationOption  : SolutionComponent
    {
        public Money Price { get; set; }
        public List<TransportationBranch> BranchesList { get; set; }

        //TODO make gets based on BranchesList
        //public DateTime DepartingDateTime { get; set; }
        //public DateTime ArrivingDateTime { get; set; }
        //public Station DepartingStation { get; set; }
        //public Station ArrivingStation { get; set; }
        //public TransportationType Type { get; set; }
        //public int StartingDestinationId { get; set; }
        //public int FinalDestinationId { get; set; }
        //public string Company { get; set; }

        public override HashSet<Destination> DestinationsSet => new HashSet<Destination>() { this.BranchesList.First().StartingDestination,
                this.BranchesList.Last().FinalDestination };

        public override Destination FinalDestination => this.BranchesList.Last().FinalDestination;
        public override Destination InitialDestination => this.BranchesList.First().StartingDestination;

        public override decimal CostValue => this.Price.Amount;

        public override decimal TimeValue
        {
            get
            {
                return (this.BranchesList.Last().ArrivingDateTime - this.BranchesList.First().DepartingDateTime).Minutes;
            }
        }

        public override DateTime FinalDate => this.BranchesList.Last().ArrivingDateTime;
        public override DateTime InitialDate => this.BranchesList.First().DepartingDateTime;
    }
}
