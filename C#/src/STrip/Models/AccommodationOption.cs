﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using STrip.Models.Util;

namespace STrip.Models
{
    public class AccommodationOption : SolutionComponent
    {
        public Accommodation Accomodation { get; set; }
        public DateTimeRange TimeRange { get; set; }
        public Money Price { get; set; }

        public AccommodationOption(Accommodation accommodation, DateTimeRange timeRange)
        {
            this.Accomodation = accommodation;
            this.TimeRange = timeRange;
            this.Price = new Money();
            foreach (PriceDate priceDate in Accomodation.PriceDateList)
            {
                if ((priceDate.Date.Date >= TimeRange.Initial.Date) && (priceDate.Date.Date <= TimeRange.Final.Date))
                    this.Price.Amount += priceDate.Price.Amount;
            }
        }

        public override HashSet<Destination> DestinationsSet => new HashSet<Destination>(){this.Accomodation.Destination};

        public override Destination FinalDestination => this.Accomodation.Destination;
        public override Destination InitialDestination => this.Accomodation.Destination;

        public override decimal CostValue => this.Price.Amount;
        public override decimal TimeValue => 0;

        public override DateTime FinalDate => this.TimeRange.Final;
        public override DateTime InitialDate => this.TimeRange.Initial;
    }
}
