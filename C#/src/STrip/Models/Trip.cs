﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using STrip.Models.TripOptimizers;
using STrip.Models.Util;

namespace STrip.Models
{
    public class Trip
    {
        public int Id { get; set; }
        public int User { get; set; }
        public HashSet<DestinationSpecification> Destinations { get; set; }
        public DateTimeRange StartDateRange { get; set; }
        public DateTimeRange EndDateRange { get; set; }
        public bool RoundTrip { get; set; }
        public DestinationSpecification StartingDestination { get; set; }
        public List<TripSolution> Solutions { get; set; }

        public AccommodationRestrictions AccommodationRestrictions { get; set; }
        public TransportationRestrictions TransportationRestrictions { get; set; }

        public List<TransportationOption> TransportationOptions { get; set; }
        public List<AccommodationOption> AccommodationOptions { get; set; }

        public void DetermineSolutions()
        {
            this.FetchOptions();

            ITripOptimizer optimizer = new AntColonyOptimizer(this);
            this.Solutions = optimizer.Optimize(this);
        }

        private void FetchOptions()
        {
            TransportationOptions = this.FetchTransportations();
            AccommodationOptions = this.FetchAccommodations();
        }

        private List<TransportationOption> FetchTransportations()
        {
            throw new NotImplementedException();

        }

        private List<AccommodationOption> FetchAccommodations()
        {
            throw new NotImplementedException();
        }
    }
}
