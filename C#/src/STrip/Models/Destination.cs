﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STrip.Models
{
    public class Destination
    { 
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MainPhotoUrl { get; set; }


    }
}
