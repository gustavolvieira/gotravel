﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using GeoCoordinatePortable;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using STrip.Models;
using STrip.Models.Util;

namespace STrip.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<AccommodationOption>().ToTable("AccommodationOption");
            builder.Entity<TransportationOption>().ToTable("TransportationOption");
            builder.Entity<SolutionComponent>().ToTable("SolutionComponent");
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        public DbSet<User> User { get; set; }

        public DbSet<Trip> Trip { get; set; }
        public DbSet<TripSolution> TripSolution { get; set; }

        public DbSet<Destination> Destination { get; set; }
        public DbSet<DestinationSpecification> DestinationSpecification { get; set; }

        public DbSet<SolutionComponent> SolutionComponent { get; set; }

        public DbSet<Accommodation> Accommodation { get; set; }
        public DbSet<AccommodationOption> AccommodationOption { get; set; } 
        public DbSet<AccommodationRestrictions> AccommodationRestrictions { get; set; }
        public DbSet<TransportationOption> TransportationOption { get; set; }
        public DbSet<TransportationRestrictions> TransportationRestrictions { get; set; }


    }
}
