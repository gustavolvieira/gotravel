﻿namespace STrip.Models
{
    public enum AccommodationType
    {
        Hotel,
        Hostel,
        House,
        Apartment,
        Couchsurfing,
        Other
    }
}