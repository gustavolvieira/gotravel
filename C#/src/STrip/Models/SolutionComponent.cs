﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STrip.Models
{
    public abstract class SolutionComponent
    {
        public int Id { get; set; }
        public bool IsEmpty { get; set; }

        public abstract HashSet<Destination> DestinationsSet { get; }

        public abstract Destination FinalDestination { get; }
        public abstract Destination InitialDestination { get; }

        public abstract decimal CostValue { get; }
        public abstract decimal TimeValue { get; }

        public abstract DateTime FinalDate { get; }
        public abstract DateTime InitialDate { get; }
    }
}
