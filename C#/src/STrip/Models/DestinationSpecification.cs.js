﻿function inherits(subConstructor, superConstructor) {
    var proto = Object.create(
        superConstructor.prototype,
        {
            "constructor": { 
                configurable: true,
                enumerable: false,
                writable: true,
                value: subConstructor
            }
        }
    );
    Object.defineProperty(subConstructor, "prototype",  { 
        configurable: true,
        enumerable: false,
        writable: true,
        value: proto
    });
}

var server = server || {};
/// <summary>The DestinationSpecification class as defined in STrip.Models.DestinationSpecification</summary>
server.DestinationSpecification = function() {
	/// <field name="id" type="Number">The Id property as defined in STrip.Models.DestinationSpecification</field>
	this.id = 0;
	/// <field name="dateInterval" type="Object">The DateInterval property as defined in STrip.Models.DestinationSpecification</field>
	this.dateInterval = { };
	/// <field name="absoluteOrder" type="Number">The AbsoluteOrder property as defined in STrip.Models.DestinationSpecification</field>
	this.absoluteOrder = 0;
	/// <field name="relativeOrderIds" type="Number[]">The RelativeOrderIds property as defined in STrip.Models.DestinationSpecification</field>
	this.relativeOrderIds = [];
	/// <field name="accommodationRequired" type="Boolean">The AccommodationRequired property as defined in STrip.Models.DestinationSpecification</field>
	this.accommodationRequired = false;
};

inherits(DestinationSpecification, Destination);/// <summary>The Destination class as defined in STrip.Models.Destination</summary>
server.Destination = function() {
	/// <field name="id" type="Number">The Id property as defined in STrip.Models.Destination</field>
	this.id = 0;
	/// <field name="name" type="String">The Name property as defined in STrip.Models.Destination</field>
	this.name = '';
	/// <field name="description" type="String">The Description property as defined in STrip.Models.Destination</field>
	this.description = '';
	/// <field name="mainPhotoUrl" type="String">The MainPhotoUrl property as defined in STrip.Models.Destination</field>
	this.mainPhotoUrl = '';
};

