﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using STrip.Models.Util;

namespace STrip.Models
{
    public class TransportationRestrictions
    {
        public int Id { get; set; }
        [NotMapped]
        public HashSet<TransportationType> TypeRestrictions { get; set; }
        public string TypeRestrictionsString { get; set; }
        public HashSet<DateTimeRange> TimeRestrictions { get; set; }
        public string CompanyRestrictions { get; set; }
        public Money PriceMin { get; set; }
        public Money PriceMax { get; set; }
    }
}
