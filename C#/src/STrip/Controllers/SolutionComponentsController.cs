using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using STrip.Models;

namespace STrip.Controllers
{
    public class SolutionComponentsController : Controller
    {
        private ApplicationDbContext _context;

        public SolutionComponentsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: SolutionComponents
        public IActionResult Index()
        {
            return View(_context.SolutionComponent.ToList());
        }

        // GET: SolutionComponents/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SolutionComponent solutionComponent = _context.SolutionComponent.Single(m => m.Id == id);
            if (solutionComponent == null)
            {
                return HttpNotFound();
            }

            return View(solutionComponent);
        }

        // GET: SolutionComponents/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SolutionComponents/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SolutionComponent solutionComponent)
        {
            if (ModelState.IsValid)
            {
                _context.SolutionComponent.Add(solutionComponent);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(solutionComponent);
        }

        // GET: SolutionComponents/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SolutionComponent solutionComponent = _context.SolutionComponent.Single(m => m.Id == id);
            if (solutionComponent == null)
            {
                return HttpNotFound();
            }
            return View(solutionComponent);
        }

        // POST: SolutionComponents/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(SolutionComponent solutionComponent)
        {
            if (ModelState.IsValid)
            {
                _context.Update(solutionComponent);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(solutionComponent);
        }

        // GET: SolutionComponents/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            SolutionComponent solutionComponent = _context.SolutionComponent.Single(m => m.Id == id);
            if (solutionComponent == null)
            {
                return HttpNotFound();
            }

            return View(solutionComponent);
        }

        // POST: SolutionComponents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            SolutionComponent solutionComponent = _context.SolutionComponent.Single(m => m.Id == id);
            _context.SolutionComponent.Remove(solutionComponent);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
