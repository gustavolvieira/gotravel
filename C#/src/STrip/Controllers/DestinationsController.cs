using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using STrip.Models;

namespace STrip.Controllers
{
    public class DestinationsController : Controller
    {
        private ApplicationDbContext _context;

        public DestinationsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Destinations
        public IActionResult Index()
        {
            return View(_context.Destination.ToList());
        }

        // GET: Destinations/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Destination destination = _context.Destination.Single(m => m.Id == id);
            if (destination == null)
            {
                return HttpNotFound();
            }

            return View(destination);
        }

        // GET: Destinations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Destinations/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Destination destination)
        {
            if (ModelState.IsValid)
            {
                _context.Destination.Add(destination);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(destination);
        }

        // GET: Destinations/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Destination destination = _context.Destination.Single(m => m.Id == id);
            if (destination == null)
            {
                return HttpNotFound();
            }
            return View(destination);
        }

        // POST: Destinations/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Destination destination)
        {
            if (ModelState.IsValid)
            {
                _context.Update(destination);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(destination);
        }

        // GET: Destinations/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Destination destination = _context.Destination.Single(m => m.Id == id);
            if (destination == null)
            {
                return HttpNotFound();
            }

            return View(destination);
        }

        // POST: Destinations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Destination destination = _context.Destination.Single(m => m.Id == id);
            _context.Destination.Remove(destination);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
