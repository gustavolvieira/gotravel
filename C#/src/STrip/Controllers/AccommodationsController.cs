using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using STrip.Models;

namespace STrip.Controllers
{
    public class AccommodationsController : Controller
    {
        private ApplicationDbContext _context;

        public AccommodationsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Accommodations
        public IActionResult Index()
        {
            return View(_context.Accommodation.ToList());
        }

        // GET: Accommodations/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Accommodation accommodation = _context.Accommodation.Single(m => m.Id == id);
            if (accommodation == null)
            {
                return HttpNotFound();
            }

            return View(accommodation);
        }

        // GET: Accommodations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Accommodations/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Accommodation accommodation)
        {
            if (ModelState.IsValid)
            {
                _context.Accommodation.Add(accommodation);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(accommodation);
        }

        // GET: Accommodations/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Accommodation accommodation = _context.Accommodation.Single(m => m.Id == id);
            if (accommodation == null)
            {
                return HttpNotFound();
            }
            return View(accommodation);
        }

        // POST: Accommodations/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Accommodation accommodation)
        {
            if (ModelState.IsValid)
            {
                _context.Update(accommodation);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(accommodation);
        }

        // GET: Accommodations/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Accommodation accommodation = _context.Accommodation.Single(m => m.Id == id);
            if (accommodation == null)
            {
                return HttpNotFound();
            }

            return View(accommodation);
        }

        // POST: Accommodations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Accommodation accommodation = _context.Accommodation.Single(m => m.Id == id);
            _context.Accommodation.Remove(accommodation);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
